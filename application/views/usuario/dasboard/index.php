<style>
     #carregando div,#carregando-nfc div {
        border-color: #f3f3f3;
        border-top-color: #0C1B25;
        border-bottom-color: #0C1B25;
    }

    #bem-vindo .modal-header, #curtir-facebook .modal-header{
        background: #0C1B25;
        color: #FFFFFF;
    }

    .pace .pace-progress {
      background: #1F4968;
    }

    a {
      color: #0C1B25;
    }

    a:hover,
    a:active,
    a:focus {
      color: #1F4968    }

    .text-primary{
        color: #0C1B25    }

    .text-primary:hover{
        color: #1F4968    }

    .cor-padrao{
      color: #0C1B25    }

    .flash-tab{
      background: #0C1B25;
    }

    .dropdown-menu > li > a:hover {
      background-color: #0C1B25;
    }

    .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
      background: #0C1B25;
    }

    .box.box-primary {
      border-top-color: #0C1B25;
    }

    .box.box-solid.box-primary > .box-header {
      background: #0C1B25;
      background-color: #0C1B25;
    }

    .box .todo-list > li.primary {
      border-left-color: #0C1B25;
    }

    .bg-light-blue {
      background-color: #0C1B25 !important;
    }

    .bg-olive {
      background-color: #0C1B25 !important;
    }

    .text-light-blue {
      color: #0C1B25 !important;
    }

    .text-olive {
      color: #0C1B25 !important;
    }

    .btn.btn-primary {
      background-color: #0C1B25;
      border-color: #1F4968;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
      background-color: #1F4968;
      border-color: #0C1B25;
    }

    .nav.nav-pills > li.active > a,
    .nav.nav-pills > li.active > a:hover {
      border-top-color: #0C1B25;
      color: #444;
    }

    .nav.nav-pills.nav-stacked > li.active > a,
    .nav.nav-pills.nav-stacked > li.active > a:hover {
      border-left-color: #0C1B25;
    }

    .nav-tabs-custom > .nav-tabs > li.active {
      border-top-color: #0C1B25;
    }

    .form-box .header {
      background: #0C1B25;
    }

    .skin-blue .navbar {
      background-color: #0C1B25;
    }

    .form-control:hover,.form-control:focus,.form-control-table:hover,.form-control-table:focus{
      border-color: #0C1B25 !important;
    }

    .progress-bar-light-blue,
    .progress-bar-primary {
      background-color: #0C1B25;
    }

    fieldset legend {
        color: #0C1B25;
    }

    table.calendario tr td .compromissos{
        background: #0C1B25;
    }

    .btn.btn-primary {
      border-color: #0C1B25;
    }

    .btn.btn-primary:hover,
    .btn.btn-primary:active,
    .btn.btn-primary.hover {
      background-color: #0C1B25;
    }

    .skin-blue .logo {
      background-color: #0C1B25;
    }


    div.token-input-dropdown ul li.token-input-selected-dropdown-item {
        background: #0C1B25 !important;
    }

    .text-info {
      color:#0C1B25;
    }
    .text-info:hover {
      color:#1F4968;
    }

    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
      background-color: #0C1B25;
    }

    .wizard li.active span.round-tab {
        border: 2px solid #0C1B25;
    }

    .wizard li.active span.round-tab i{
        color: #0C1B25;
    }

    .wizard li:after{
        border-bottom-color: #0C1B25;
    }

    .wizard li.active:after {
        border-bottom-color: #0C1B25;
    }

    .ui-button.ui-state-active:hover {
    	background: #0C1B25 !important;
    }
</style> 
<!--codigo para imprimir via javascritp -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-imprimir").click(function() {
            $('#tab1').printElement();
        });
    });
</script>
 <!--Atualiza a pagina a cada 3 minutos -->
<script>
setTimeout(function() {
  window.location.reload(1);
}, 180000); // 3 minutos
</script>   
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Chamados
        <small>Gerenciar Chamados</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dasboard</a></li>
        <li class="active">os</li>
      </ol>
      
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <?php
        get_msg('salvo');
        ?>
         <div class="row">
        <div class="col-xs-12">
          
            <div class="box-header">
            <a href="<?php echo base_url();?>index.php/painel/abrir-chamado" class="btn btn-success btn-flat btn-responsive"><i class="fa fa-plus"></i> Abrir Chamado  </a>
              
            </div>
           
            <!-- /.box-header -->
          <!--AQUI VAI ADIÇÃO FILTROS DE BUSCA -->  
          
          <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title "> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon text-right"><i class="icon-list"></i></span>
                                <h5>Busca Avançada</h5>
                            </a> </div>
                    </div>
                    <div class="accordion-body  <?php if($filtro){echo 'in';}else{echo '';} ?> collapse" id="collapseGOne" style="height: auto;">
                        <div class="widget-content">
                        <div class="box-body">
          <div class="row">
                <form action="" method="get">
                  
                
                <div class="col-md-3">
                  <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" id="st_os"  name="st_os"   style="width: 100%;">
                                <option></option>
                                <option value="Aberto" >Aberto</option> 
                                <option value="Resolvendo" >Resolvendo</option> 
                                <option value="Parado" >Parado</option>   
                                <option value="Cancelado" >Cancelado</option>  
                                <option class="text-green" value="Fechado">Fechado</option>
                                </select>
                                        
                            </div> 
                </div>
                <div class="col-md-3">
                <div class="form-group">
                     <label>Data Inicial</label>
                     <input name="data1"  id="data1" placeholder="Data Inicial" class="form-control date datepicker hasDatepicker" value="" type="text">
                     </div>
                </div>   
                <div class="col-md-3">
                <div class="form-group"> 
                     <label>Data Final</label>
                      <input name="data2" id="data2" placeholder="Data Final" class="form-control date datepicker hasDatepicker" value="" type="text">
                </div>
                 </div>
              
                <div class="col-md-3">
                  <div class="form-group">
                                <label><br> </label>
                                <input type="submit" class="form-control select2 btn  <?php if($filtro){echo 'btn bg-purple';}else{echo 'btn btn-primary';} ?>" id="os_busca_nome"   name="buscar" value="<?php if($filtro){echo 'Desativar Filtro';}else{echo 'Ativar Filtro';} ?>" style="width: 100%;">       
                            </div> 
                </div>
                
                
                
                </form>
                   </div>
                        </div>
                        </div>
                    </div>
          </div>
            <!--FINAL FILTROS DE BUSCA -->  
            <!-- /.box-header -->
            <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-tags"></i> </span>
                            <h5>Meus Chamados</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered table-responsive-xs" id="os_tabela_busca">
                                
                                <thead>
                                    <tr style="backgroud-color: #2D335B">
                  <th>#Chamado</th>
                  <th>Data</th>
                  <th>Hora</th>
                  <th>Título</th>
                  <th>Responsavél</th>
                  <th>Status</th>
                  
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody  >
                <tr>
                <?php if($os){ foreach ($os as $oss) { ?>
                  <td><?php echo $oss->id_os;?></td>
                  <td><?php echo formataVisao($oss->dt_os);?></td>
                  <td><?php echo $oss->hr_os;?></td>
                  <td><?php echo $oss->titulo_os; ?> </td>
                 <?php $id_tecnico=$oss->id_tecnico;
                             $like=array(
                                 'id_tecnico'=>$id_tecnico
                             );
                 $tecnico=$this->Mos->listaOsTecnicoLike($p=0,$por_pagina=null,'v_tecnico','*',$like,$porData=null,$order=null);   ?>  


                  <td> <?php if($tecnico['0']->id_tecnico==1){echo 'Aguardando Atendimento.'; }else{ echo ''.$tecnico['0']->nm_funcionario.'';} ?> </td>
                  <td class="text-center "><?php if($oss->st_os=='Aberto') echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">'.$oss->st_os.'</div>';
                  if ($oss->st_os=='Resolvendo'){ echo '<div class="badge badge-info" >'.$oss->st_os.'</div>';
                  }
                  if ($oss->st_os=='Cancelado'){ echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">'.$oss->st_os.'</div>';
                  }
                  if ($oss->st_os=='Fechado'){ echo '<div class="badge " style="background-color: green; border-color: #E97F02" > Finalizado</div>';
                  }
                  if ($oss->st_os=='Parado'){ echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >'.$oss->st_os.'</div>';
                  }?></td>
                  <td> <div class="btn-group-horiontal text-center">
                  <a href="<?php echo base_url();?>index.php/painel/detalhe/<?php echo encript($oss->id_os); ?>"     title="Visualizar" class="btn btn-xs btn-flat btn-default"><i class="fa fa-eye"></i></a>
            
                     <?php if($oss->st_os<>'Fechado'){ ?>
                      <a  <?php if($oss->st_os<>'Aberto'){echo 'disabled="true" href="#"';}else{echo 'href='.base_url().'index.php/painel/excluir-chamado/'.encript($oss->id_os).''; } ?>    title="Cancelar" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-close"></i></a>
                     <?php  }else{?>
                        <a      title="Finalizado" class="btn btn-xs btn-flat btn-default"><i class="fa fa-lock"></i></a> 
                     <?php } ?>   
                      <a href="<?php echo base_url().'index.php/painel/imprime-chamado/'.encript($oss->id_os); ?>" title="Imprimir Chamado"  class="btn btn-xs btn-flat bg-navy"><i class="fa fa-print"></i></a>
                    </div></td>
                </tr>
                <?php } }else{?>    
                
                <td colspan="6"><center>Nenhum Chamado cadastrado</center> </td>
                </tr>
                <?php } ?>
               
                </tbody>
                <tfoot>
                    <tr>
                  <td colspan="9"  style="align:right">    
                  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                  <ul class="pagination">
                  <?php if(isset($pag) && !empty($pag)){foreach ($pag as $key => $value) {
                      echo " {$value} ";
                  }}; ?>
                  </ul>
                  </div>
                  </td>
                    </tr>
                   
                </tfoot>
              </table>
              
                <!-- /.box-body -->
                <div class="box-footer">
                
                        </div>
                        <!-- /.box-footer-->
            </div>
            <!-- /.box-body -->
          
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->








<!--modal visualizar detalhes -->


<!-- Button trigger modal -->


<!-- Modal-->
<?php
if ($os) {
    foreach ($os as $oss) {
        ?>
        <div class="modal modal-details fade" style="border-color: red;" id="ver_<?php echo $oss->id_os; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-white  " style="margin: 5px;" d="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="" style="padding:10px;"> <b>Nº Chamado:</b> <?php echo $oss->id_os; ?><br>
                         
                           
                            <b>Data chamado: </b><?php echo formataVisao($oss->dt_os); ?>   <b>Horário: </b><?php echo $oss->hr_os; ?>
                            <br> 
                            
                            <?php //pesquisa para saber quem foi o filha da puta que atendeu essa porra p/ passar informação p/ cliente pau no cu
                             $id_tecnico=$oss->id_tecnico;
                             $like=array(
                                 'id_tecnico'=>$id_tecnico
                             );
                             $tecnico=$this->Mos->listaOsTecnicoLike($p=0,$por_pagina=null,'v_tecnico','*',$like,$porData=null,$order=null);   ?>
                            
                            <b>Problema: </b><textarea class=" form-control  "  style="padding:10px;" disabled="true" name="ob_os" id="ob_os" cols="30" rows="2"><?php echo $oss->df_os; ?></textarea>
                            <br> 
                            <b>Observação: </b><textarea class="col-lg-12 form-control " style="padding:10px;" disabled="true" name="ob_os" id="ob_os" cols="30" rows="2"><?php echo $oss->ob_os; ?></textarea>
                            <br>
                            <hr>
                            <b>Status: </b><?php echo $oss->st_os;?> </b>
                            <br> 
                            <b>Data Atendimento: </b><?php if(isset($oss->dt_atendimento)){ echo formataVisao($oss->dt_atendimento);}else{echo 'Aguardando...';}; ?>   <b>Horário: </b><?php if(isset($oss->hr_atendimento)){echo $oss->hr_atendimento;}else{echo 'Aguardando...';} ?>
                            </br>
                            <?php if($tecnico['0']->id_tecnico==1){echo '<b>Técnico Responsável: </b> Aguardando Atendimento. <br>'; }else{ echo '<b>Técnico Responsável: </b>'.$tecnico['0']->nm_funcionario.' <br>';} ?> 
                           </br>
                            <b>Solução: </b><textarea class="col-lg-12 form-control " disabled="true"><?php echo $oss->ld_tecnico; ?></textarea>
                            </br>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<?php
if ($os) {
    foreach ($os as $oss) {
        ?>
        <form action="<?php echo base_url() ?>index.php/os/delete/<?php echo encript($oss->id_os);?>/<?php echo encript($oss->id_local);?>" method="get" >
        <div class="modal fade" id="modal-logar_<?php echo $oss->id_os; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Para atender esse chamado é preciso entrar com usuário e senha</center></h4>

         <br> 

         <div class="control-group">
               <input id="email" name="email" type="text" class="form-control" placeholder="Email" /> 
            </div>
    </br> 
          
            <div class="control-group">
                <div class="controls">
                   <input name="senha" type="password" class="form-control" placeholder="Senha" /> </div>
                
            </div>
            <div class="form-actions" style="text-align: center">
                <div id="progress-acessar" class='hide progress progress-info progress-striped active'>
                    <div class='bar' style='width: 100%'></div>
                </div> <button id="btn-acessar" class="btn btn-success btn-large" /> Acessar</button>
            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  


