<style>
     #carregando div,#carregando-nfc div {
        border-color: #f3f3f3;
        border-top-color: #0C1B25;
        border-bottom-color: #0C1B25;
    }

    #bem-vindo .modal-header, #curtir-facebook .modal-header{
        background: #0C1B25;
        color: #FFFFFF;
    }

    .pace .pace-progress {
      background: #1F4968;
    }

    a {
      color: #0C1B25;
    }

    a:hover,
    a:active,
    a:focus {
      color: #1F4968    }

    .text-primary{
        color: #0C1B25    }

    .text-primary:hover{
        color: #1F4968    }

    .cor-padrao{
      color: #0C1B25    }

    .flash-tab{
      background: #0C1B25;
    }

    .dropdown-menu > li > a:hover {
      background-color: #0C1B25;
    }

    .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
      background: #0C1B25;
    }

    .box.box-primary {
      border-top-color: #0C1B25;
    }

    .box.box-solid.box-primary > .box-header {
      background: #0C1B25;
      background-color: #0C1B25;
    }

    .box .todo-list > li.primary {
      border-left-color: #0C1B25;
    }

    .bg-light-blue {
      background-color: #0C1B25 !important;
    }

    .bg-olive {
      background-color: #0C1B25 !important;
    }

    .text-light-blue {
      color: #0C1B25 !important;
    }

    .text-olive {
      color: #0C1B25 !important;
    }

    .btn.btn-primary {
      background-color: #0C1B25;
      border-color: #1F4968;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
      background-color: #1F4968;
      border-color: #0C1B25;
    }

    .nav.nav-pills > li.active > a,
    .nav.nav-pills > li.active > a:hover {
      border-top-color: #0C1B25;
      color: #444;
    }

    .nav.nav-pills.nav-stacked > li.active > a,
    .nav.nav-pills.nav-stacked > li.active > a:hover {
      border-left-color: #0C1B25;
    }

    .nav-tabs-custom > .nav-tabs > li.active {
      border-top-color: #0C1B25;
    }

    .form-box .header {
      background: #0C1B25;
    }

    .skin-blue .navbar {
      background-color: #0C1B25;
    }

    .form-control:hover,.form-control:focus,.form-control-table:hover,.form-control-table:focus{
      border-color: #0C1B25 !important;
    }

    .progress-bar-light-blue,
    .progress-bar-primary {
      background-color: #0C1B25;
    }

    fieldset legend {
        color: #0C1B25;
    }

    table.calendario tr td .compromissos{
        background: #0C1B25;
    }

    .btn.btn-primary {
      border-color: #0C1B25;
    }

    .btn.btn-primary:hover,
    .btn.btn-primary:active,
    .btn.btn-primary.hover {
      background-color: #0C1B25;
    }

    .skin-blue .logo {
      background-color: #0C1B25;
    }


    div.token-input-dropdown ul li.token-input-selected-dropdown-item {
        background: #0C1B25 !important;
    }

    .text-info {
      color:#0C1B25;
    }
    .text-info:hover {
      color:#1F4968;
    }

    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
      background-color: #0C1B25;
    }

    .wizard li.active span.round-tab {
        border: 2px solid #0C1B25;
    }

    .wizard li.active span.round-tab i{
        color: #0C1B25;
    }

    .wizard li:after{
        border-bottom-color: #0C1B25;
    }

    .wizard li.active:after {
        border-bottom-color: #0C1B25;
    }

    .ui-button.ui-state-active:hover {
    	background: #0C1B25 !important;
    }
</style> 
<!--codigo para imprimir via javascritp -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-imprimir").click(function() {
            $('#tab1').printElement();
        });
    });
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detalhe
            <small>Chamado</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php }; ?> </li>
        </ol>
    </section>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="row-fluid" style="margin-top:0">
                    <div class="span12">

                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-tags"></i> </span>
                                <h5>Detalhes do Chamado</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                                    <ul class="nav nav-tabs responsive">
                                        <li class="dropdown pull-right tabdrop hide"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-align-justify"></i> <b class="caret"></b></a>
                                            <ul class="dropdown-menu"></ul>
                                        </li>
                                        <li class="active"><a href="#dados-gerais" data-toggle="tab">Dados gerais</a></li>
                                        <li class=""><a href="#historico" data-toggle="tab">Histórico</a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="dados-gerais">
                                        <div class="col-sm-12 col-lg-12 col-md-12">
                                                <br>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: right; width: 11%"><strong> <b>Nº Chamado:</b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->id_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Data chamado: </b></strong></td>
                                                        <td><?php echo formataVisao($os[0]->dt_os); ?></td>
                                                        <td style="text-align: right;"><b>Horário: </b></td>
                                                        <td colspan="5"><?php echo $os[0]->hr_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Funcionário: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->nm_funcionario; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Secretaria: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->nm_secretaria; ?></td>
                                                        <td style="text-align: right"><strong><b>Setor: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->nm_setor; ?></td>
                                                        <td style="text-align: right"><strong><b>Local: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->nm_local; ?></td>
                                                        <td style="text-align: right"><strong><b>Ramal: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->n_ramal; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Título: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->titulo_os; ?></td>
                                                    </tr>
                                                    <?php //pesquisa para saber quem foi o filha da puta que atendeu essa porra p/ passar informação p/ cliente pau no cu
                                                    $id_tecnico = $os[0]->id_tecnico;
                                                    $like = array(
                                                        'id_tecnico' => $id_tecnico
                                                    );
                                                    $tecnico = $this->Mos->listaOsTecnicoLike($p = 0, $por_pagina = null, 'v_tecnico', '*', $like, $porData = null, $order = null);   ?>
                                                    <tr>
                                                        <td style="text-align: right"><strong> <b>Problema: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->df_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong> <b>Observação: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->ob_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; width: 13%"><strong>Status:</strong></td>
                                                        <td colspan="7"><?php if ($os[0]->st_os == 'Fechado') {
                                                                            echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Cancelado') {
                                                                            echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Aberto') {
                                                                            echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Resolvendo') {
                                                                            echo '<div class="badge badge-info" >' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Parado') {
                                                                            echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left;"><strong>Data Atendimento:</strong></td>
                                                        <td><?php if (isset($os[0]->dt_atendimento)) {
                                                                echo formataVisao($os[0]->dt_atendimento);
                                                            } else {
                                                                echo 'Aguardando...';
                                                            } ?> </td>

                                                        <td style="text-align: right"><strong>Horário:</strong></td>
                                                        <td colspan="1"><?php if (isset($os[0]->hr_atendimento)) {
                                                                            echo $os[0]->hr_atendimento;
                                                                        } else {
                                                                            echo 'Aguardando...';
                                                                        } ?></td>
                                                        <td colspan="" style="text-align: right;"><strong>Técnico Responsável:</strong></td>
                                                        <td colspan="3"> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                                                echo 'Aguardando Atendimento...';
                                                                            } else {
                                                                                echo   $tecnico['0']->nm_funcionario;
                                                                            } ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: right;"><strong>Solução:</strong></td>
                                                        <td colspan="8"><?php echo $os[0]->ld_tecnico; ?></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                
                                            
                                    
                                        <div class="tab-pane" id="historico">
                                            <div class="col-sm-12 col-lg-12 col-md-12">
                                                <br>
                                           
                                                            <!--aqui vai o looping com a  atualização dos chamados -->
                                                          
                                                            <table class="table table-bordered table-condensed">
                                                                     <thead>
                                                                     <th width="7%" style="text-align: center;"><strong>Data Atendimento</strong></th>
                                                                   
                                                                     <th width="1%" ><strong>Status:</strong></th>
                                                                     <th  width="40%" style="text-align: center;"><strong>Solução</strong></th>
                                                                     <th  width="10%" style="text-align: center;"><strong>Técnico Responsável</strong></th>

                                                                     </thead>                
                                                                <?php if (isset($movimentacao)) {
                                                                    foreach ($movimentacao as $movi) { ?>
                                                                   
                                                                                    <tr>
                                                                                    <td style="text-align:center;"><?php if (isset($movi->dt_movimentacao)) {
                                                                                                echo formataVisao($movi->dt_movimentacao);
                                                                                            } else {
                                                                                                echo 'Aguardando...';
                                                                                            } ?> 

                                                                                         
                                                                                         <?php if (isset($movi->hr_movimentacao)) {
                                                                                                            echo '- '.$movi->hr_movimentacao;
                                                                                                        } else {
                                                                                                            echo 'Aguardando...';
                                                                                                        } ?></td>
                                                                                      
                                                                                        <td style="text-align:center;"><?php if ($movi->st_os == 'Fechado') {
                                                                                                            echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                                                                        }
                                                                                                        if ($movi->st_os == 'Cancelado') {
                                                                                                            echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $movi->st_os . '</div>';
                                                                                                        }
                                                                                                        if ($movi->st_os == 'Aberto') {
                                                                                                            echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $movi->st_os . '</div>';
                                                                                                        }
                                                                                                        if ($movi->st_os == 'Resolvendo') {
                                                                                                            echo '<div class="badge badge-info" >' . $movi->st_os . '</div>';
                                                                                                        }
                                                                                                        if ($movi->st_os == 'Parado') {
                                                                                                            echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $movi->st_os . '</div>';
                                                                                                        }
                                                                                                        ?></td>
                                                                                    
                                                                                  
                                                                                        <td ><?php echo $movi->ld_movimentacao; ?></td>
                                                                                      
                                                                                      
                                                                                        <td> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                                                                                echo 'Aguardando Atendimento...';
                                                                                                            } else {
                                                                                                                echo   $tecnico['0']->nm_funcionario;
                                                                                                            } ?></td>
                                                                                   
                                                                                      
                                                                                   
                                                                                        
                                                                                           <?php }
                                                                } ?>
                                                                        </tr>
                                                                                   
                                                       
                                                                                   </table>
                                                                                   <br>
                                                                                        
                                                          
                                            </div>
                                        </div>
                                    </div>
                                </div> .
                            </div>
                        </div>

                    



                        <div class="modal-footer">

                            <a class="btn btn-navy align-right" onclick="$('#btn-imprimir').printElement()" id="btn-imprimir"><i class="fa fa-print"></i> Imprimir</a>
                            <a class="btn btn-default align-right" href="<?php echo  base_url() ?>index.php/painel"><i class="fa fa-close"></i> Fechar</a>

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
</div>
</div> 

</div>
</div>
</div>




</div>