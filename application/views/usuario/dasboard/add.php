<style>
     #carregando div,#carregando-nfc div {
        border-color: #f3f3f3;
        border-top-color: #0C1B25;
        border-bottom-color: #0C1B25;
    }

    #bem-vindo .modal-header, #curtir-facebook .modal-header{
        background: #0C1B25;
        color: #FFFFFF;
    }

    .pace .pace-progress {
      background: #1F4968;
    }

    a {
      color: #0C1B25;
    }

    a:hover,
    a:active,
    a:focus {
      color: #1F4968    }

    .text-primary{
        color: #0C1B25    }

    .text-primary:hover{
        color: #1F4968    }

    .cor-padrao{
      color: #0C1B25    }

    .flash-tab{
      background: #0C1B25;
    }

    .dropdown-menu > li > a:hover {
      background-color: #0C1B25;
    }

    .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
      background: #0C1B25;
    }

    .box.box-primary {
      border-top-color: #0C1B25;
    }

    .box.box-solid.box-primary > .box-header {
      background: #0C1B25;
      background-color: #0C1B25;
    }

    .box .todo-list > li.primary {
      border-left-color: #0C1B25;
    }

    .bg-light-blue {
      background-color: #0C1B25 !important;
    }

    .bg-olive {
      background-color: #0C1B25 !important;
    }

    .text-light-blue {
      color: #0C1B25 !important;
    }

    .text-olive {
      color: #0C1B25 !important;
    }

    .btn.btn-primary {
      background-color: #0C1B25;
      border-color: #1F4968;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
      background-color: #1F4968;
      border-color: #0C1B25;
    }

    .nav.nav-pills > li.active > a,
    .nav.nav-pills > li.active > a:hover {
      border-top-color: #0C1B25;
      color: #444;
    }

    .nav.nav-pills.nav-stacked > li.active > a,
    .nav.nav-pills.nav-stacked > li.active > a:hover {
      border-left-color: #0C1B25;
    }

    .nav-tabs-custom > .nav-tabs > li.active {
      border-top-color: #0C1B25;
    }

    .form-box .header {
      background: #0C1B25;
    }

    .skin-blue .navbar {
      background-color: #0C1B25;
    }

    .form-control:hover,.form-control:focus,.form-control-table:hover,.form-control-table:focus{
      border-color: #0C1B25 !important;
    }

    .progress-bar-light-blue,
    .progress-bar-primary {
      background-color: #0C1B25;
    }

    fieldset legend {
        color: #0C1B25;
    }

    table.calendario tr td .compromissos{
        background: #0C1B25;
    }

    .btn.btn-primary {
      border-color: #0C1B25;
    }

    .btn.btn-primary:hover,
    .btn.btn-primary:active,
    .btn.btn-primary.hover {
      background-color: #0C1B25;
    }

    .skin-blue .logo {
      background-color: #0C1B25;
    }


    div.token-input-dropdown ul li.token-input-selected-dropdown-item {
        background: #0C1B25 !important;
    }

    .text-info {
      color:#0C1B25;
    }
    .text-info:hover {
      color:#1F4968;
    }

    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
      background-color: #0C1B25;
    }

    .wizard li.active span.round-tab {
        border: 2px solid #0C1B25;
    }

    .wizard li.active span.round-tab i{
        color: #0C1B25;
    }
    label {
    display: inline-block;
    font-weight: bold;
}
    .wizard li:after{
        border-bottom-color: #0C1B25;
    }

    .wizard li.active:after {
        border-bottom-color: #0C1B25;
    }

    .ui-button.ui-state-active:hover {
    	background: #0C1B25 !important;
    }
</style> 
<!--codigo para imprimir via javascritp -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-imprimir").click(function() {
            $('#tab1').printElement();
        });
    });
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>
            Chamados
            <small> Adicionar chamado</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php
                                                                                                                                                                                                                                                                                    }; ?> </li>
        </ol>
    </section>





    <section class="content">
        <?php
        get_msg('salvo');
        ?>
        <!-- Default box -->



        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid" style="margin-top:0">
                        <div class="span12">
                            <div class="widget-box">
                                <div class="widget-title"> <span class="icon"> <i class="icon-tags"></i> </span>
                                    <h5>Chamados</h5>
                                </div>
                                <div class="widget-content nopadding">
                                    <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                                        <ul class="nav nav-tabs">
                                            <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Abrir Chamado</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">
                                                <div class="span12" id="divCadastrarOs">
                                                    <form action="<?php echo base_url(); ?>index.php/painel/save-chamado" method="post" id="formOs">
                                                        
                                                       
                                                           
                                                            <input id="dt_os" name="dt_os" class="col-lg-3 datepicker form-control " type="hidden" value="<?php echo date('d/m/Y '); ?>" type="text" name="dt_os" />
                                                        
                                                       
                                                        
                                                            <input id="hr_os" name="hr_os" class="col-lg-2 datepicker form-control" type="hidden" value="<?php echo date("H:i:s"); ?>" name="hr_os" />
                                

                                                            <input id="cid_funcionario" class="span12 form-control" type="hidden" name="cid_funcionario" value="<?php echo $funcionario[0]->id_funcionario; ?>" /> 
                                                      
                                                            <input id="id_tecnico" value="1" class="col-lg-12 form-control" type="hidden" name="id_tecnico" /> 

                                                  


                                                            <input type="hidden" class="col-lg-4 form-control" name="st_os" value="Aberto" id="st_os">



                                                        
                                                        <div class="col-lg-12">
                                                            </br>
                                                        </div>
                                                                                                                                                                                                                                                                                      
                                                       
                                                        <div class="col-lg-12 " style="padding: 1%;">
                                                        <div class="col-lg-12  " style="padding:1%;" >  <label for="dataFinal">Título</label>
                                                            <input id="titulo_os" name="titulo_os" class="col-lg-2 datepicker form-control  "  type="text" />
                                                            
                                                        </div>
                                                            <div class="col-lg-6">
                                                                <label for="descricaoProduto">Problema </label>
                                                                <textarea class="col-lg-12 form-control  " name="df_os" id="df_os" cols="30" rows="5"></textarea>
                                                            </div>
                                                            <div class="col-lg-6"> <label for="defeito">Observações </label>
                                                                <textarea class="col-lg-12 form-control   " name="ob_os" id="ob_os" cols="30" rows="5"></textarea> </div>
                                                        </div>


                                                        <div class="col-lg-12 text-right" style="padding: 1%; ">
                                                            <div class=" text-right">
                                                                <button class="btn btn-success" id="btnContinuar"><i class="fa fa-check"></i> Salvar</button> 
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> .
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal-->


</div>