<?php


class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $Image=base_url('assets/img/logo.png');
    $this->Image($Image,10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','',10);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,1,'ERPOS |Sistema de chamados',0,0,'C');
    $this->Ln(5);
    $this->Cell(80);
    $this->SetFont('Arial','',8);
    $this->Cell(30,1,'CPD/TI Ramais: 500,476 e 213',0,0,'C');
    $this->Ln(10);
    $this->Cell(80);
    $this->SetFont('Arial','B',10);
    $this->Cell(30,1,utf8_decode('Detalhe do Chamado'),0,0,'C');
    $this->Ln(5);
    $this->SetFont('Arial','',9);
    $this->Cell(30,1,'ERPOS | Nome Empresa aqui',0,0,'C');
    $this->Cell(100);
    $this->Cell(30,1,'Gerado: '.date('d/m/Y').' as '.date('H:i:s').'',0,0,'');
    $this->SetFont('Arial','B',15);
    $this->Cell(30,1,'__________________________________________________________________________________________________________________________________',0,0,'C');
    // Line break
    $this->Ln(5);
}

function body($os,$tecnico){
$this->SetFont('Arial','B',10); 
 $this->Cell(30,10,'Chamado: '.$os[0]->id_os.'',0,0,'');   
 $this->Ln(10);
 $this->SetFont('Arial','',9); 
 $this->Cell(30,1,utf8_decode('Data: '.formataVisao($os[0]->dt_os).'              Horário: '.$os[0]->hr_os.''),0,0,'');
 $this->Ln(5);
 $this->Cell(30,1,utf8_decode('Funcionário: '.$os[0]->nm_funcionario.''),0,0,'');
 $this->Cell(40);
 $this->Cell(30,1,utf8_decode('Secretaria: '.$os[0]->nm_secretaria.''),0,0,'');
 $this->Cell(10);
  $this->Ln(5);
 $this->Cell(30,1,utf8_decode('Setor: '.$os[0]->nm_setor.''),0,0,'');   
 $this->Ln(5);
 $this->Cell(30,1,utf8_decode('Local: '.$os[0]->nm_local.''),0,0,'');  
 //$this->Cell();  
 $this->Cell(30,1,utf8_decode('Ramal/Tel: '.$os[0]->n_ramal.''),0,0,'');
 $this->Ln(5);
 $this->SetFont('Arial','B',9);
 $this->MultiCell(0,5,utf8_decode('Problema: '),'','','');
 $this->Cell(15);
 $this->SetFont('Arial','',9); 
 $this->MultiCell(0,5,utf8_decode(''.$os[0]->df_os.''),'','L','');
 $this->Ln(5);
 $this->SetFont('Arial','',9);
 $this->MultiCell(0,5,utf8_decode('Obs: '.$os[0]->ob_os.''),'B','L','');
 $this->SetFont('Arial','',9);
 $this->Ln(5);
 if(isset($os[0]->dt_atendimento)){
 $this->Cell(30,1,utf8_decode('Data Atendimento: '.formataVisao($os[0]->dt_atendimento).'      Horário: '.$os[0]->hr_os.'     Status: '.$os[0]->st_os.'              Técnico: '.$tecnico[0]->nm_funcionario.''),0,0,'');   
 }else{
    $this->Cell(30,1,utf8_decode('Data Atendimento:               Horário:          Status: '.$os[0]->st_os.'              Técnico:                  '),0,0,'');      
 }
 $this->Ln(5);
 $this->MultiCell(0,5,utf8_decode('Solução: '.$os[0]->ld_tecnico.''),'','L','');
 $this->Footer();

}

}
// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Página '.$this->PageNo().'/{nb}',0,0,'C');
}
// Simple table





// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->Footer();
$pdf->AddPage();

//$pdf->SetFont('Arial','',10);
//busca por tecnico 


$pdf->body($os,$tecnico);
$pdf->Output();