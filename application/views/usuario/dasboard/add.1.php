<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>
            Chamados
            <small> Adicionar chamado</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php
                                                                                                                                                                                                                                                                                    }; ?> </li>
        </ol>
    </section>





    <section class="content">
        <?php
        get_msg('salvo');
        ?>
        <!-- Default box -->



        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid" style="margin-top:0">
                        <div class="span12">
                            <div class="widget-box">
                                <div class="widget-title"> <span class="icon"> <i class="icon-tags"></i> </span>
                                    <h5>Chamados</h5>
                                </div>
                                <div class="widget-content nopadding">
                                    <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                                        <ul class="nav nav-tabs">
                                            <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Abrir Chamado</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">
                                                <div class="span12" id="divCadastrarOs">
                                                    <form action="<?php echo base_url(); ?>index.php/painel/save-chamado" method="post" id="formOs">
                                                        
                                                        <div class="col-lg-3"> <label for="dataInicial">Data chamado<span class="required">*</span></label>
                                                            <input id="dt_os" class="col-lg-3 datepicker form-control " disabled="true" value="<?php echo date('d/m/Y '); ?>" type="text" name="dt_os" /> -->
                                                            <input id="dt_os" name="dt_os" class="col-lg-3 datepicker form-control " type="hidden" value="<?php echo date('d/m/Y '); ?>" type="text" name="dt_os" />
                                                        </div>
                                                        <div class="col-lg-3"> <label for="dataFinal">Hora chamado</label>
                                                            <input id="hr_os" name="hr_os" class="col-lg-2 datepicker form-control" disabled type="text" value="<?php echo date("H:i:s"); ?>" name="hr_os" />
                                                            <input id="hr_os" name="hr_os" class="col-lg-2 datepicker form-control" type="hidden" value="<?php echo date("H:i:s"); ?>" name="hr_os" />
                                                        </div>
                                                        

                                                        <div class="col-lg-3"> <label for="cliente">Funcionário<span class="required"></span></label>
                                                            <input id="nm_funcionario" class="span12 form-control" type="text" name="nm_funcionario" disabled value="<?php echo $funcionario[0]->nm_funcionario; ?> " />

                                                            <input id="cid_funcionario" class="span12 form-control" type="hidden" name="cid_funcionario" value="<?php echo $funcionario[0]->id_funcionario; ?>" /> </div>
                                                        <div class="col-lg-3"> <label for="tecnico">Técnico / Responsável<span class="required "></span></label>
                                                            <select class="form-control select2" disabled name="id_tecnico" style="width: 100%;">
                                                                <option selected="selected" value="1">Em Aberto</option>

                                                            </select>
                                                            <input id="id_tecnico" value="1" class="col-lg-12 form-control" type="hidden" name="id_tecnico" /> </div>

                                                        <div class="col-lg-12" style="padding: 1%">
                                                            <div class="col-lg-3"> </br><label for="cliente">Secretaria<span class="required">*</span></label>
                                                                <input id="nm_secretaria" class="span12 form-control" type="text" name="nm_secretaria" disabled value="<?php echo $funcionario[0]->nm_secretaria; ?> " />
                                                            </div>
                                                            <div class="col-lg-3"></br> <label for="cliente">Setor<span class="required">*</span></label>
                                                                <input id="nm_setor" class="span12 form-control" type="text" name="nm_setor" disabled value="<?php echo $funcionario[0]->nm_setor; ?> " />
                                                            </div>
                                                            <div class="col-lg-3"></br> <label for="cliente">Ramal/Telefone<span class="required">*</span></label>
                                                                <input id="n_ramal" class="span12 form-control" type="text" name="n_ramal" disabled value="<?php echo $funcionario[0]->n_ramal; ?> " />
                                                            </div>
                                                            <div class="col-lg-3"></br> <label for="cliente">Local<span class="required">*</span></label>
                                                                <input id="nm_local" class="span12 form-control" type="text" name="nm_local" disabled value="<?php echo $funcionario[0]->nm_local; ?> " />
                                                            </div>



                                                            <input type="hidden" class="col-lg-4 form-control" name="st_os" value="Aberto" id="st_os">



                                                        </div>
                                                        <div class="col-lg-12">
                                                            </br>
                                                        </div>
                                                                                                                                                                                                                                                                                      
                                                       
                                                        <div class="col-lg-12 " style="padding: 1%;">
                                                        <div class="col-lg-12 text-uppercase" style="padding:1%;" >  <label for="dataFinal">Título</label>
                                                            <input id="titulo_os" name="titulo_os" class="col-lg-2 datepicker form-control text-uppercase"  type="text" />
                                                            
                                                        </div>
                                                            <div class="col-lg-6">
                                                                <label for="descricaoProduto">Problema </label>
                                                                <textarea class="col-lg-12 form-control text-uppercase" name="df_os" id="df_os" cols="30" rows="5"></textarea>
                                                            </div>
                                                            <div class="col-lg-6"> <label for="defeito">Observações </label>
                                                                <textarea class="col-lg-12 form-control text-uppercase " name="ob_os" id="ob_os" cols="30" rows="5"></textarea> </div>
                                                        </div>


                                                        <div class="col-lg-12 text-right" style="padding: 1%; ">
                                                            <div class=" text-right">
                                                                <button class="btn btn-primary" id="btnContinuar"><i class="fa fa-check"></i> Salvar</button> 
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> .
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal-->


</div>