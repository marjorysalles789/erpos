<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ERPOS | Administração</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  
  
 
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
  <!--Form validation -->
  <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
  <script src="<?php echo base_url();?>assets/js/form_validation.js"></script>


  
  <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/matrix-media.css"> -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/matrix-style.css"> 
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/dist/css/matrix-media.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/font-awesome.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/jquery-ui-1.9.2.custom.css">
  <!--full  calendar -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/dist/css/fullcalendar.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/dist/css/jquery-ui.css" />
 
 <!-- Select2 -->
 <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->


<body class="hold-transition skin-purple sidebar-collapse ">

<div class="wrapper">

  <!-- Main Header -->
 
  <header class="main-header">
  
    <!-- Logo -->
    <a href="<?php echo base_url();?>index.php/painel" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>ERP</b>OS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ERP</b>OS</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <li class="dropdown user user-menu ">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
             
              <span class="hidden-xs">Meus dados</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
               

                <p>
                   <?php echo 
                   $this->session->userdata('nm_funcionario');
                    ?>
                 </p>
                 <p class="text-left">   
                  <small>Cargo: <?php echo $this->session->userdata('nm_cargo');  ?></small>
                  <small>Secretaria: <?php echo $this->session->userdata('nm_secretaria');  ?></small>
                  <small>Setor: <?php echo $this->session->userdata('nm_setor');  ?></small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo base_url();?>index.php/usuario/logout"  class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sair</a>
                </div>
                
                <div class="pull-right">
  </li>     
        </ul>
      </div>
    </nav>
  </header>
  
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        
        <!-- Optionally, you can add icons to the links -->
        <li class="<?php if ($this->uri->segment(1)==''){ echo 'active';} ?>"><a href="<?php echo base_url();?>index.php/painel"><i class="fa fa-dashboard"></i> <span>Dasboard</span></a></li>
       
      
        <li class="<?php if ($this->uri->segment(1)=='os'){ echo 'active';} ?>" ><a href="<?php echo base_url();?>index.php/painel"><i class="fa fa-line-chart"></i> <span> Meus Chamados</span></a></li>
  <li ><a href="<?php echo base_url();?>index.php/usuario/logout" ><i class="fa fa-sign-out"></i> <span>Sair</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

