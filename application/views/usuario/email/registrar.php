<div class="rps_acfc">
<div leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" style="margin:0; padding:0; height:100%; width:100%; background-color:#F7F7F7">
<div style="display:none; max-height:0px; overflow:hidden"></div>
<center>
<table id="x_bodyTable" style="border-collapse:collapse; height:100%; margin:0; padding:0; width:100%; background-color:#F7F7F7" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody>
<tr>
<td id="x_bodyCell" style="height:100%; margin:0; padding:40px; width:100%; font-family:Helvetica,Arial,sans-serif; line-height:160%" valign="top" align="center">
<span class="x_hidden" style="color:#FFFFFF; font-size:0; height:0"></span><table id="x_templateContainer" style="border-collapse:collapse; width:600px; background-color:#FFFFFF; border:1px solid #D9D9D9" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%" valign="top" align="center">
<table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody>
<tr>
<td class="x_nuHeader" style="background-color:#FFFFFF; font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-top:40px; padding-bottom:40px; background:#FFF" align="center">
<img data-imagetype="External" src="https://images.nubank.com.br/nubank-new.png" alt="Nubank" id="x_nuLogo" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none; max-width:80px; width:80px" width="80">
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%">
<table id="x_templateHeader" style="border-collapse:collapse; background-color:#FFFFFF; border-top:1px solid #FFFFFF; border-bottom:1px solid #FFFFFF" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td class="x_headerContent" style="font-family:Helvetica,Arial,sans-serif; line-height:100%; color:#505050; font-size:20px; font-weight:bold; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; text-align:left; vertical-align:middle" valign="top">
<img data-imagetype="External" src="https://images.nubank.com.br/header_card_activated.png" alt="Seu cartão foi ativado" id="x_headerImage" style="max-width:600px; border:0; height:auto; line-height:100%; outline:none; text-decoration:none">
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="x_bodyContent" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; color:#404040; font-size:16px; padding-top:64px; padding-bottom:60px; padding-right:72px; padding-left:72px; background:#FFFFFF">
<table id="x_templateBody" style="border-collapse:collapse; background-color:#FFFFFF" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:32px; text-align:center" valign="top">
<h2 style="display:block; font-family:Helvetica,Arial,sans-serif; font-style:normal; font-weight:bold; line-height:100%; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; text-align:center; color:#404040; font-size:20px">
Olá <strong class="x_highlight" style="color:#8D3DC8; font-weight:600">Wesley</strong>!</h2>
</td>
</tr>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:32px; text-align:center" valign="top">
<p style="margin:0">Agora que seu <strong>cartão novo foi ativado</strong>, o seu cartão antigo foi cancelado automaticamente. Fizemos uma colinha com tudo que você precisa saber:</p>
</td>
</tr>
<tr>
<td class="x_closing x_sectionFeatures" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; margin-bottom:0; padding-bottom:0; text-align:center" valign="top">
<table class="x_closing" style="border-collapse:collapse; margin-bottom:0; padding-bottom:0" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:32px; text-align:center; padding-left:8%; padding-right:8%" valign="top">
<img data-imagetype="External" src="https://images.nubank.com.br/icons/card.png" class="x_feature-icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none; padding-bottom:16px; display:inline; max-width:560px" width="40">
<p style="margin:0">A numeração do seu cartão é diferente, mas a sua <strong>senha de compras continua a mesma.</strong></p>
</td>
</tr>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:32px; text-align:center; padding-left:8%; padding-right:8%" valign="top">
<img data-imagetype="External" src="https://images.nubank.com.br/icons/contactless.png" class="x_feature-icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none; padding-bottom:16px; display:inline; max-width:560px" width="55">
<p style="margin:0"><strong>Aproxime e pague:</strong> depois de fazer a primeira compra inserindo o cartão na maquininha, nas próximas é só aproximar o cartão e está pago. Sem pedido de senha para compras abaixo de R$50.</p>
</td>
</tr>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:32px; text-align:center; padding-left:8%; padding-right:8%" valign="top">
<img data-imagetype="External" src="https://images.nubank.com.br/icons/mobile.png" class="x_feature-icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none; padding-bottom:16px; display:inline; max-width:560px" width="45">
<p style="margin:0">Com o novo cartão, nada muda no acesso ao aplicativo, geração de boletos e faturas.</p>
</td>
</tr>
<tr>
<td style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:32px; text-align:center; padding-left:8%; padding-right:8%" valign="top">
<img data-imagetype="External" src="https://images.nubank.com.br/icons/abu.png" class="x_feature-icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none; padding-bottom:16px; display:inline; max-width:560px" width="45">
<p style="margin:0">Caso tenha desativado o serviço de atualização automática, não se esqueça de
<strong>atualizar os dados do seu novo cartão nos seus sites, apps e serviços online preferidos.</strong></p>
</td>
</tr>
<tr>
<td class="x_closing" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; margin-bottom:0; padding-bottom:0; text-align:center; padding-left:8%; padding-right:8%">
<p class="x_closing" style="margin:0; margin-bottom:0; padding-bottom:0">Abraços,<br>
Equipe Nubank</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody>
</table>
<br><table id="x_atendimento" style="border-collapse:collapse" width="600px" cellspacing="0" cellpadding="0" border="0">

<tbody>
<tr>
<td class="x_footerContent" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; background:#FFFFFF">
<table id="x_templateFooter" style="border-collapse:collapse; background-color:#FFFFFF" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td class="x_social" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding-bottom:16px; text-align:center; background:#f7f7f7; padding:10px 0px 40px 0px">
<a href="https://mandrillapp.com/track/click/30052082/www.facebook.com?p=eyJzIjoiRFcxOXZ0QlU2a0RYaUlZbWNOcDhwa01ZdEFRIiwidiI6MSwicCI6IntcInVcIjozMDA1MjA4MixcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy5mYWNlYm9vay5jb21cXFwvbnViYW5rXCIsXCJpZFwiOlwiNmI1OWNhYjljNTJlNDdjYzg2MjM5NDkwZTdmOGZlZmJcIixcInVybF9pZHNcIjpbXCI3YjI3Y2NhZDU1NWUwOWY2MjdkMDY0N2I5NDJlYzMyNmRhZDM0ZDdkXCJdfSJ9" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="color:#8D3DC8; font-weight:bold; text-decoration:none"><img data-imagetype="External" src="https://images.nubank.com.br/ico-facebook-grey.png" alt="Facebook" class="x_icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none" width="40">
</a>&nbsp;&nbsp; <a href="https://mandrillapp.com/track/click/30052082/twitter.com?p=eyJzIjoiTWppc2hRcmhmUFAzbmFvdVpDR0I3UUQ0enpzIiwidiI6MSwicCI6IntcInVcIjozMDA1MjA4MixcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3R3aXR0ZXIuY29tXFxcL251YmFua1wiLFwiaWRcIjpcIjZiNTljYWI5YzUyZTQ3Y2M4NjIzOTQ5MGU3ZjhmZWZiXCIsXCJ1cmxfaWRzXCI6W1wiOTBmMTYwODYyMWQ3MzA1MWUxNzJhYzZmNzU3YjI3ZmUyYTRmNDgwOFwiXX0ifQ" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="color:#8D3DC8; font-weight:bold; text-decoration:none">
<img data-imagetype="External" src="https://images.nubank.com.br/ico-twitter-grey.png" alt="Twitter" class="x_icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none" width="40">
</a>&nbsp;&nbsp; <a href="https://mandrillapp.com/track/click/30052082/www.youtube.com?p=eyJzIjoiSmVDbVJTb3liYUNyMHAyWHdOd0pPQWp6X3lRIiwidiI6MSwicCI6IntcInVcIjozMDA1MjA4MixcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy55b3V0dWJlLmNvbVxcXC9jaGFubmVsXFxcL1VDZ3NEWDNoVHdpUGR0R0hKak1GZkR4Z1wiLFwiaWRcIjpcIjZiNTljYWI5YzUyZTQ3Y2M4NjIzOTQ5MGU3ZjhmZWZiXCIsXCJ1cmxfaWRzXCI6W1wiZmM0YzcyMzZiNTY1NjkxMzg5N2QzODc4ZWFhMTQ1ZDYyYTU5ZmRjYlwiXX0ifQ" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="color:#8D3DC8; font-weight:bold; text-decoration:none">
<img data-imagetype="External" src="https://images.nubank.com.br/ico-youtube-grey.png" alt="YouTube" class="x_icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none" width="40">
</a>&nbsp;&nbsp; <a href="https://mandrillapp.com/track/click/30052082/www.instagram.com?p=eyJzIjoid0RlYXRvVzY4YjVySTNWZDk4V2RRR2JkSjRNIiwidiI6MSwicCI6IntcInVcIjozMDA1MjA4MixcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy5pbnN0YWdyYW0uY29tXFxcL251YmFua1xcXC9cIixcImlkXCI6XCI2YjU5Y2FiOWM1MmU0N2NjODYyMzk0OTBlN2Y4ZmVmYlwiLFwidXJsX2lkc1wiOltcIjIwNzRhZGRlODFmOTYyNDliNWE1NTE4ZDUzYTllNzcwNzBjZGJiMGJcIl19In0" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="color:#8D3DC8; font-weight:bold; text-decoration:none">
<img data-imagetype="External" src="https://images.nubank.com.br/ico-instagram-grey.png" alt="Instagram" class="x_icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none" width="40">
</a>&nbsp;&nbsp; <a href="https://mandrillapp.com/track/click/30052082/www.linkedin.com?p=eyJzIjoiNENWdFNiTHpJZ2ItZWh2MTBPS3NsbHFjdzdZIiwidiI6MSwicCI6IntcInVcIjozMDA1MjA4MixcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy5saW5rZWRpbi5jb21cXFwvY29tcGFueVxcXC9udWJhbmtcIixcImlkXCI6XCI2YjU5Y2FiOWM1MmU0N2NjODYyMzk0OTBlN2Y4ZmVmYlwiLFwidXJsX2lkc1wiOltcImU0ZjRiMTg2YzQ3MmZhYWRkMzk2M2VhYjdhNjY5OWRhNTU3ODc4YmVcIl19In0" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="color:#8D3DC8; font-weight:bold; text-decoration:none">
<img data-imagetype="External" src="https://images.nubank.com.br/ico-linkedin-grey.png" alt="Linkedin" class="x_icon" style="border:0; height:auto; line-height:100%; outline:none; text-decoration:none" width="40">
</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="x_support" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding:0px 0px 0px 0px; background:#f7f7f7; color:#777; font-size:13px; text-align:center">
<p class="x_spaceS" style="margin:0; padding-bottom:8px"><strong>Comunidade</strong></p>
<p class="x_spaceS" style="margin:0; padding-bottom:8px">Tem alguma dúvida? A nossa comunidade pode te ajudar. Visite:
<a href="https://mandrillapp.com/track/click/30052082/comunidade.nubank.com.br?p=eyJzIjoiQUxmeWFKemR2dFROLWljbjBkYWpUQ0w0U3E4IiwidiI6MSwicCI6IntcInVcIjozMDA1MjA4MixcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL2NvbXVuaWRhZGUubnViYW5rLmNvbS5iclxcXC9cIixcImlkXCI6XCI2YjU5Y2FiOWM1MmU0N2NjODYyMzk0OTBlN2Y4ZmVmYlwiLFwidXJsX2lkc1wiOltcIjVhMGE1NTA1Yjg5ZjRlOTc2NTRlYjFhYjY2YzVjNjc4MzJmMzFlYmRcIl19In0" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" class="x_footer-link" style="color:#8D3DC8; font-weight:bold; text-decoration:underline">
comunidade.nubank.com.br</a></p>
</td>
</tr>
<tr>
<td class="x_support" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding:0px 0px 0px 0px; background:#f7f7f7; color:#777; font-size:13px; text-align:center">
<p class="x_spaceS" style="margin:0; padding-bottom:8px"><strong>Atendimento 24 horas</strong></p>
<p class="x_spaceS" style="margin:0; padding-bottom:8px">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no
<a href="mailto:meajuda@nubank.com.br" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" class="x_footer-link" style="color:#8D3DC8; font-weight:bold; text-decoration:underline">
meajuda@nubank.com.br</a></p>
<p class="x_spaceS" style="margin:0; padding-bottom:8px">Para urgências ligue para 0800 591 2117. Atendimento 24 horas, todos os dias.</p>
</td>
</tr>
</tbody>
</table>
<table style="border-collapse:collapse" width="600px">
<tbody>
<tr>
<td class="x_support" style="font-family:Helvetica,Arial,sans-serif; line-height:160%; padding:0px 0px 0px 0px; background:#f7f7f7; color:#777; font-size:13px; text-align:center">
<br>
<p style="margin:0">Nubank 2019</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center>
<img data-imagetype="External" src="https://mandrillapp.com/track/open.php?u=30052082&amp;id=6b59cab9c52e47cc86239490e7f8fefb" width="1" height="1">
</div>
</div>