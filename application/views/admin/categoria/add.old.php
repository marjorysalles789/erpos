<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categoria
            <small>Nova Categoria</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php }; ?>  </li>
        </ol>
    </section>





    <section class="content">
    <?php
        get_msg('salvo');
        ?>
        <!-- Default box -->
        <div class="">
            <div class="">

              
                
            
            <div class="box-body">
                <!--aqui vem a tabela que vai vir do banco de dados -->

                <div class="row">
                    <!-- left column -->
                    
                <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Cadastrar Categoria</h5>
          </div>
          <form role="form" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/categoria/save'; ?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Categoria</label>

                                    <input type="text" class="form-control     " id="txtnome" name="nome" value="<?php echo set_value('nm_categoria'); ?>" placeholder="Nome.">
                                    <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                                </div>
                                
                                   
                                

                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <button type="submit" class="btn btn-primary "><i class="fa fa-check"></i> Adicionar</button>
                                </div>
                        </form>
         
       
                
                </section>

                </div>


            <!-- /.content-wrapper -->




            <!--modal nova marca -->

            <script>
                $(document).ready(function () {
                    $('.date').mask('00/00/0000');
                    $('.time').mask('00:00:00');
                    $('.date_time').mask('00/00/0000 00:00:00');
                    $('.cep').mask('00000-000');
                    $('.phone').mask('0000-0000');
                    $('.phone_with_ddd').mask('(00) 0000-0000');
                    $('.phone_us').mask('(000) 000-0000');
                    $('.mixed').mask('AAA 000-S0S');
                    $('.cpf').mask('000.000.000-00', {reverse: true});
                    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
                    $('.money').mask('000.000.000.000.000,00', {reverse: true});
                    $('.money2').mask("#.##0,00", {reverse: true});
                    $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                        translation: {
                            'Z': {
                                pattern: /[0-9]/, optional: true
                            }
                        }
                    });
                    $('.ip_address').mask('099.099.099.099');
                    $('.percent').mask('##0,00%', {reverse: true});
                    $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
                    $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
                    $('.fallback').mask("00r00r0000", {
                        translation: {
                            'r': {
                                pattern: /[\/]/,
                                fallback: '/'
                            },
                            placeholder: "__/__/____"
                        }
                    });
                    $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
                });

            </script>     