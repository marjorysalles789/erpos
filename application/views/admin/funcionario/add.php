<div class="row">
    <div class="col-md-12">
        <?php
        get_msg('salvo');
        ?>
        <form id="form_funcionario" method="post"  action="<?php echo base_url() . 'index.php/funcionario/save'; ?>" class="form-horizontal">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title"><?php echo $titulo; ?></h2>
                    <p class="panel-subtitle">

                    </p>
                </header>
                <div class="panel-body">
                    <div class="form-group">

                        <label for="exampleInputEmail1">Nome</label>

                        <input type="text" class="form-control     " id="nome" name="nome" value="<?php echo set_value('nome'); ?>">
                        <span id="nm_funcionario-erro" class="text-danger  " style="display:none">Este Funcionário já esta cadatrado.</span>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>

                        <input type="email" required class="form-control     " id="mail_funcionario" onchange="verificaEmailExiste()" name="mail_funcionario" value="<?php echo set_value('mail_funcionario'); ?>">
                        <?php echo form_error('mail_funcionario', '<div class="text-danger">', '</div>'); ?>
                        <div class="Erro-email text-danger hide">Esse E-mail já está em uso!</div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nº resp</label>

                        <input type="text" class="form-control     " id="n_resp" name="n_resp" onchange="verificaNumeroResp()" value="<?php echo set_value('n_resp'); ?>">
                        <?php echo form_error('n_resp', '<div class="text-danger">', '</div>'); ?>
                        <div class="Erro-resp text-danger hide"></div>
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <select class="form-control      select2" id="cid_cargo" name="cid_cargo" style="width: 100%;">
                            <option></option>
                            <?php if ($cargo1) {
                                foreach ($cargo1 as $cargos) { ?>
                                    <option value="<?php echo $cargos->id_cargo; ?>"><?php echo $cargos->nm_cargo; ?></option>
                            <?php
                                }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Secretaria</label>
                        <select class="form-control      select2" id="id_secretaria" name="cid_secretaria" onchange="busca_setor($(this).val())" style="width: 100%;">
                            <option></option>
                            <?php if ($secretaria) {
                                foreach ($secretaria as $secretarias) { ?>
                                    <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
                            <?php
                                }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Setor</label>
                        <select class="form-control      select2" id="id_setor" name="id_setor" style="width: 100%;">


                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Local</label>

                        <input type="text" class="form-control     " id="nm_local" name="nm_local" value="<?php echo set_value('nm_local'); ?>">
                        <?php echo form_error('nm_local', '<div class="text-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nº Ramal ou Telefone</label>

                        <input type="text" class="form-control     " id="n_ramal" name="n_ramal" value="<?php echo set_value('n_ramal'); ?>">
                        <?php echo form_error('nm_local', '<div class="text-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Obs. sobre local de trabalho</label>

                        <input type="text" class="form-control     " id="obs_local" name="obs_local" value="<?php echo set_value('obs_local'); ?>">
                        <?php echo form_error('obs_local', '<div class="text-danger">', '</div>'); ?>
                    </div>
                    <label class="h5 text-capitalize  text-primary ">Dados para o Painel de usuário </label>
                
                    <div class="form-group">
                        <label for="exampleInputEmail1">Usuário</label>

                        <input type="text" class="form-control     " onchange="verificaUsuario()" id="nm_usuario" name="nm_usuario" value="<?php echo set_value('nm_usuario'); ?>">
                        <?php echo form_error('obs_local', '<div class="text-danger">', '</div>'); ?>
                        <div class="Erro-usuario text-danger hide"></div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Senha</label>

                        <input type="password" class="form-control     " id="senha" name="senha" value="<?php echo set_value('senha'); ?>">
                        <?php echo form_error('obs_local', '<div class="text-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Repita senha</label>

                        <input type="password" class="form-control     " id="confirma_senha" name="confirma_senha" value="<?php echo set_value('senha'); ?>">
                        <?php echo form_error('obs_local', '<div class="text-danger">', '</div>'); ?>
                    </div>

                </div>
    </div>
    <footer class="panel-footer">
        <button type="submit" id="botao" onsubmit="verificaUnicidade()" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

    </footer>
    </section>
    </form>
    

</div>
</div>