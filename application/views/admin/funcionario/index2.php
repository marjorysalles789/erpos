
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Funcionário
        <small>Gerenciar Funcionários</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dasboard</a></li>
        <li class="active">Funcionário</li>
      </ol>
    </section>
     <hr>
    <!-- Main content -->
    <section class="content container-fluid">
    <?php
        get_msg('salvo');
        ?>
          <div class="row">
        <div class="col-xs-12">
          <div class="">
            <div class="">
            <div class="align-right"><a href="<?php echo base_url();?>index.php/funcionario/novo" class="btn btn-primary  "><i class="fa fa-plus-circle"></i> Adicionar Funcionário</a>
            </div>
            </div>
            <!--AQUI VAI ADIÇÃO FILTROS DE BUSCA -->  
          
            <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title "> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon text-right"><i class="icon-list"></i></span>
                                <h5>Busca Avançada</h5>
                            </a> </div>
                    </div>
                    <div class="accordion-body <?php if($filtro){echo 'in';}else{echo '';} ?>  collapse" id="collapseGOne" style="height: auto;">
                        <div class="widget-content">
                        <div class="box-body">

            <div class="row">
                <form action="" method="get">
                   <div class="col-md-3">
                   <div class="form-group">
                                    <label>Secretaria</label>
                                    <select class="form-control select2" id="os_id_secretaria" name="id_secretaria" onchange="os_busca_setor($(this).val())" style="width: 100%;">
                                    <option ></option>
                                    <?php if($this->session->userdata('secretaria')){
                                        echo $this->session->userdata('secretaria');
                                        foreach ($this->session->userdata('secretaria') as $secretarias) {?>
                                         <option value="<?php echo $secretarias->id_secretaria; ?>" ><?php echo $secretarias->nm_secretaria; ?></option>
                                           
                                      <?php  }} ?>

                                    } ?>
                                    <?php if ($secretaria) {
                                        foreach ($secretaria as $secretarias) { ?>
                                    <option value="<?php echo $secretarias->id_secretaria; ?>" ><?php echo $secretarias->nm_secretaria; ?></option>
                                    <?php 
                                }
                            } ?>
                                     </select>
                                        
                              </div>   
                              
                  </div>  
                  <div class="col-md-2">
                  <div class="form-group">
                                <label>Setor</label>
                                <select class="form-control select2" id="os_id_setor"  name="id_setor"  style="width: 100%;">
                                <option></option>
                               
                                </select>
                                        
                            </div> 
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                                <label>Cargo</label>
                                <select class="form-control select2" id="st_os"  name="cid_cargo"   style="width: 100%;">
                                <option></option>
                                <?php if ($cargo) {
                                        foreach ($cargo as $cargos) { ?>
                                <option value="<?php echo $cargos->id_cargo; ?>" ><?php echo $cargos->nm_cargo; ?></option>
                                    <?php 
                                }
                            } ?>
                              </select>
                               
                            
                                        
                            </div> 
                </div>
                
                <!--aquiv vai mais um o nome p/ busca -->
                <div class="col-md-3">
                  <div class="form-group">
                                <label>Funcionário</label>
                                <input type="text" class="form-control select2" id="os_busca_nome"  name="busca_nome"  style="width: 100%;">       
                            </div> 
                </div>
               
                <div class="col-md-2">
                  <div class="form-group">
                                <label><br> </label>
                                <input type="submit" class="form-control select2 btn  <?php if($filtro){echo 'btn bg-purple';}else{echo 'btn btn-primary';} ?>" id="os_busca_nome"   name="buscar" value="<?php if($filtro){echo 'Desativar Filtro';}else{echo 'Ativar Filtro';} ?>" style="width: 100%;"> 
                            </div> 
                </div>
                
                
                
                </form>
                   </div>
                        </div>
                        </div>
                    </div>
            </div>
            <!--FINAL FILTROS DE BUSCA -->  
            <!-- /.box-header -->
           
    
<div class="row wrapper border-bottom white-bg page-heading">

   

   <div class="col-lg-2">

   </div>

</div>

<div class="row">

   <div class="col-lg-12">

      

        

         <div class="ibox-content">

         <div class="table table-responsive">

            <table class="table table-striped table-bordered table-hover Tax" >

               <thead>

                  <tr>

                     <th><input onclick="toggle(this,'cbgroup1')" id="foo[]" name="foo[]" type="checkbox" value="" /></th>

                     <th> Sr No. </th>

                     
				
				<th> <a href="http://localhost/cigenerator/cigenerator/index.php/admin/tb_cargo/index?sortBy=nm_cargo" class="link_css"> Nm_cargo <i class='fa fa-sort' aria-hidden='true'></i></a></th>

						

                     <th> Action </th>

                  </tr>

               </thead>

               <tbody>

                  
                  
                  <tr  id="hide3" >

                  <th><input name='input' id='del' onclick="callme('show')"  type='checkbox' class='del' value='3'/></th>

                              

            <th>1</th><th>auxiliar administrativo</th>

                <th class="action-width">

		   <a href="http://localhost/cigenerator/cigenerator/admin/tb_cargo/view/3" title="View">

            <span class="btn btn-info " ><i class="fa fa-eye"></i></span>

           </a>

           <a href="http://localhost/cigenerator/cigenerator/admin/tb_cargo/edit/3" title="Edit">

            <span class="btn btn-info " ><i class="fa fa-edit"></i></span>

           </a>

           <a  title="Delete" data-toggle="modal" data-target="#commonDelete" onclick="set_common_delete('3','tb_cargo');">

           <span class="btn btn-info " ><i class="fa fa-trash-o "></i></span>

           </a>

            </th>

                  </tr>

                  
                  <tr  id="hide2" >

                  <th><input name='input' id='del' onclick="callme('show')"  type='checkbox' class='del' value='2'/></th>

                              

            <th>2</th><th>auxiliar de administração</th>

                <th class="action-width">

		   <a href="http://localhost/cigenerator/cigenerator/admin/tb_cargo/view/2" title="View">

            <span class="btn btn-info " ><i class="fa fa-eye"></i></span>

           </a>

           <a href="http://localhost/cigenerator/cigenerator/admin/tb_cargo/edit/2" title="Edit">

            <span class="btn btn-info " ><i class="fa fa-edit"></i></span>

           </a>

           <a  title="Delete" data-toggle="modal" data-target="#commonDelete" onclick="set_common_delete('2','tb_cargo');">

           <span class="btn btn-info " ><i class="fa fa-trash-o "></i></span>

           </a>

            </th>

                  </tr>

                  
                  <tr  id="hide1" >

                  <th><input name='input' id='del' onclick="callme('show')"  type='checkbox' class='del' value='1'/></th>

                              

            <th>3</th><th>ANALISTA DE SISTEMAS</th>

                <th class="action-width">

		   <a href="http://localhost/cigenerator/cigenerator/admin/tb_cargo/view/1" title="View">

            <span class="btn btn-info " ><i class="fa fa-eye"></i></span>

           </a>

           <a href="http://localhost/cigenerator/cigenerator/admin/tb_cargo/edit/1" title="Edit">

            <span class="btn btn-info " ><i class="fa fa-edit"></i></span>

           </a>

           <a  title="Delete" data-toggle="modal" data-target="#commonDelete" onclick="set_common_delete('1','tb_cargo');">

           <span class="btn btn-info " ><i class="fa fa-trash-o "></i></span>

           </a>

            </th>

                  </tr>

                    

               </tbody>

            </table>

            </div>

            
         </div>

      </div>

      <img onclick="callme('','item','')" src="http://localhost/cigenerator/cigenerator/accets//img/mac-trashcan_full-new.png" id="recycle" style="width:90px;  display:none; position:fixed; bottom: 50px; right: 50px;"/>

   </div>

</div>        
  <!-- /.content-wrapper -->








<!--modal visualizar detalhes -->


<!-- Button trigger modal -->


<!-- Modal-->
<?php
if ($funcionario) {
    foreach ($funcionario as $funcionarios) {
        ?>
        <div class="modal fade" id="ver_<?php echo $funcionarios->id_funcionario; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-info"> <b>Id:</b> <?php echo $funcionarios->id_funcionario; ?><br>
                            <b>Funcionário: </b><?php echo $funcionarios->nm_funcionario; ?>
                            <br> 
                            <b>Nº Resp: </b><?php echo $funcionarios->n_resp; ?>
                            <br> 
                            <b>E-mail: </b><?php echo $funcionarios->mail_funcionario; ?>
                            <br> 
                            <b>Cargo: </b><?php echo $funcionarios->nm_cargo; ?>
                            <br> 
                            <b>Nome de Usuário do Sistema: </b><?php echo $funcionarios->nm_usuario; ?>
                            <br> 
                            <b>Secretaria: </b><?php echo $funcionarios->nm_secretaria; ?>
                            <br> 
                            <b>Setor: </b><?php echo $funcionarios->nm_setor; ?>
                            <br> 
                            <b>Local: </b><?php echo $funcionarios->nm_local; ?>
                            <br>
                            <b>Obs local de trabalho: </b><?php echo $funcionarios->obs_local; ?>
                            <br> 
                            <b>Responsável do Setor: </b><?php echo $funcionarios->nm_responsavel; ?>
                            <br> 
                            <b>Nº Ramal/Telefone: </b><?php echo $funcionarios->n_ramal; ?>
                            <br> 
                            <b>Status: </b><?php if($funcionarios->ativo){echo 'ativo';}else{echo 'inativo';} ?>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<?php
if ($funcionario) {
    foreach ($funcionario as $funcionarios) {
        ?>
        <form action="<?php echo base_url() ?>index.php/funcionario/delete/<?php echo encript($funcionarios->id_funcionario);?>/<?php echo encript($funcionarios->id_local);?>" method="get" >
        <div class="modal fade" id="modal-excluir_<?php echo $funcionarios->id_funcionario; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Deseja realmente apagar esse registro?</center></h4>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  