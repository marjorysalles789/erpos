<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ERPOS - Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/dist/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/font-awesome.css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>assets/dist/css/sb-admin.css" rel="stylesheet">

</head>

<body style="background-color:#7a868f;">

  <div style="padding:0%; margin: 10%;" class="container border-danger">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">ERPOS | Login</div>
      <div class="card-body border-primary">
        <form action="<?php echo base_url()?>index.php/principal/logar" method="post">
          <div class="form-group">
          
            <div class="form-group">
              <input type="text"class="form-control" name="nm_usuario" id="nm_usuario" placeholder="Usuário" >
             
            </div>
          </div>
          <div class="form-group">
          
            <div class="form-group">
              <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" >
              
            </div>
          </div>
        
          <input type="submit" class="btn btn-primary btn-block" href="index.html" value="Entrar">
        </form>
       
        <?php
        echo '<hr>';
        get_msg('salvo');
        echo '<br>';
        ?>
    
        <div class="text-center">
       
          <a class="d-block small" href="forgot-password.html">Esqueci minha senha</a>
        </div>
      </div>
    </div>
  </div>

  

</body>

</html>

