<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script> 
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">

        <form id="form1" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/subcategoria/update'; ?>" class="form-horizontal">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title"><?php echo $titulo; ?></h2>
                    <p class="panel-subtitle">

                    </p>
                </header>
                <div class="panel-body">

                   <div class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>

                                    <input type="text" class="form-control     " id="txtnome" name="nm_subcategoria" value="<?php echo $subcategoria[0]->nm_subcategoria; ?>" placeholder="Nome.">
                                    <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                                </div>
                                <input type="hidden" name="id_subcategoria" value="<?php echo $subcategoria[0]->id_subcategoria; ?>">


                </div>
                </div>
                <footer class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

                </footer>
            </section>
        </form>
    </div>
