<div class="row">
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/secretaria/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Secretaria</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php if ($filtro) {
                                                                    echo 'in';
                                                                } else {
                                                                    echo '';
                                                                } ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-4">
                                <div class="form-group">

                        <label>Secretaria</label>
                        <select class="form-control      select2" name="nm_secretaria" style="width: 100%;">
                            <option selected="selected"></option>
                            <?php if ($secretaria1) {
                                foreach ($secretaria1 as $secretarias) { ?>
                                    <option value="<?php echo $secretarias->nm_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
                            <?php }
                            } ?>
                        </select>
                    </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Secretário</label>
                                    <input type="text" class="form-control select2" name="nm_secretario" style="width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn <?php if ($filtro) {
                                                                                                echo 'btn bg-quartenary';
                                                                                            } else {
                                                                                                echo 'btn btn-info';
                                                                                            } ?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
                                                                                                                                                echo 'Desativar Filtro';
                                                                                                                                            } else {
                                                                                                                                                echo 'Ativar Filtro';
                                                                                                                                            } ?>" style="width: 100%;">
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>

<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-condensed mb-none">


                <th class="text-center" class="text-center">#id</th>
                <th class="text-center">secretaria</th>
                <th class="text-center">secretario</th>
                <th class="text-center">Ações</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                    <tr>
                        <?php if ($secretaria) {
                            foreach ($secretaria as $secretarias) { ?>
                                <td><?php echo $secretarias->id_secretaria; ?></td>
                                <td><?php echo $secretarias->nm_secretaria; ?></td>
                                <td><?php echo $secretarias->nm_secretario; ?></td>
                                <td>
                                    <div class="btn-group-horiontal">
                                        <a data-toggle="modal" data-target="#ver_<?php echo $secretarias->id_secretaria; ?>" title="visualizar" class="btn btn-flat btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo base_url(); ?>index.php/secretaria/edit/<?php echo encript($secretarias->id_secretaria); ?>" title="editar" class="btn btn btn-xs btn-flat btn-info"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" data-target="#modal-excluir_<?php echo $secretarias->id_secretaria; ?>" title="excluir" class="btn btn-flat btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </td>
                    </tr>
                <?php }
                        } else { ?>

                <td colspan="4">
                    <center>Nenhuma secretaria cadastrada</center>
                </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination">
                    <?php if (isset($pag) && !empty($pag)) {
                        foreach ($pag as $key => $value) {
                            echo " {$value} ";
                        }
                    }; ?>
            </div>
        </div>
</section>





<!-- modal visualização -->

<!-- Modal Info -->

<?php
if ($secretaria) {
    foreach ($secretaria as $secretarias) {
?>
        <div class="modal fade modal-info " id="ver_<?php echo $secretarias->id_secretaria; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS </h4>

                    </div>
                    <div class="modal-body">

                        <div class="text-info"> <b>Id:</b> <?php echo $secretarias->id_secretaria; ?><br>
                            <b>Secretaria: </b><?php echo $secretarias->nm_secretaria; ?>
                            <br>
                            <b>Secretario: </b><?php echo $secretarias->nm_secretario; ?>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

<?php
    }
}
?>

<!-- modal realmente deletar -->
<?php
if ($secretaria) {
    foreach ($secretaria as $secretarias) {
?>
        <form action="<?php echo base_url() ?>index.php/secretaria/delete/<?php echo encript($secretarias->id_secretaria); ?>" method="get">
            <div class="modal fade" id="modal-excluir_<?php echo $secretarias->id_secretaria; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS </h4>

                        </div>
                        <div class="modal-body">

                            <div class="text-bold ">
                                <h4>
                                    <center> Deseja realmente apagar esse registro?</center>
                                </h4>
                                <br>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            <input type="submit" class="btn btn-danger" value="Excluir">

                        </div>
                    </div>
                </div>
            </div>
        </form>
<?php
    }
}
?>