<div class="row">
    <div class="col-md-12">
        <?php
        get_msg('salvo');
        ?>
        <form id="form1" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/setor/update'; ?>" class="form-horizontal">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title"><?php echo $titulo; ?></h2>
                    <p class="panel-subtitle">

                    </p>
                </header>
                <div class="panel-body">
                    <div class="form-group">

                        <label>Secretaria</label>
                        <select class="form-control      select2" name="id_secretaria" style="width: 100%;">
                            <option value="<?php echo $setor[0]->cid_secretaria; ?>"><?php echo $setor[0]->nm_secretaria; ?></option>
                            <?php if ($secretaria) {
                                foreach ($secretaria as $secretarias) { ?>
                                    <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
                            <?php }
                            } ?>
                        </select>
                        <?php echo form_error('id_secretaria', '<div class="text-danger">', '</div>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Nome</label>

                        <input type="text" class="form-control     " id="txtnome" name="nome" value="<?php echo $setor[0]->nm_setor; ?>" placeholder="Nome.">
                        <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Responsável setor</label>

                        <input type="text" class="form-control     " id="txtnome" name="responsavel" value="<?php echo $setor[0]->nm_responsavel; ?>" placeholder="Nome.">
                        <?php echo form_error('responsavel', '<div class="text-danger">', '</div>'); ?>
                    </div>
                </div>
                <input type="hidden" name="id_setor" value="<?php echo $setor[0]->id_setor; ?>">
    </div>
    <footer class="panel-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

    </footer>
    </section>
    </form>
</div>
</div>