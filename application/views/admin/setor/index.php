<div class="row">
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/setor/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Setor</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php if ($filtro) {
                                                                    echo 'in';
                                                                } else {
                                                                    echo '';
                                                                } ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        



                            <!--aquiv vai mais um o nome p/ busca -->
                            <form action="" method="get">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Secretaria</label>
                                        <select class="form-control select2" id="os_id_secretaria" name="id_secretaria" onchange="os_busca_setor($(this).val())" style="width: 100%;">
                                            <option></option>
                                            <?php if ($this->session->userdata('secretaria')) {
                                                echo $this->session->userdata('secretaria');
                                                foreach ($this->session->userdata('secretaria') as $secretarias) { ?>
                                                    <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>

                                            <?php  }
                                            } ?>

                                            } ?>
                                            <?php if ($secretaria) {
                                                foreach ($secretaria as $secretarias) { ?>
                                                    <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
                                            <?php
                                                }
                                            } ?>
                                        </select>

                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Setor</label>
                                        <select class="form-control select2" id="os_id_setor" name="nm_setor" style="width: 100%;">
                                            <option></option>

                                        </select>

                                    </div>
                                </div>


                                <!--aquiv vai mais um o nome p/ busca -->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Responsável Setor</label>
                                        <input type="text" class="form-control select2" name="nm_responsavel" style="width: 100%;">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><br> </label>
                                        <input type="submit" class="form-control select2 btn  <?php if ($filtro) {
                                                                                                    echo 'btn bg-quartenary';
                                                                                                } else {
                                                                                                    echo 'btn btn-primary';
                                                                                                } ?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
                                                                                                                                                                                                                    echo 'Desativar Filtro';
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    echo 'Ativar Filtro';
                                                                                                                                                                                                                } ?>" style="width: 100%;">
                                    </div>
                                </div>



                            </form>




                    </div>

                </div>
            </div>
        </div>

                
    </div>

</div>
        <!--final busca -->

        <section class="panel">

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-condensed mb-none">
                   
                      
               
                                
                  <th class="text-center">#id</th>
                  <th class="text-center">setor</th>
                  <th class="text-center">responsável setor</th>
                  <th class="text-center">secretaria</th>
                  <th class="text-center">Ações</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                <tr>
                
                <?php if($setor){ foreach ($setor as $setors) { ?>
                  <td><?php echo $setors->id_setor;?></td>
                  <td><?php echo $setors->nm_setor;?></td>
                  <td><?php echo $setors->nm_responsavel;?></td>
                  <td><?php echo $setors->nm_secretaria;?></td>
                  <td class="text-center"> <div class="btn-group-horiontal">
                      <a  data-toggle="modal" data-target="#ver_<?php echo $setors->id_setor; ?>" title="visualizar" class="btn btn btn-xs btn-flat btn-default"><i class="fa fa-eye"></i></a>
                      <a href="<?php echo base_url(); ?>index.php/setor/edit/<?php echo encript($setors->id_setor); ?>"  title="editar" class="btn btn btn-xs btn-flat btn-info"><i class="fa fa-edit"></i></a>
                      <a data-toggle="modal" data-target="#modal-excluir_<?php echo $setors->id_setor; ?>" title="excluir"  class="btn btn-flat btn btn-xs btn-danger"><i class="fa fa fa-trash-o"></i></a>
                    </div></td>
                </tr>
                <?php } }else{?>    
                
                <td colspan="4"><center>Nenhum setor cadastrado</center> </td>
                </tr>
                <?php } ?>
                </tbody>

                    </table>
                </div>
            </div>
            </table>
             <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                  <ul class="pagination">
                  <?php if(isset($pag) && !empty($pag)){foreach ($pag as $key => $value) {
                      echo " {$value} ";
                  }}; ?>
								</div>
							</div>
        </section>





        <!-- Modal -->
<?php
if ($setor) {
    foreach ($setor as $setors) {
        ?>
        <div class="modal fade" id="ver_<?php echo $setors->id_setor; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-info"> <b>Id:</b> <?php echo $setors->id_setor; ?><br>
                            <b>Setor: </b><?php echo $setors->nm_setor; ?>
                            <br> 
                            <b>Responsável: </b><?php echo $setors->nm_responsavel; ?>
                            <br>
                            <b>Secretaria: </b><?php echo $setors->nm_secretaria; ?>
                            <br>
                            <b>Secretário: </b><?php echo $setors->nm_secretario; ?>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<?php
if ($setor) {
    foreach ($setor as $setors) {
        ?>
        <form action="<?php echo base_url() ?>index.php/setor/delete/<?php echo encript($setors->id_setor); ?>" method="get" >
        <div class="modal fade" id="modal-excluir_<?php echo $setors->id_setor; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Deseja realmente apagar esse registro?</center></h4>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  