   
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Produto
            <small>Adicionar Produto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php 
                                                                                                                                                                                                                                                                                }; ?>  </li>
        </ol>
    </section>


    <section class="content">
    <?php
    get_msg('salvo');
    ?>
        <!-- Default box -->
        <div class="">
            <div class="">

            <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title "> <span class="icon text-right"><i class="icon-list"></i></span>
                                <h5>Novo Produto</h5>
                            </a> </div>
                    </div>
                    <div class="accordion-body in collapse" id="collapseGOne" style="height: auto;">
                        <div class="widget-content">
                        <div class="box-body">
               
            </div>
            <div class="box-body">
                <!--aqui vem a tabela que vai vir do banco de dados -->

                <div class="row">
                    <!-- left column -->
                    <div class="col-lg-12">

                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" id="form_produto"    action="<?php echo base_url() . 'index.php/produto/save'; ?>">
                            
                                <div  class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>

                                    <input type="text" class="form-control     " id="nome"  name="nm_produto"    value="<?php echo set_value('nm_produto'); ?>" >
                                    <span id="nm_produto-erro" class="text-danger  " style="display:none">Este Funcionário já esta cadatrado.</span>
                                   
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Descrição</label>

                                    <input type="text" required class="form-control     " id="desc_produto" onchange="verificaEmailExiste()" name="desc_produto" value="<?php echo set_value('desc_produto'); ?>" >
                                    <?php echo form_error('mail_produto', '<div class="text-danger">', '</div>'); ?>
                                    
                                </div>
                                
                
                   <div class="form-group">
                                    <label>Grupo</label>
                                    <select class="form-control select2" id="os_id_categoria" name="id_categoria" onchange="os_busca_categoria($(this).val())" style="width: 100%;">
                                    <option ></option>
                                    <?php if($this->session->userdata('categoria')){
                                        echo $this->session->userdata('categoria');
                                        foreach ($this->session->userdata('categoria') as $categorias) {?>
                                         <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                           
                                      <?php  }} ?>

                                    } ?>
                                    <?php if ($categoria) {
                                        foreach ($categoria as $categorias) { ?>
                                    <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                    <?php 
                                }
                            } ?>
                                     </select>
                                        
                              </div>   
                              
                 
                  
                  <div class="form-group">
                                <label>Sub Grupo</label>
                                <select class="form-control select2" id="os_id_setor"  name="id_subgrupo"  style="width: 100%;">
                                <option></option>
                               
                                </select>
                                        
                            </div> 
                




                                <div class="form-group">
                                    <label for="exampleInputEmail1">Código</label>

                                    <input type="text" class="form-control     " id="n_resp" name="cod_sysdardani" onchange="verificaNumeroResp()" value="<?php echo set_value('cod_sysdardani'); ?>" >
                                    <?php echo form_error('n_resp', '<div class="text-danger">', '</div>'); ?>
                                    
                                </div>
                               
                            <div class="form-group">
                                <label>Unidade</label>
                                <select class="form-control      select2" id="unidade" name="unidade"  style="width: 100%;">
                                <option>UN</option>
                                <option>Kl</option>
                                </select>
                                        
                            </div>   
                          
                                <div class="box-footer text-right">
                                    <button type="submit" id="botao" onsubmit="verificaUnicidade()" class="btn btn-primary "><i class="fa fa-check"></i> Adicionar</button>
                                </div>
                            </div>      
                        </form>
                    </div>

                </div>
               
                
                </section>



                        </div>
                        
            

            <!-- /.content-wrapper -->
        



            <!--modal nova marca -->

<script>
     function verificaUnicidade(){
      var nm_usuario=$('#nm_produto').val();
      var mail_produto=$('#desc_produto').val()
      var nm_produto=$('#nm_produto').val();
     
      
			$.post(base_url+"index.php/produto/verificaUnicidade", {
        nm_usuario:nm_usuario,
        nm_produto:nm_produto,
        mail_produto:mail_produto        
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                   
                 }
                 if(retorno==0){
                    $('#botao').attr('disabled','true'); 
                    $('.modal-body').html('<p>Esse Produto já está cadastrado!</p><p>Por segurança o botão salvar foi desativado,recomece o processo de cadastro verificando os dados.</p>');
                    $('#modal-danger').modal();
                       
                   
                 }
			});
		}
        function verificaEmailExiste(){
      
      var mail_produto=$('#mail_produto').val()
      
     
      
			$.post(base_url+"index.php/produto/verificaEmailExiste", {
     
        mail_produto:mail_produto        
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                   
                 }
                 if(retorno==0){
                    $('#mail_produto').val(''); 
                    $('#mail_produto').focus();
                    $('.modal-body').html('<p>O e-mail:<strong> '+mail_produto+'</strong> já está em uso.</p><p>Entre com um novo endereço de e-mail</p>');
                    $('#modal-danger').modal();
                       
                   
                 }
			});
		}
        function verificaNumeroResp(){
      
      var n_resp=$('#n_resp').val()
      
     
      
			$.post(base_url+"index.php/produto/verificaNumeroResp", {
     
           n_resp:n_resp        
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                   
                 }
                 if(retorno==0){
                    $('#n_resp').val(''); 
                    $('#n_resp').focus();
                    $('.modal-body').html('<p>O Resp :<strong> '+n_resp+'</strong> já está em uso.</p><p>O Número Resp é único a cada funcionário caso não  saiba ou não se lembre, ligue no departamento de RH.</p>');
                    $('#modal-danger').modal();
                       
                   
                 }
			});
		}
        function verificaUsuario(){
      
      var nm_usuario=$('#nm_usuario').val()
      
     
      
			$.post(base_url+"index.php/produto/verificaUsuario", {
     
           nm_usuario:nm_usuario      
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                   
                 }
                 if(retorno==0){
                    $('#nm_usuario').val(''); 
                    $('#nm_usuario').focus();
                    $('.modal-body').html('<p>O nome de usuário:<strong> '+nm_usuario+'</strong> já está em uso.</p><p>Para usuário sugerimos que adote o padrão Nome+iniciais dos sobrenomes.</p></p>Exemplo: João Alves Lima, resultado: joaoal<p>');
                    $('#modal-danger').modal();
                       
                   
                 }
			});
		}    
</script> 

<!-- treta aqui validar com js 08-05-2019 -->

<!--modal erro caso usuario já esteja cadastrado -->
<div class="modal modal-warning fade" id="modal-danger" style="display: none">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">ERPOS</h4>
              </div>
              <div class="modal-body default">
                <p>O usuário já está cadastrado! por favor verifique.&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Entendi</button>
               
              </div>
            </div>
          </div>  
</div>     
            <!-- /.modal-content -->