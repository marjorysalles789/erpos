<div class="row">
<script>
function msg(){
 $('.mb-xs').trigger('click');
}
</script> 
<?php
        get_msg('salvo');
        ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/produto/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Produto</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php if ($filtro) {
                                                                    echo 'in';
                                                                } else {
                                                                    echo '';
                                                                } ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                   <div class="col-md-3">
                   <div class="form-group">
                                    <label>Grupo</label>
                                    <select class="form-control select2" id="os_id_categoria" name="id_categoria" onchange="os_busca_categoria($(this).val())" style="width: 100%;">
                                    <option ></option>
                                    <?php if($this->session->userdata('categoria')){
                                        echo $this->session->userdata('categoria');
                                        foreach ($this->session->userdata('categoria') as $categorias) {?>
                                         <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                           
                                      <?php  }} ?>

                                    } ?>
                                    <?php if ($categoria) {
                                        foreach ($categoria as $categorias) { ?>
                                    <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                    <?php 
                                }
                            } ?>
                                     </select>
                                        
                              </div>   
                              
                  </div>  
                  <div class="col-md-3">
                  <div class="form-group">
                                <label>Sub Grupo</label>
                                <select class="form-control select2" id="os_id_setor"  name="id_subgrupo"  style="width: 100%;">
                                <option></option>
                               
                                </select>
                                        
                            </div> 
                </div>
               
                
                <!--aquiv vai mais um o nome p/ busca -->
                <div class="col-md-3">
                  <div class="form-group">
                                <label>produto</label>
                                <input type="text" class="form-control select2" id="os_busca_nome"  name="nm_produto"  style="width: 100%;">       
                            </div> 
                </div>
                               <div class="col-md-3">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn <?php if ($filtro) {
                                                                                                echo 'btn bg-quartenary';
                                                                                            } else {
                                                                                                echo 'btn btn-info';
                                                                                            } ?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
                                                                                                                                                echo 'Desativar Filtro';
                                                                                                                                            } else {
                                                                                                                                                echo 'Ativar Filtro';
                                                                                                                                            } ?>" style="width: 100%;">
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>



<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
         <table class="table table-bordered table-striped table-condensed mb-none">


                
                                <thead>
                                    <tr  style="backgroud-color: #2D335B ;">
                  <th>#id</th> 
                  <th>produto</th>
                  <th>descrição</th>
                  <th>grupo</th>
                  <th>subgrupo</th>
                  <th>codigo</th>
                  <th>unidade</th>
                 
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <?php if($produto){ foreach ($produto as $produtos) { ?>
                 
                   
                  <td><?php echo $produtos->id_produto;?></td>
                  <td><?php echo $produtos->nm_produto;?></td>
                  <td><?php echo $produtos->desc_produto;?></td>
                  <td><?php echo $produtos->nm_categoria;?></td>
                  <td><?php echo $produtos->nm_subcategoria;?></td>
                  <td><?php echo $produtos->cod_sysdardani;?></td>
                  <td><?php echo $produtos->unidade;?></td>
                  <td> <div class="btn-group-horiontal">
                      <a  data-toggle="modal" data-target="#ver_<?php echo $produtos->id_produto; ?>" title="visualizar" class="btn btn-outline-info btn-info"><i class="fa fa-eye"></i></a>
                      <a href="<?php echo base_url(); ?>index.php/produto/edit/<?php echo encript($produtos->id_produto); ?>"  title="editar" class="btn btn-flat btn-info"><i class="fa fa-edit"></i></a>
                      <a data-toggle="modal" data-target="#modal-excluir_<?php echo $produtos->id_produto; ?>" title="excluir"  class="btn btn-flat btn-info"><i class="fa fa-trash"></i></a>
                    </div></td>
                </tr>
                <?php  } }else{?>    
                
                <td colspan="8"><center>Nenhum produto cadastrado</center> </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                  <td colspan="9" style="align:right">    
                  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                  <ul class="pagination">
                  <?php if(isset($pag) && (!empty($pag))){foreach ($pag as $key => $value) {
                      echo " {$value} ";
                  }}; ?>
                  </ul>
                  </div>
                  </td>
                    </tr>
                   
                </tfoot>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination">
                    <?php if (isset($pag) && !empty($pag)) {
                        foreach ($pag as $key => $value) {
                            echo " {$value} ";
                        }
                    }; ?>
            </div>
        </div>
</section>
</div>




<!-- modal visualização -->

<!-- Modal Info -->

<!-- Modal-->
<?php
if ($produto) {
    foreach ($produto as $produtos) {
        ?>
        <div class="modal fade" id="ver_<?php echo $produtos->id_produto; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-info"> <b>Id:</b> <?php echo $produtos->id_produto; ?><br>
                            <b>Funcionário: </b><?php echo $produtos->nm_funcionario; ?>
                            <br> 
                            <b>Nº Resp: </b><?php echo $produtos->n_resp; ?>
                            <br> 
                            <b>E-mail: </b><?php echo $produtos->mail_funcionario; ?>
                            <br> 
                            <b>Cargo: </b><?php echo $produtos->nm_cargo; ?>
                            <br> 
                            <b>Nome de Usuário do Sistema: </b><?php echo $produtos->usuario; ?>
                            <br> 
                            <b>Secretaria: </b><?php echo $produtos->nm_secretaria; ?>
                            <br> 
                            <b>Setor: </b><?php echo $produtos->nm_setor; ?>
                            <br> 
                            <b>Local: </b><?php echo $produtos->nm_local; ?>
                            <br>
                            <b>Obs local de trabalho: </b><?php echo $produtos->obs_local; ?>
                            <br> 
                            <b>Responsável do Setor: </b><?php echo $produtos->nm_responsavel; ?>
                            <br> 
                            <b>Nº Ramal/Telefone: </b><?php echo $produtos->n_ramal; ?>
                            <br> 
                            <b>Status: </b><?php if($produtos->status){echo 'ativo';}else{echo 'inativo';} ?>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<!-- modal realmente deletar -->
<?php
if ($produto) {
    foreach ($produto as $produtos) {
        ?>
        <form action="<?php echo base_url(); ?>index.php/produto/delete/<?php echo encript($produtos->id_produto);?>/<?php echo encript($produtos->fid_funcionario);?>/<?php echo encript($produtos->cid_local);?>" method="get" >
        <div class="modal fade" id="modal-excluir_<?php echo $produtos->id_produto; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Deseja realmente apagar esse registro?</center></h4>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  