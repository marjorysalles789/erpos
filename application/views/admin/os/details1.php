<div class="span12" id="divProdutosServicos" style=" margin-left: 0">
    <ul class="nav nav-tabs">
        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="span12" id="divCadastrarOs">
                <form action="http://localhost/os/index.php/os/adicionar" method="post" id="formOs" novalidate="novalidate">
                    <div class="span12" style="padding: 1%">
                        <div class="span6"> <label for="cliente">Cliente<span class="required">*</span></label> <input id="cliente" class="span12 ui-autocomplete-input" type="text" name="cliente" value="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <input id="clientes_id" class="span12" type="hidden" name="clientes_id" value=""> </div>
                        <div class="span6"> <label for="tecnico">Técnico / Responsável<span class="required">*</span></label> <input id="tecnico" class="span12 ui-autocomplete-input" type="text" name="tecnico" value="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <input id="usuarios_id" class="span12" type="hidden" name="usuarios_id" value=""> </div>
                    </div>
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span3"> <label for="status">Status<span class="required">*</span></label> <select class="span12" name="status" id="status" value="">
                                <option value="Orçamento">Orçamento</option>
                                <option value="Aberto">Aberto</option>
                                <option value="Em Andamento">Em Andamento</option>
                                <option value="Finalizado">Finalizado</option>
                                <option value="Cancelado">Cancelado</option>
                            </select> </div>
                        <div class="span3"> <label for="dataInicial">Data Inicial<span class="required">*</span></label> <input id="dataInicial" class="span12 datepicker hasDatepicker" type="text" name="dataInicial" value="14/07/2019"> </div>
                        <div class="span3"> <label for="dataFinal">Data Final</label> <input id="dataFinal" class="span12 datepicker hasDatepicker" type="text" name="dataFinal" value=""> </div>
                        <div class="span3"> <label for="garantia">Garantia</label> <input id="garantia" type="text" class="span12" name="garantia" value=""> </div>
                    </div>
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span6"> <label for="descricaoProduto">Descrição Produto/Serviço</label> <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5"></textarea> </div>
                        <div class="span6"> <label for="defeito">Defeito</label> <textarea class="span12" name="defeito" id="defeito" cols="30" rows="5"></textarea> </div>
                    </div>
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span6"> <label for="observacoes">Observações</label> <textarea class="span12" name="observacoes" id="observacoes" cols="30" rows="5"></textarea> </div>
                        <div class="span6"> <label for="laudoTecnico">Laudo Técnico</label> <textarea class="span12" name="laudoTecnico" id="laudoTecnico" cols="30" rows="5"></textarea> </div>
                    </div>
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span6 offset3" style="text-align: center"> <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button> <a href="http://localhost/os/index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



//
<!--codigo para imprimir via javascritp -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-imprimir").click(function() {
            $('#tab1').printElement();
        });
    });
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detalhe
            <small>Chamado</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php }; ?> </li>
        </ol>
    </section>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="row-fluid" style="margin-top:0">
                    <div class="span12">



                        <div id="tab1" class="tab-pane active" style="min-height: 300px">
                            <div class="accordion" id="collapse-group">
                                <div class="accordion-group widget-box">
                                    <div class="accordion-heading">
                                        <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon"><i class="fa fa-arrows-v"></i></span>
                                                <h5>Dados do Chamado</h5>
                                            </a> </div>
                                    </div>
                                    <div class="collapse in accordion-body" id="collapseGOne">
                                        <div class="widget-content">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: right; width: 11%"><strong> <b>Nº Chamado:</b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->id_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Data chamado: </b></strong></td>
                                                        <td><?php echo formataVisao($os[0]->dt_os); ?></td>
                                                        <td style="text-align: right;"><b>Horário: </b></td>
                                                        <td colspan="5"><?php echo $os[0]->hr_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Funcionário: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->nm_funcionario; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Secretaria: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->nm_secretaria; ?></td>
                                                        <td style="text-align: right"><strong><b>Setor: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->nm_setor; ?></td>
                                                        <td style="text-align: right"><strong><b>Local: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->nm_local; ?></td>
                                                        <td style="text-align: right"><strong><b>Ramal: </b></strong></td>
                                                        <td colspan=""><?php echo $os[0]->n_ramal; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong><b>Título: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->titulo_os; ?></td>
                                                    </tr>
                                                    <?php //pesquisa para saber quem foi o filha da puta que atendeu essa porra p/ passar informação p/ cliente pau no cu
                                                    $id_tecnico = $os[0]->id_tecnico;
                                                    $like = array(
                                                        'id_tecnico' => $id_tecnico
                                                    );
                                                    $tecnico = $this->Mos->listaOsTecnicoLike($p = 0, $por_pagina = null, 'v_tecnico', '*', $like, $porData = null, $order = null);   ?>
                                                    <tr>
                                                        <td style="text-align: right"><strong> <b>Problema: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->df_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right"><strong> <b>Observação: </b></strong></td>
                                                        <td colspan="8"><?php echo $os[0]->ob_os; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; width: 13%"><strong>Status:</strong></td>
                                                        <td colspan="7"><?php if ($os[0]->st_os == 'Fechado') {
                                                                            echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Cancelado') {
                                                                            echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Aberto') {
                                                                            echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Resolvendo') {
                                                                            echo '<div class="badge badge-info" >' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        if ($os[0]->st_os == 'Parado') {
                                                                            echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $os[0]->st_os . '</div>';
                                                                        }
                                                                        ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left;"><strong>Data Atendimento:</strong></td>
                                                        <td><?php if (isset($os[0]->dt_atendimento)) {
                                                                echo formataVisao($os[0]->dt_atendimento);
                                                            } else {
                                                                echo 'Aguardando...';
                                                            } ?> </td>

                                                        <td style="text-align: right"><strong>Horário:</strong></td>
                                                        <td colspan="1"><?php if (isset($os[0]->hr_atendimento)) {
                                                                            echo $os[0]->hr_atendimento;
                                                                        } else {
                                                                            echo 'Aguardando...';
                                                                        } ?></td>
                                                        <td colspan="" style="text-align: right;"><strong>Técnico Responsável:</strong></td>
                                                        <td colspan="3"> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                                                echo 'Aguardando Atendimento...';
                                                                            } else {
                                                                                echo   $tecnico['0']->nm_funcionario;
                                                                            } ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: right;"><strong>Solução:</strong></td>
                                                        <td colspan="8"><?php echo $os[0]->ld_tecnico; ?></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab1" class="tab-pane active" style="min-height: 300px">
                                    <div class="accordion" id="collapse-group">
                                        <div class="accordion-group widget-box">
                                            <div class="accordion-heading">
                                                <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGOne1" data-toggle="collapse"> <span class="icon"><i class="fa fa-arrows-v"></i></span>
                                                        <h5>Movimentações</h5>
                                                    </a> </div>
                                            </div>
                                            <div class="collapse  accordion-body" id="collapseGOne1">
                                                <div class="widget-content">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <!--aqui vai o looping com a  atualização dos chamados -->
                                                            <tr border="1">

                                                                <?php if (isset($movimentacao)) {
                                                                    foreach ($movimentacao as $movi) { ?>
                                                                    <tr>
                                                                        <td style="text-align: right; width: 13%"><strong>Status:</strong></td>
                                                                        <td colspan="7"><?php if ($movi->st_os == 'Fechado') {
                                                                                            echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                                                        }
                                                                                        if ($movi->st_os == 'Cancelado') {
                                                                                            echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $movi->st_os . '</div>';
                                                                                        }
                                                                                        if ($movi->st_os == 'Aberto') {
                                                                                            echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $movi->st_os . '</div>';
                                                                                        }
                                                                                        if ($movi->st_os == 'Resolvendo') {
                                                                                            echo '<div class="badge badge-info" >' . $movi->st_os . '</div>';
                                                                                        }
                                                                                        if ($movi->st_os == 'Parado') {
                                                                                            echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $movi->st_os . '</div>';
                                                                                        }
                                                                                        ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: left;"><strong>Data Atendimento:</strong></td>
                                                                        <td><?php if (isset($movi->dt_movimentacao)) {
                                                                                echo formataVisao($movi->dt_movimentacao);
                                                                            } else {
                                                                                echo 'Aguardando...';
                                                                            } ?> </td>

                                                                        <td style="text-align: right"><strong>Horário:</strong></td>
                                                                        <td colspan="1"><?php if (isset($movi->hr_movimentacao)) {
                                                                                            echo $movi->hr_movimentacao;
                                                                                        } else {
                                                                                            echo 'Aguardando...';
                                                                                        } ?></td>
                                                                        <td colspan="" style="text-align: right;"><strong>Técnico Responsável:</strong></td>
                                                                        <td colspan="3"> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                                                                echo 'Aguardando Atendimento...';
                                                                                            } else {
                                                                                                echo   $tecnico['0']->nm_funcionario;
                                                                                            } ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: right;"><strong>Solução:</strong></td>
                                                                        <td colspan="8"><?php echo $movi->ld_movimentacao; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="9" style="text-align: center;"><strong><br></strong></td>
                                                                    </tr>
                                                                <?php }
                                                        } ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>






                                        <div class="modal-footer">

                                            <a class="btn btn-navy align-right" onclick="$('#btn-imprimir').printElement()" id="btn-imprimir"><i class="fa fa-print"></i> Imprimir</a>
                                            <a class="btn btn-default align-right" href="<?php echo  base_url() ?>index.php/os"><i class="fa fa-close"></i> Fechar</a>

                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- Modal visualizar anexo -->

    </div>
</div>
</div>




</div>