<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script>
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Dados do Chamado</h2>
            </header>
            <div class="panel-body">

                <form id="form1" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/os/save'; ?>" >

                    <div class="form-group col-lg-3">
                        <label for="dataInicial">Data chamado<span class="required"></span></label>

                        <input id="dt_os" class="col-lg-3 datepicker form-control      " disabled="true" value="<?php echo date('d/m/Y '); ?>" type="text" name="dt_os" />
                        <input id="dt_os" name="dt_os" class="col-lg-3 datepicker form-control      " type="hidden" value="<?php echo date('d/m/Y '); ?>" type="text" name="dt_os" />
                    </div>
                    <div class="form-group col-lg-3">
                       <label for="dataFinal">Hora chamado</label>
                        <input id="hr_os" name="hr_os" class="col-lg-2 datepicker form-control     " disabled type="text" value="<?php echo date("H:i:s"); ?>" name="hr_os" />
                        <input id="hr_os" name="hr_os" class="col-lg-2 datepicker form-control     " type="hidden" value="<?php echo date("H:i:s"); ?>" name="hr_os" />
                    </div>
                  


                    <div class="form-group col-lg-3"> <label for="cliente">Cliente<span class="required"></span></label>
                        <input id="nm_funcionario" class="span12 form-control     " type="text" name="nm_funcionario" disabled value="<?php echo $funcionario[0]->nm_funcionario; ?> " />

                        <input id="cid_funcionario" class="span12 form-control     " type="hidden" name="cid_funcionario" value="<?php echo $funcionario[0]->id_funcionario; ?>" /> 
                    </div>
                    <div class=" form-group col-lg-3"> <label for="tecnico">Técnico / Responsável<span class="required "></span></label>
                        <select class="form-control      select2" name="id_tecnico" style="width: 100%;">
                            <option selected="selected" value="<?php echo $tecnico[0]->id_tecnico;?>">Em Aberto</option>
                            
                                <?php
                            
                            if ($tecnico) {
                                foreach ($tecnico as $tecnicos) {
                                    ?>
                                    <?php if ($tecnicos->id_tecnico <> '1') { ?>
                                        <option value="<?php echo $tecnicos->id_tecnico; ?>"><?php echo $tecnicos->nm_funcionario; ?></option>
                                    <?php } ?> 
                                <?php
                                }
                            }
                             ?>
                                    
                        </select>
                       <!--<input id="cid_tecnico" class="col-lg-12 form-control " value="<?php echo $tecnicos->id_tecnico; ?>" type="hidden" name="cid_tecnico" /> -->
                    </div>


                    <div class="form-group col-lg-3"> </br><label for="cliente">Secretaria<span class="required">*</span></label>
                        <input id="nm_secretaria" class="span12 form-control     " type="text" name="nm_secretaria" disabled value="<?php echo $funcionario[0]->nm_secretaria; ?> " />
                    </div>
                    <div class="form-group col-lg-3"></br> <label for="cliente">Setor<span class="required">*</span></label>
                        <input id="nm_setor" class="span12 form-control     " type="text" name="nm_setor" disabled value="<?php echo $funcionario[0]->nm_setor; ?> " />
                    </div>
                    <div class="form-group col-lg-3"></br> <label for="cliente">Ramal/Telefone<span class="required">*</span></label>
                        <input id="n_ramal" class="span12 form-control     " type="text" name="n_ramal" disabled value="<?php echo $funcionario[0]->n_ramal; ?> " />
                    </div>
                    <div class="form-group col-lg-3"></br> <label for="cliente">Local<span class="required">*</span></label>
                        <input id="nm_local" class="span12 form-control     " type="text" name="nm_local" disabled value="<?php echo $funcionario[0]->nm_local; ?> " />
                        <input type="hidden" class="col-lg-4 form-control     " name="st_os" value="Aberto" id="st_os">
                    </div>





                    <div class="form-group col-lg-12">

                        <label for="dataFinal">Título *</label>
                        <input id="titulo_os"    class="col-lg-2 form-control     "  type="text"  name="titulo_os" />
                    </div>

                    <div class="form-group col-lg-6" style="padding:1%;">
                        <label for="descricaoProduto">Problema * </label>
                        <textarea class="col-lg-12 form-control      " name="df_os" id="df_os" cols="30" rows="5"></textarea>
                    </div>
                    <div class="form-group col-lg-6" style="padding:1%;"> <label for="defeito">Observações </label>
                        <textarea class="col-lg-12 form-control      " name="ob_os" id="ob_os" cols="30" rows="5"></textarea> 
                    </div>
            </div>









    </div>
</div>
<footer class="panel-footer">
    <button class="btn btn-primary" ><i class="fa fa-check"></i> Cadastrar</button> 

</footer>

</form>










</div>
</section>
</div>
</div>






<!--modal --ver -->



<?php
if ($funcionario) {
    foreach ($funcionario as $funcionarios) {
        ?>
        <div id="atender_<?php echo $funcionarios->id_funcionario; ?>"  class="modal-block modal-block-md mfp-hide ">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">ERPOS</h2>
                </header>
                <div class="panel-body">
                    <div class="modal-wrapper">
                        <div class="modal-text">
                            <label for="dataFinal">Título *</label>
                            <input id="titulo_os"    class="col-lg-2 form-control     "  type="text"  name="titulo_os" />
                            <div class="col-lg-6" style="padding:1%;">
                                <label for="descricaoProduto">Problema * </label>
                                <textarea class="col-lg-12 form-control      " name="df_os" id="df_os" cols="30" rows="5"></textarea>
                            </div>
                            <div class="col-lg-6" style="padding:1%;"> <label for="defeito">Observações </label>
                                <textarea class="col-lg-12 form-control      " name="ob_os" id="ob_os" cols="30" rows="5"></textarea> </div>
                        </div>


                        <div class="col-lg-12" style="padding: 1%; ">
                            <div class="text-right">
                                <button class="btn btn-primary" ><i class="fa fa-check"></i> Cadastrar</button> <a href="#" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>

        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm">Fechar</button>

                </div>
            </div>
        </footer>
        </section>
        </div>
        <?php
    }
}
?>

