<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script>
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php if ($filtro) {
                                                                    echo 'in';
                                                                } else {
                                                                    echo '';
                                                                } ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Secretaria</label>
                                    <select class="form-control      select2" id="id_secretaria" name="cid_secretaria" onchange="busca_setor($(this).val())" style="width: 100%;">
                                        <option></option>
                                        <?php if ($secretaria) {
                                            foreach ($secretaria as $secretarias) { ?>
                                                <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
                                        <?php
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Setor</label>
                                    <select class="form-control      select2" id="id_setor" name="id_setor" style="width: 100%;">


                                    </select>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Funcionário</label>
                                    <input type="text" class="form-control  select2" id="os_busca_nome" name="busca_nome" style="width: 100%;">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn <?php if ($filtro) {
                                                                                                echo 'btn bg-quartenary';
                                                                                            } else {
                                                                                                echo 'btn btn-info';
                                                                                            } ?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
                                                                                                                                                echo 'Desativar Filtro';
                                                                                                                                            } else {
                                                                                                                                                echo 'Ativar Filtro';
                                                                                                                                            } ?>" style="width: 100%;">
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-condensed mb-none">

            <tr">
                                <th>#id</th>
                                <th>Funcionário</th>
                                <th>Resp</th>
                                <th>Cargo</th>
                                <th>Secretaria</th>
                                <th>Setor</th>
                                <th>Ramal/Telefone</th>

                                <th colspan="4">Ações</th>
                            </tr>
                </thead>
                <tbody style="text-align: center;">
                    <tr>
                    
                                <?php if ($funcionario) {
                                        foreach ($funcionario as $funcionarios) { ?>
                                <?php if ($funcionarios->id_funcionario <> '1') { ?>
                                <td>
                                    <?php echo $funcionarios->id_funcionario; ?>
                                </td>
                                <td>
                                    <?php echo $funcionarios->nm_funcionario; ?>
                                </td>
                                <td>
                                    <?php echo $funcionarios->n_resp; ?>
                                </td>
                                <td>
                                    <?php echo $funcionarios->nm_cargo; ?>
                                </td>
                                <td>
                                    <?php echo $funcionarios->nm_secretaria; ?>
                                </td>
                                <td>
                                    <?php echo $funcionarios->nm_setor; ?>
                                </td>
                                <td>
                                    <?php echo $funcionarios->n_ramal; ?>
                                </td>
                                <td>
                                    <div class="btn-group-horiontal">
                                        <a class=" mt-xs mr-xs modal-sizes btn-xs btn btn-default" href="#ver_<?php echo $funcionarios->id_funcionario; ?>" title="visualizar" class="btn  btn-flat btn-default"><i class="fa fa-eye"></i>Ver</a>
                                        <a href="<?php echo base_url(); ?>index.php/os/open/<?php echo encript($funcionarios->id_funcionario); ?>" title="Abrir Chamado" class="btn btn-xs btn-primary"><i class="fa fa-ticket"></i> Abrir</a>

                                    </div>
                                </td>
                            
                    </tr>
                <?php } }
                        } else { ?>

                <td colspan="8">
                    <center>Nenhuma secretaria cadastrada</center>
                </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination">
                    <?php if (isset($pag) && !empty($pag)) {
                        foreach ($pag as $key => $value) {
                            echo " {$value} ";
                        }
                    }; ?>
            </div>
        </div>
</section>


<!--modal --ver -->
<!--

-->
<?php
if ($funcionario) {
    foreach ($funcionario as $funcionarios) {
        ?>
<div id="ver_<?php echo $funcionarios->id_funcionario; ?>"  class="modal-block modal-block-md mfp-hide ">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-text">
                             <b>Id:</b>
                            <?php echo $funcionarios->id_funcionario; ?><br>
                            <b>Funcionário: </b>
                            <?php echo $funcionarios->nm_funcionario; ?>
                            <br>
                            <b>Nº Resp: </b>
                            <?php echo $funcionarios->n_resp; ?>
                            <br>
                            <b>E-mail: </b>
                            <?php echo $funcionarios->mail_funcionario; ?>
                            <br>
                            <b>Cargo: </b>
                            <?php echo $funcionarios->nm_cargo; ?>
                            <br>
                            <b>Nome de Usuário do Sistema: </b>
                            <?php echo $funcionarios->nm_usuario; ?>
                            <br>
                            <b>Secretaria: </b>
                            <?php echo $funcionarios->nm_secretaria; ?>
                            <br>
                            <b>Setor: </b>
                            <?php echo $funcionarios->nm_setor; ?>
                            <br>
                            <b>Local: </b>
                            <?php echo $funcionarios->nm_local; ?>
                            <br>
                            <b>Obs local de trabalho: </b>
                            <?php echo $funcionarios->obs_local; ?>
                            <br>
                            <b>Responsável do Setor: </b>
                            <?php echo $funcionarios->nm_responsavel; ?>
                            <br>
                            <b>Nº Ramal/Telefone: </b>
                            <?php echo $funcionarios->n_ramal; ?>
                            <br>
                            <b>Status: </b>
                            <?php if ($funcionarios->ativo) {
                                                echo 'ativo';
                                            } else {
                                                echo 'inativo';
                                            } ?>
                        </div>

													</div>
												
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-primary modal-confirm">Fechar</button>
														
													</div>
												</div>
											</footer>
										</section>
                                    </div>
                                    <?php

}
}
?>

