<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Database Error</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>

		<?php
		//pega os valores do retorno de erro por violação de chave fk
		$cargo=strpos($message, 'tb_cargo');//195
		$secretaria=strpos($message, 'tb_secretaria');//199
		$setor=strpos($message, 'tb_setor');//201
		?>
		<?php $sub_string = substr($message,16,5);?>
		<?php if($sub_string=='1451'){
			set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
			<section class="panel">
				<header class="panel-heading">
					<h2 class="panel-title">ERPOS</h2>
				</header>
				<div class="panel-body">
					<div class="modal-wrapper">
						<div class="modal-icon">
							<i class="fa fa-info-circle"></i>
						</div>
						<div class="modal-text">
							<h4>Informação</h4>
							<p>Registro não pode ser apagado pois já esta em uso.</p>
						</div>
					</div>
				</div>
				<footer class="panel-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<button class="btn btn-info modal-dismiss">OK</button>
						</div>
					</div>
				</footer>
			</section>
		</div>','sucesso');
		/*	set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-warning"></i> Atenção!</h4>
     O Registro não pode ser apagado pois o mesmo está em uso.
   </div>', 'sucesso'); */
   if($cargo){
	redirect('cargo', 'refresh');
   }
   if($secretaria){
	set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">ERPOS</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-icon">
					<i class="fa fa-info-circle"></i>
				</div>
				<div class="modal-text">
					<h4>Informação</h4>
					<p>Registro não pode ser apagado pois já esta em uso.</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-info modal-dismiss">OK</button>
				</div>
			</div>
		</footer>
	</section>
</div>','sucesso');
	 redirect('secretaria', 'refresh');
	
	
   }
   if($setor){
	redirect('setor', 'refresh');
   }
	}else{?>	
		<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message;?> <?php echo '</div>'; }?>
	
</body>
</html>