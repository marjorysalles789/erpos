<!-- start: page -->
<section class="body-error error-inside">
    <div class="center-error">

        <div class="row">
            <div class="col-md-8">
                <div class="main-error mb-xlg">
                    <h2 class="error-code text-dark text-center text-semibold m-none">404 <i class="fa fa-file"></i></h2>
                    <p class="error-explanation text-center">A página que você tentou acessar parece não existir.</p>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="text">Você pode usar alguns link:</h4>
                <ul class="nav nav-list primary">
                    <li>
                        <a href="#"><i class="fa fa-caret-right text-dark"></i> Inicio</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-caret-right text-dark"></i> Meu Perfil</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-caret-right text-dark"></i> FAQ's</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->