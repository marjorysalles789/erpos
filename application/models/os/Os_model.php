<?php
class Os_model extends CI_Model {

    /**
     * author: Wesley da Silva Pereira
     * email: wesley_cras@hotmail.com
     * 
     */
    
    function __construct() {
        parent::__construct();
        $this->load->model('funcionario/Funcionario_model','Mfuncionario');
        
    }

    
    function get($table,$fields,$where='',$join='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
       // $this->db->order_by('id_categoria','asc');
       
        $this->db->limit($perpage,$start);
        if($join){
           $this->db->join($join);
            
        }
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getFuncionario($id){
        $this->db->where('',$id);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{  
            $negociacao_id = $this->db->insert_id();
			return $negociacao_id;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }

    function count($table,$where) {
       
            
        
      
        return    $this->db->where($where)->get($table)->result(); 
    }
    public function contar($table){
    
            return $this->db->count_all($table);
        
    }
    
    public function getCat($id){
        $this->db->where('id_categoria',$id);
        $this->db->order_by('id_categoria','desc');
       // $this->db->limit(10);
        return $this->db->get('tb_categoria')->result();
    }
    function getMarca($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('id_marca','asc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    function getCategoria($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('id_categoria','asc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    public function verificaUnicidade($table,$fields,$where){
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function geraTabela($tabela,$where)
    {
         /*
            toda pesquisa é passada por session.

         */
       
        
     
        //$config['suffix'] = '/cid_setor='.$where['cid_setor'].'&id_secretaria='.$where['id_secretaria'].'&nm_funcionario='.$where['nm_funcionario'].'&st_os='.$where['st_os'];
        //Teste com where
        
        if($this->uri->segment(3)){
            $pular=$this->uri->segment(3);
        }else{
            $pular=0;
        }
        //filtros aplicar aqui 
        $this->load->library('pagination');
       /* $cid_secretaria=$this->input->get('id_secretaria');
        $id_setor=$this->input->get('id_setor');
        $st_os=$this->input->get('st_os');
        $data2=$this->input->get('data2');
        $data1=$this->input->get('data1');
        $busca_nome=$this->input->get('busca_nome');
        $tecnico_nome=$this->input->post('tecnico_nome');
        */
        $where=array(
            'cid_secretaria'=>$this->session->userdata('id_secretaria'),
            'id_setor'=>$this->session->userdata('cid_setor'),
            'st_os'=>$this->session->userdata('st_os'),
            'nm_funcionario'=>$this->session->userdata('busca_nome')

        );
       
        $url='cid_secretaria='.$this->session->userdata('id_secretaria').'&id_setor'.$this->session->userdata('cid_setor').'&st_os='.$this->session->userdata('st_os');
        
    
        
        
       ####parei aqui quase resolvendo essa porra
           

            $this->load->library('pagination');
            $config['suffix']='?'.$url;
            $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
            $config['total_rows'] = count($this->Mos->listaOsLike($panda=null,$nada=null,'v_os','*',$where));
          
            $config['per_page'] =10;
           
          
              
         $this->pagination->initialize($config);
         $order='v_os';
        $os= $this->Mos->listaOsLike($pular,$config['per_page'],'v_os','*',$where,$order);
      
        echo '<table class="table table-bordered ">';
                                
       echo '<thead>';
       echo  '<tr style="backgroud-color: #2D335B">';
             echo '<th>#OS</th>';
              echo '<th>Data</th>';
              echo '<th>Hora</th>';
              echo '<th>Secretaria</th>';
              echo '<th>Setor</th>';
              echo '<th>Funcionário</th>';
              echo  '<th>Status</th>';
              echo  '<th>Ramal/Telefone</th>';
                echo '<th>Ações</th>';
              echo  '</tr>';
              echo  '</thead>';
             
       echo '<tbody >';  
       echo  '<tr>';
        if($os){ foreach ($os as $oss) {
          echo '<td>'.$oss->id_os.'</td>';
         echo  '<td>'.formataVisao($oss->dt_os).'</td>';
         echo  '<td>'.$oss->hr_os.'</td>';
          echo '<td>'.$oss->nm_secretaria.'</td>';
          echo '<td>'.$oss->nm_setor.'</td>';
          echo '<td>'.$oss->nm_funcionario.'</td>';
         echo '<td>';
         if($oss->st_os=='Aberto') echo '<div class="bg-blue text-center">'.$oss->st_os.'</div>';
          if ($oss->st_os=='Resolvendo'){ echo '<div class="bg-orange-active text-center" >'.$oss->st_os.'</div>';
          }
          if ($oss->st_os=='Cancelado'){ echo '<div class="bg-red text-center" >'.$oss->st_os.'</div>';
          }
          if ($oss->st_os=='Parado'){ echo '<div class="bg-gray-active text-center" >'.$oss->st_os.'</div>';
          }
          echo'</td>';
          echo '<td>'.$oss->n_ramal.'</td>';
          echo '<td> <div class="btn-group-horiontal">';
           echo  '<a  data-toggle="modal" data-target="#ver_'.$oss->id_os.'" title="visualizar" class="btn btn-flat btn-default"><i class="fa fa-eye"></i></a>';
          echo   '<a href="'.base_url().'index.php/os/answer/'.encript($oss->id_os).'"     title="Atender chamdo" class="btn btn-flat btn-success"><i class="fa fa-check"></i></a>';
          echo   '<a data-toggle="modal" data-target="#modal-excluir_'.$oss->id_os.'" title="Imprimir OS"  class="btn btn-flat bg-navy"><i class="fa fa-print"></i></a>';
          
          
        
          echo "</div></td>"; 
         


           echo "</tr>"; 
           




        }
    }else{
        echo "<td colspan='8'><center>Nenhum funcionario encontrado.</center></td></tr>";
    }

   echo '<tfoot>';
   echo '<tr>';
  echo'<td colspan="9" style="align:right">';      
  echo $this->pagination->create_links();
  echo '</td>';
  echo  '</tr>';
   
echo'</tfoot>';
    echo'</tbody>';
              
       
            
         
     
    }
    public function geraSelect($tabela,$where)
    {

        $this->db->select('*');
       // $this->db->limit();
        $this->db->where($where);
        $query = $this->db->get($tabela)->result();
       $options='<option></option>';
        foreach ($query as $setor) {
           $options.="<option value='{$setor->id_setor}'>{$setor->nm_setor}</option>".PHP_EOL;
        }
        return $options;
            
            
        
    } 

  public function listaOsLike($pular=null,$row_perpage=null,$table,$fields,$like=null,$porData=null,$where=null,$order=null){
     
    $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($row_perpage,$pular);
        
    
        $this->db->order_by('id_os','desc');
    if($where){
        $this->db->where($where);
    }
    if($like){
        if(!empty($like['cid_tecnico'])){
            $this->db->where($like);
        }else{
        $this->db->like($like,'before');
        }
    }
    if($porData){
       
   $this->db->where("dt_os >='".$porData['de']."' and dt_os <='".$porData['ate']."'");

  
    }
    
   // $this->db->where("st_os <>'Fechado'"); para mostrar somente os que ainda estão sendo feitos
    
    

    
    $query = $this->db->get();
        
    $result =   $query->result();
        return $result;

  }

  public function getMovimentacao($tabela,$where){
    $this->db->select('*');
    $this->db->from($tabela);
    $this->db->where($where);
    $this->db->order_by('id_movimentacao','desc');
    $query = $this->db->get();
        
    $result =   $query->result();
        return $result;


  }


    function getOslike($table,$fields,$like='',$join='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
       // $this->db->order_by('id_categoria','asc');
       $this->db->order_by('id_os','desc');
        $this->db->limit($perpage,$start);
        if($join){
            $this->db->join('tb_cargo','cid_cargo=id_cargo');
            $this->db->join('tb_local_trabalho','cid_local=id_local');
            $this->db->join('tb_setor','cid_setor=id_setor');
            $this->db->join('tb_secretaria','cid_secretaria=id_secretaria');
        }
        if($like){
            $this->db->like($like,'before');
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    public function geraTabelaFuncionario($tabela,$where)
    {

      
        	
        
        
        if($this->uri->segment(3)){
            $pular=$this->uri->segment(3);
        }else{
            $pular=0;
        }
        //filtros aplicar aqui 
        $this->load->library('pagination');
        $where=array(
            'cid_secretaria'=>$this->session->userdata('id_secretaria'),
            'id_setor'=>$this->session->userdata('cid_setor'),
            'st_os'=>$this->session->userdata('st_os'),
            'nm_funcionario'=>$this->session->userdata('busca_nome')

        );

       ####parei aqui quase resolvendo essa porra
           

            $this->load->library('pagination');
            $config['suffix']='?'.$url;
            $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
            $config['total_rows'] = count($this->Mos->listaOsLike($panda=null,$nada=null,'v_funcionario','*',$where));
          
            $config['per_page'] =10;
           
          
              
         $this->pagination->initialize($config);
         $order='v_funcionario';
        $funcionario= $this->Mos->listaOsLike($pular,$config['per_page'],'v_funcionario','*',$where,$order);
       
        echo'<table class="table table-bordered ">';
                                
       echo '<thead>';
       echo     '<tr style="backgroud-color: #2D335B">';
       echo '<th>#id</th>';
       echo '<th>Funcionário</th>';
       echo '<th>Resp</th>';
       echo '<th>Cargo</th>';
       echo '<th>Secretaria</th>';
       echo '<th>Setor</th>';
       echo '<th>Ramal/Telefone</th>';
       echo '<th>Ações</th>';
      echo '</tr>';
      echo '</thead>';
      echo '<tbody>';
      echo '<tr>';
     if($funcionario){ foreach ($funcionario as $funcionarios) {
       echo '<td>'.$funcionarios->id_funcionario.'</td>';
        echo'<td>'.$funcionarios->nm_funcionario.'</td>';
        echo '<td>'.$funcionarios->n_resp.'</td>';
        echo'<td>'.$funcionarios->nm_cargo.'</td>';
       echo  '<td>'.$funcionarios->nm_secretaria.'</td>';
       echo '<td>'.$funcionarios->nm_setor.'</td>';
       echo '<td>'.$funcionarios->n_ramal.'</td>';
    echo'<td> <div class="btn-group-horiontal">';
   echo  '<a  data-toggle="modal" data-target="#ver_'.$funcionarios->id_funcionario.'" title="visualizar" class="btn btn-flat btn-default"><i class="fa fa-eye"></i></a>';
   echo '<a href="'.base_url().'index.php/os/open/'.encript($funcionarios->id_funcionario).'"  title="Abrir OS" class="btn btn-flat btn-success"><i class="ion ion-ios-pricetag-outline"></i> Abri OS</a>';
 
        echo '</div></td>';
     echo '</tr>';
      } }else{  
      
     echo '<td colspan="8"><center>Nenhum funcionario cadastrado</center> </td>';
     
     echo '</tr>';
      }
      echo '<tfoot>';
      echo '<tr>';
     echo'<td colspan="9" style="align:right">';      
     echo $this->pagination->create_links();
     echo '</td>';
     echo  '</tr>';
      
   echo'</tfoot>';
       echo'</tbody>';
       
    }


    public function listaOsTecnicoLike($pular=null,$row_perpage=null,$table,$fields,$like=null,$porData=null,$where=null,$order=null){
     
        $this->db->select($fields);
            $this->db->from($table);
            $this->db->limit($row_perpage,$pular);
            
        
           // $this->db->order_by('id_os','desc');
        
        if($like){
            if(!empty($like['cid_tecnico'])){
                $this->db->where($like);
            }else{
            $this->db->like($like,'before');
            }
        }
        if($porData){
       $this->db->where("dt_os >='".$porData['de']."' and dt_os <='".$porData['ate']."'");
    
      
        }
        
       // $this->db->where("st_os <>'Fechado'"); para mostrar somente os que ainda estão sendo feitos
        
        
    
        
        $query = $this->db->get();
            
        $result =   $query->result();
            return $result;
    
      }


}