<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargo extends CI_Controller {

	/**
	 * 
	 */
    public function __construct() {
        parent::__construct();
        
        $this->load->model('cargo/Cargo_model','MCargo');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

	public function index()
	{
        //index manda para funcão principal gerenciar
      $this->gerenciar();
        
    }
    public function gerenciar(){

          //filtro nova os
          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['nm_cargo']) && !empty($_GET['nm_cargo'])){   
        $url='';
        
        if(isset($_GET['nm_cargo']) && !empty($_GET['nm_cargo'])){
            $filtro=true;
            $url='&nm_cargo='.$_GET['nm_cargo'];
            $like['nm_cargo']=$_GET['nm_cargo'];  
        }
        
       
       
    }else{
        $like=null;
        $url='';
    }
    
        //primeiro passas as  configuraçoes para pagination

     //$data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
     $p=0;//inicio do contador da paginação
     $pg=1;
     $total_registros= count($this->MCargo->listaCargoLike($p=0,$por_pagina=null,'tb_cargo','*',$like,$order=null));//pegar total registros
     $per_page=5;//numero de registros por paginas;
     $paginas=$total_registros/$per_page;
       
     if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;
       
 


        //primeiro busca todos os dados no banco 
        $data['filtro']=$filtro;
        $data['cargo1'] = $this->MCargo->listaCargoLike($p,$per_page,'tb_cargo','*',$like,$order=null);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
       
        $data['titulo']='Cargo';
        $data['view']='admin/cargo/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
            $this->load->view('theme/header',$data);

    }
    public function novo(){
       //função que chama o formulario para adição de novo cargo
       $data = null;
       $data['titulo']='Adicionar Cargo';
       $data['view']='admin/cargo/add';//passa a view por padrao
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }
    public function save(){
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');

        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nome');
           
            //monta o array de dados
            $dados=array(
                'nm_cargo'=>$nome
               
            );
            
            if ($this->MCargo->verificaUnicidade('tb_cargo','nm_cargo',$where=['nm_cargo'=>$nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a>
                 <div id="modalSuccess" class="modal-block modal-block-warning mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-warning"></i>
													</div>
													<div class="modal-text">
														<h4>Atenção</h4>
														<p>Esse cargo já esta cadastrado.</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-warning modal-dismiss">OK</button>
													</div>
												</div>
											</footer>
										</section>
									</div>', 'sucesso');
                redirect('cargo/novo', 'refresh');
            } else {
                $this->MCargo->add('tb_cargo', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                 set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a>
                 <div id="modalSuccess" class="modal-block modal-block-success mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-check"></i>
													</div>
													<div class="modal-text">
														<h4>Sucesso!</h4>
														<p>Cargo adicionado com sucesso.</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-success modal-dismiss">OK</button>
													</div>
												</div>
                                            </footer></div> ','sucesso');
                
                redirect('cargo', 'refresh');
                
                /*  set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Cargo adicionado com sucesso.
                 </div>', 'sucesso');
                  redirect('cargo', 'refresh'); */
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
            $data = null;
            $data['titulo']='Adicionar Cargo';
            $data['view']='admin/cargo/add';//passa a view por padrao
            $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
            $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
            $this->load->view('theme/header',$data);
           // $this->load->view('theme/header');
           // $this->load->view('admin/cargo/add', $data);
           // $this->load->view('theme/footer');
        }
    }
    public function edit(){
      //pega parametro da url via get
      $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
      $config['per_page']=null;
      $data['cargo1'] = $this->MCargo->get('tb_cargo', '*', $where =['id_cargo'=>$id], $config['per_page'], null);
      
      $data['titulo']='Editar Cargo';
      $data['view']='admin/cargo/edit';//passa a view por padrao
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
         $this->load->view('theme/header',$data);
    }

    public function update(){
     $id=$this->input->post('id_cargo');
     $nome=$this->input->post('nm_cargo'); 
     
     //editar  meio parecido com save
     $this->form_validation->set_rules('nm_cargo', 'Nome', 'required');

     if ($this->form_validation->run()) {
         //if this check of validation ok, to do  this action
         
        
         $nome = $this->input->post('nm_cargo');
        
         //monta o array de dados
         $dados=array(
             'nm_cargo'=>$nome
             
         );
         
         if ($this->MCargo->verificaUnicidade('tb_cargo','nm_cargo',$where=['nm_cargo'=>$nome])) {
              //Verifica se já existe esse dada cadastrado no banco de dados
             set_msg('salvo', '<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalWarning" class="modal-block modal-block-warning mfp-hide">
             <section class="panel">
                 <header class="panel-heading">
                     <h2 class="panel-title">ERPOS</h2>
                 </header>
                 <div class="panel-body">
                     <div class="modal-wrapper">
                         <div class="modal-icon">
                             <i class="fa fa-warning"></i>
                         </div>
                         <div class="modal-text">
                             <h4>Atenção!</h4>
                             <p>Esse cargo já esta cadastrado.</p>
                         </div>
                     </div>
                 </div>
                 <footer class="panel-footer">
                     <div class="row">
                         <div class="col-md-12 text-right">
                             <button class="btn btn-warning modal-dismiss">OK</button>
                         </div>
                     </div>
                 </footer>
             </section>
         </div>', 'sucesso');
             redirect('cargo/novo', 'refresh');
         } else {
             $this->MCargo->edit('tb_cargo',$dados,'id_cargo',$id);
             //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
             set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a>
                 <div id="modalSuccess" class="modal-block modal-block-success mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-check"></i>
													</div>
													<div class="modal-text">
														<h4>Sucesso!</h4>
														<p>Cargo Cargo Atualizado.</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-success modal-dismiss">OK</button>
													</div>
												</div>
                                            </footer></div> ','sucesso');
                
                redirect('cargo', 'refresh');  
             
             
            
         }
           
         
         
     } else {
         //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
        
         $data['titulo']='Editar Cargo';
         $data['view']='admin/cargo/edit';//passa a view por padrao
         $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
         $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
         $this->load->view('theme/header',$data);
     }



    }
    public function delete(){
     $id=decrypt($this->uri->segment(3));
     //deletar simples sem confirmação por hora
     
      
     try {
       //senão tiver executa a exclusão.
     $this->MCargo->delete('tb_cargo','id_cargo',$id);
     
     
     set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
     <section class="panel">
         <header class="panel-heading">
             <h2 class="panel-title">ERPOS</h2>
         </header>
         <div class="panel-body">
             <div class="modal-wrapper">
                 <div class="modal-icon">
                     <i class="fa fa-info-circle"></i>
                 </div>
                 <div class="modal-text">
                     <h4>Informação</h4>
                     <p>Registro apagado.</p>
                 </div>
             </div>
         </div>
         <footer class="panel-footer">
             <div class="row">
                 <div class="col-md-12 text-right">
                     <button class="btn btn-info modal-dismiss">OK</button>
                 </div>
             </div>
         </footer>
     </section>
 </div>','sucesso');
 redirect('cargo', 'refresh'); 
     
     /*set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
    redirect('cargo', 'refresh'); */
    } catch (Exception $e) {
        echo 'Exceção capturada: ',  $e->getMessage(), "\n";
    }
  

    
    }
    
    
    
    
}

