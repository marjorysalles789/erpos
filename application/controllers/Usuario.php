<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuario extends CI_Controller{



public function __construct()
{
    parent::__construct();
    //Do your magic here
    $this->load->model('funcionario/Funcionario_model','Mfuncionario');
    $this->load->helper('funcao_helper');
    
 
    
}

public function index(){

  if($this->session->userdata('id_funcionario')){
      
			
		
    redirect('painel', 'refresh');
     
     }
  $this->load->view('usuario/login/index');
  

}


public function logar(){
/*
 função logar do sistema essa função é acessada somente do front do usuario
 é chamada por ajax e retorna 1 ou 0 para o js do navegador
 implementada dia 16/05/2019 sistema ERPOS
 configurado para salvar sessão no banco de dados evitando sobrecarga no servidor web
*/
$user=$this->input->post('nm_usuario');
$senha=$this->input->post('senha');

$where=array(
  'nm_usuario'=>$user,
  'senha'=>$senha
);

$resultado=$this->Mfuncionario->verificaUnicidade('v_funcionario','*',$where);


if($resultado){
 
 $this->session->set_userdata($resultado);//salva todos os dados do funcionario na sessão                                                                                                                                                                                                                                                             
 
//var_dump($this->session->userdata('nm_funcionario')); consigo pegar cada dado pelo nome da variavel
//usuario logado redireciona pra principal
redirect('painel', 'refresh');

}else{
  set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Usuário ou senha incorretos.</label></center>', 'sucesso');
  redirect('usuario', 'refresh');
}

}

public function logout(){
  $this->Mfuncionario->delete('ci_sessions','id',$this->session->session_id);
  $this->session->sess_destroy($this->session->session_id);
  
  
  
  redirect('usuario','refresh');
 
 

}

public function registrar(){
  $data = null;
        $config['per_page'] = null;
        $data['cargo'] = $this->Mfuncionario->get('tb_cargo', '*', $where = '', $config['per_page'], $this->uri->segment(3));
        $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '', $config['per_page'], $this->uri->segment(3));
        $this->load->view('usuario/login/register',$data);

}

public function save(){

  $nome = $this->input->post('nome');
  $nm_local = $this->input->post('nm_local');
  $id_setor = $this->input->post('id_setor');
  $n_ramal = $this->input->post('n_ramal');
  $obs_local = $this->input->post('obs_local');
  $cid_cargo = $this->input->post('cid_cargo');
  $cid_secretaria = $this->input->post('cid_secretaria');
  $mail_funcionario = $this->input->post('mail_funcionario');
  $n_resp= $this->input->post('n_resp');
  $nm_usuario=$this->input->post('nm_usuario');
  $senha=$this->input->post('senha');
  //monta o array de dados

  $dlocal = array(
      'cid_setor' => $id_setor,
      'nm_local' => $nm_local,
      'n_ramal' => $n_ramal,
      'obs_local' => $obs_local,
      'cid_sec'=>$cid_secretaria

  );
 //salva primeiro o local de trabalho
 $id_local = $this->Mfuncionario->add('tb_local_trabalho', $dlocal);//salva local de trabalho e retorna o id inserido
                
 /* cria os dados pra serem inseridos no novo funcionar junto com o id do local */
 $dados = array(
     'cid_local'=>$id_local,
     'cid_cargo'=>$cid_cargo,
     'mail_funcionario'=>$mail_funcionario,
     'n_resp'=>$n_resp,
     'nm_funcionario'=>$nome,
     'nm_usuario'=>$nm_usuario,
     'senha'=>$senha,
     'ativo'=>1
 );
//dispara email com confirmação
$this->envia_email($nm_usuario,$senha,$mail_funcionario,$nome);


 $this->Mfuncionario->add('tb_funcionario', $dados);
 //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
 set_msg('salvo', '<div class="alert alert-success alert-dismissible">
   
    Seu cadastro foi efetuado com sucesso. 
  </div>', 'sucesso');
 redirect('usuario', 'refresh');



}


public function envia_email($usuario=null,$senha=null,$email=null,$nome){
//ao termino de adicionar uma nova ordem de serviço é disparado um email.




 $this->load->library('email');

   $subject = 'Prefeitura Municipal de Patrocínio| CPD-chamados';
   $message = '<div class="tudo">
<h3>CPD- Prefeitura Municipal de Patrocínio</h3>
<br><p> Olá <b>'.$nome.'</b> sua conta foi criada.</p>
<p>Agora você poderá criar e acompanhar seus pedidos de suporte ao CPD da Prefeitura.</p>
<div class="balao">

<table>
<tr>
<td ><p><b>Seus dados de acesso</b></p></td>
</tr>
<tr>
<td><p><b>Usuário:</b> '.$usuario.'</p></td>
</tr>
<tr>
<td><p><b>Senha:</b> '.$senha.'</p></td>
</tr>
</table>

</div>
<br>
<br>
<hr>
<p>Qualquer dúvida entre em contato conosco 3839-1801 ramais 476,213 ou 500.</p>
<p><b>CPD- Prefeitura de Patrocinio Minas Gerais.</b></p>
<p><small>cpdpmp@patrocinio.mg.gov.br</small>.</p>
</div>';
    
    $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
    <title>' . html_escape($subject) . '</title>
    <style type="text/css">
    body {
      font-family: Arial, Verdana, Helvetica, sans-serif;
      font-size: 16px;
  }
 
.balao{
  border: 2px solid #605ca8;
  background-color: silver;
  padding:1%;
  color:#5C2582;
}
.tudo{
  background-color:white;
  color:black;
  padding:1%;
  border:1px solid #605ca8;

    
    </style>
</head>
<body>
' . $message . '
</body>
</html>';

$result = $this->email
    ->from('suporte@wepsistemas.net.eu.org')
    ->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
    ->to($email)
    ->subject($subject)
    ->message($body)
    ->send();





}





}








?>
