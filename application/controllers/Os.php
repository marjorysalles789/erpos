<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Os extends CI_Controller
{

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('os/Os_model', 'Mos');
        $this->load->model('secretaria/Secretaria_model','Msecretaria');
        $this->load->model('funcionario/Funcionario_model','Mfuncionario');
        $this->load->model('tecnico/Tecnico_model', 'Mtecnico');
        $this->load->library('form_validation');
        $this->load->helper('date');
        $this->load->helper('fpdf_helper');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

    public function index()
    {  
        $this->gerenciar();
        
    }
    public function gerenciar()
    {   
        
        $data['tecnico']=$this->Mtecnico->listaTecnicoLike($p=0,$por_pagina=null,'v_tecnico','*',$like=null,$order=null);
        
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['id_secretaria'])&& !empty($_GET['id_secretaria']) or (isset($_GET['id_setor']) && !empty($_GET['id_setor']) or (isset($_GET['st_os'])&& !empty($_GET['st_os'])) or (isset($_GET['busca_nome'])&& !empty($_GET['busca_nome'])) or (isset($_GET['data1'])&& !empty($_GET['data1'])) or (isset($_GET['data2'])&& !empty($_GET['data2'])) or isset($_GET['cid_tecnico'])&& !empty($_GET['cid_tecnico']) )){   
        $url='';
        $like=null;
        $porData='';
        $where='';
        if(isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria'])){
            $filtro=true;
            $url='&id_secretaria='.$_GET['id_secretaria'].'&id_setor='.$_GET['id_setor'].'&st_os='.$_GET['st_os'].'&busca_nome='.$_GET['busca_nome'];
            $like['cid_secretaria']=$_GET['id_secretaria'];  
        }
        if(!empty($_GET['id_setor'])){
            $filtro=true;
            $url=$url.'&id_setor='.$_GET['id_setor'];
            $like['id_setor']=$_GET['id_setor'];
        }
        if(!empty($_GET['st_os'])){
            $filtro=true;
            $url=$url.'&st_os='.$_GET['st_os'];
            $like['st_os']=$_GET['st_os'];
        }
        if(!empty($_GET['busca_nome'])){
            $filtro=true;
            $url=$url.'&busca_nome='.$_GET['busca_nome'];
            $like['nm_funcionario']=$_GET['busca_nome'];
        }
        if(!empty($_GET['data1']) && (!empty($_GET['data2']))){
            $filtro=true;
            $url=$url.'&data1='.$_GET['data1'].'&data2='.$_GET['data2'];
            $porData=array('de'=>formataBD($_GET['data1']),'ate'=>formataBD($_GET['data2']));
            
        }else{
            $url=$url.'&data1=';
        }
        if(!empty($_GET['cid_tecnico'])){
            $filtro=true;
            $url=$url.'&cid_tecnico='.$_GET['cid_tecnico'];
            $like['cid_tecnico']=$_GET['cid_tecnico'];
        }

        
       
       
    }else{
        $like=null;
        $url='';
        $porData='';
        $where='';
    
    }
    
 /* busca por data esta funcionando 06-04-2019
     wesley da silva pereira  
 */


        //primeiro passas as  configuraçoes para pagination

        $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
        $p=0;//inicio do contador da paginação
        $pg=1;
        $total_registros= count($this->Mos->listaOsLike($p=0,$por_pagina=null,'v_os','*',$like,$porData,$order=null));//pegar total registros
        $per_page=15;//numero de registros por paginas;
        $paginas=$total_registros/$per_page;
    
       



        if(isset($_GET['p']) && !empty($_GET['p'])){
            $pg=addslashes($_GET['p']);
        }
        $p=($pg-1)*$per_page;
        #carrega todos os tecnicos para o filtro
        

        $order='v_os';
        $data['os']=$this->Mos->listaOsLike($p,$per_page,'v_os','*',$like,$porData,$where,$order);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['filtro']=$filtro;
        $data['titulo']='Chamados';
        $data['view']='admin/os/index';//passa a view por padrao
        $data['jscript']='theme/footer';
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
         $this->load->view('theme/header',$data);
       
       
      

       
            
    }
    public function novo()
    {
       //função que chama o formulario para adição de novo os
       //carregar secretaria todas p/ fazer o carregamento dos setores
       
        //filtro nova os
          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['id_secretaria'])&& !empty($_GET['id_secretaria']) or (isset($_GET['id_setor']) && !empty($_GET['id_setor']) or (isset($_GET['busca_nome'])&& !empty($_GET['busca_nome'])) )){   
        $url='';
        
        if(isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria'])){
            $filtro=true;
            $url='&id_secretaria='.$_GET['id_secretaria'].'&id_setor='.$_GET['id_setor'].'&busca_nome='.$_GET['busca_nome'];
            $like['cid_secretaria']=$_GET['id_secretaria'];  
        }
        if(!empty($_GET['id_setor'])){
            $filtro=true;
            $url=$url.'&id_setor='.$_GET['id_setor'];
            $like['id_setor']=$_GET['id_setor'];
        }
       
        if(!empty($_GET['busca_nome'])){
            $filtro=true;
            $url=$url.'&busca_nome='.$_GET['busca_nome'];
            $like['nm_funcionario']=$_GET['busca_nome'];
        }
       
       
    }else{
        $like=null;
        $url='';
    }
    
     //primeiro passas as  configuraçoes para pagination

     //$data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
     $p=0;//inicio do contador da paginação
     $pg=1;
     $total_registros= count($this->Mfuncionario->listaFuncionariosLike($p=0,$por_pagina=null,'v_funcionario','*',$like,$order=null));//pegar total registros
     $per_page=10;//numero de registros por paginas;
     $paginas=$total_registros/$per_page;
 
    



     if(isset($_GET['p']) && !empty($_GET['p'])){
         $pg=addslashes($_GET['p']);
     }
     $p=($pg-1)*$per_page;
        


    
     $data['filtro']=$filtro;
     $config['per_page'] = null;
     $data['cargo1'] = $this->Mos->get('tb_cargo', '*', $where = '', $config['per_page']=null, $this->uri->segment(3));//padrao de busca
     $data['secretaria'] = $this->Mos->get('tb_secretaria', '*', $where = '', $config['per_page']=null, $this->uri->segment(3));//padrao de busca
        //OS PADRÃO SEM FILTRO TRAZ TUDO
       
        $data['funcionario'] = $this->Mfuncionario->listaFuncionariosLike($p,$per_page,'v_funcionario','*',$like,$order=null);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['titulo']='Abrir Chamado';
        $data['view']='admin/os/add';//passa a view por padrao
        $data['jscript']='theme/footer';
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['tecnico']= $this->session->userdata('id_tecnico');
        
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
         $this->load->view('theme/header',$data);

    }
    public function save()
    {
       
       
        

    //validação somente front fica mais leve o codigo e mais facil
           

            
            $nm_funcionario = $this->input->post('nm_funcionario');
            $cid_funcionario = $this->input->post('cid_funcionario');
            $ob_os = $this->input->post('ob_os');
            $cid_tecnico = $this->input->post('id_tecnico');
            $df_os= $this->input->post('df_os');
            $st_os= $this->input->post('st_os');
            $dt_os=$this->input->post('dt_os');
            $hr_or=$this->input->post('hr_os');
            $titulo_os=$this->input->post('titulo_os');
            //monta o array de dados
            $data_banco=formataBD($dt_os);
            if($cid_tecnico=='null'){
                $cid_tecnico=1;
            } 
            $dados= array(
                'cid_funcionario' => $cid_funcionario,
                'ob_os' => $ob_os,
                'cid_tecnico' => $cid_tecnico,
                'df_os' => $df_os,
                'st_os' => $st_os,
                'dt_os'=>$data_banco,
                'hr_os'=>$hr_or,
                'titulo_os'=>$titulo_os

            );
           
            

           
           //envia e-mail para o funcionario ao qual o técnico adicionou novo chamado e também para o email padrão do sitema.
           
                
           $where=array(
            'id_funcionario'=>$cid_funcionario
        );
                    $id_chamado=$this->Mos->add('tb_os', $dados);//salva o chamado e pega o numero do chamdo.
                   
                    $movimentacao=array(//prepara para gravar na tabela tb_os_movimentação
                        'dt_movimentacao'=>$data_banco,
                        'hr_movimentacao'=>$hr_or,
                        'ld_movimentacao'=>'',
                        'cid_os'=>$id_chamado,
                        'st_os'=>$st_os
                    );

              $this->Mos->add('tb_os_movimentacao',$movimentacao); //grava movimentação  
                    
                    //busca o email do cliente para enviar o email.
                    $email=$this->Mfuncionario->pegaFuncionario('tb_funcionario',$where);
                   
                    
                    $this->envia_email($email[0]->nm_funcionario,$email[0]->mail_funcionario,$dados,$id_chamado);//envia o email



                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
               /* set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Chamado  adicionado com sucesso.
                 </div>', 'sucesso'); */
                  //Verifica se já existe esse dada cadastrado no banco de dados
             set_msg('salvo', '<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalWarning">Success</a><div id="modalWarning" class="modal-block modal-block-warning mfp-hide">
             <section class="panel">
                 <header class="panel-heading">
                     <h2 class="panel-title">ERPOS</h2>
                 </header>
                 <div class="panel-body">
                     <div class="modal-wrapper">
                         <div class="modal-icon">
                             <i class="fa fa-warning"></i>
                         </div>
                         <div class="modal-text">
                             <h4>Sucesso!</h4>
                             <p>Chamado Atualizado!.</p>
                         </div>
                     </div>
                 </div>
                 <footer class="panel-footer">
                     <div class="row">
                         <div class="col-md-12 text-right">
                             <button class="btn btn-warning modal-dismiss">OK</button>
                         </div>
                     </div>
                 </footer>
             </section>
         </div>', 'sucesso');
              
                redirect('os', 'refresh');

             

            




            
    
    }
    public function edit()
    {
      //pega parametro da url via get
        $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
        $config['per_page'] = null;
        $data['os'] = $this->Mos->get('tb_os', '*', $where = ['id_os' => $id], $config['per_page'], null);

        $data['titulo']='Editar Chamado';
        $data['view']='admin/os/edit';//passa a view por padrao
        $data['jscript']='theme/footer';
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
         $this->load->view('theme/header',$data);

    }

    public function update()
    {
        $id = $this->input->post('id_os');
        $nome = $this->input->post('nm_os'); 
     
     //editar  meio parecido com save
        $this->form_validation->set_rules('nm_os', 'Nome', 'required');

        if ($this->form_validation->run()) {
         //if this check of validation ok, to do  this action


            $nome = $this->input->post('nm_os');
        
         //monta o array de dados
            $dados = array(
                'nm_os' => $nome

            );

            if ($this->Mos->verificaUnicidade('tb_os', 'nm_os', $where = ['nm_os' => $nome])) {
              //Verifica se já existe esse dada cadastrado no banco de dados
                set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Atenção!</h4>
              Esse os já esta cadastrado.
            </div>', 'sucesso');
                redirect('os/new', 'refresh');
            } else {
                $this->Mos->edit('tb_os', $dados, 'id_os', $id);
             //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                os Atualizado.
              </div>', 'sucesso');
                redirect('os', 'refresh');
            }



        } else {
         //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.

            $data = null;
            $data['titulo']='Abrir Chamado';
            $data['view']='admin/os/add';//passa a view por padrao
            $data['jscript']='theme/footer';
           $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
            $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
            $this->load->view('theme/header',$data);
        }



    }
    public function delete()
    {
        $id = decrypt($this->uri->segment(3));
        $id_local= decrypt($this->uri->segment(4));
     //deletar simples sem confirmação por hora

        $this->Mos->delete('tb_os', 'id_os', $id);
        $this->Mos->delete('tb_local_trabalho', 'id_local', $id_local);
        
        set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
        redirect('os', 'refresh');

    }
    public function get_setor()
    {
        $id_setor = $this->input->post('cid_secretaria');
        $id_setor;
        echo $this->Mos->geraSelect('tb_setor', $where = ['cid_secretaria' => $id_setor]);

    }
    public function get_funcionario(){
        $where=array(
            'cid_setor'=>$id_setor,
            'id_secretaria'=>$cid_secretaria,
            'nm_funcionario'=>$busca_nome,
            'st_os'=>$st_os
           // 'dt_os'=>formataBD($data1).' BETWEEN '.formataBD($data2)
        );
        $this->session->set_userdata($where);
        
        //var_dump($this->input->post());
        echo $this->Mos->geraTabelaFuncionario('v_funcionario',$where);
    }
    public function get_os(){

        $id_setor = $this->input->post('id_setor');
        $cid_secretaria=$this->input->post('id_secretaria');
        $busca_nome=$this->input->post('busca_nome');
        $st_os=$this->input->post('st_os');
        $data2=$this->input->post('data2');
        $data1=$this->input->post('data1');
        
        $where=array(
            'cid_setor'=>$id_setor,
            'id_secretaria'=>$cid_secretaria,
            'nm_funcionario'=>$busca_nome,
            'st_os'=>$st_os
           // 'dt_os'=>formataBD($data1).' BETWEEN '.formataBD($data2)
        );
        $this->session->set_userdata($where);
        
        echo $this->Mos->geraTabela('v_os',$where);
    }


    


public function open(){
     //pegar uri do funcionario para fazer o join
     $id = decrypt($this->uri->segment(3));
     
     
     //prepara o where 
     $like=array(
         'id_funcionario'=>$id
     );
     $config['per_page']=null;
     


     $data['funcionario'] = $this->Mfuncionario->getFuncionariolike('v_funcionario', '*', $like,$join='', $config['per_page'], $this->uri->segment(3));
     

     $data['tecnico'] = $this->Mtecnico->get('v_tecnico', '*', $where = '',$join='', $config['per_page'], $this->uri->segment(3));
     
    
     $data['titulo']='Atender  Chamado';
     $data['view']='admin/os/open';//passa a view por padrao
     $data['jscript']='theme/footer';
     $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
     $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
      $this->load->view('theme/header',$data);
 
}
public function answer(){
     //primeiro busca todos os dados no banco 
     
      //pegar uri do funcionario para fazer o join
      $id = decrypt($this->uri->segment(3));
      //prepara o where 
      $where=array(
          'id_os'=>$id
      );
      $config['per_page']=null;
     // $data['funcionario'] = $this->Mfuncionario->get('tb_funcionario', '*', $where,$join='true', $config['per_page'], $this->uri->segment(3));
     // $data['tecnico'] = $this->Mtecnico->get('tb_funcionario', '*', $where = '',$join='true', $config['per_page'], $this->uri->segment(3));
      $data['os'] = $this->Mos->get('v_os', '*', $where,$join='', $config['per_page'], $this->uri->segment(3)); //busca dados os
      $id_tecnico=$data['os'][0]->cid_tecnico;

      $where=array(
          'id_tecnico'=>$id_tecnico
      );

      $data['tecnico'] = $this->Mtecnico->listaTecnicoWhere('v_tecnico','*',$where);//pega nome do tecnico
      
      $data['tecnico_tem']=$this->Mtecnico->listaTecnicoLike($pular=null,$row_perpage=null,'v_tecnico','*',$like=null,$where,$order=null);//pega todos os tecnicos

      /*criar logica aqui p/ quando o id tecnico estive null  */



     //$data['tecnico'] = $this->Mtecnico->get('v_tecnico', '*', $where =array('id_tecnico'=>$id_tecnico),$join='', $config['per_page'], $this->uri->segment(3));
    //carregar secretaria todas p/ fazer o carregamento dos setores
    // print_r($data['os']);
     
    $data['titulo']='Atender  Chamado';
    $data['view']='admin/os/answer';//passa a view por padrao
    $data['jscript']='theme/footer';
    $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
    $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
     $this->load->view('theme/header',$data);
}
public function logar(){
    #função para atualizar chamado 
  /*FUNÇAO ONDE ATUALIZA O STATUS DA OS PELO TECNICO */


    $user=$this->input->post('user');
    $passwords=$this->input->post('passwords');
    $id=$this->input->post('id2');
    
    $st_os=$this->input->post('st_os');
    $dt_atendimento=$this->input->post('dt_atendimento');
    $hr_atendimento=$this->input->post('hr_atendimento');
    $id_tecnico= $this->input->post('id_tecnico');
    $ld_tecnico=$this->input->post('ld_tecnico');
    $id_os=$this->input->post('id_os');
    $dt_encerramento=$this->input->post('dt_encerramento');
    
   
   
        
    $where=array(
      'usuario'=>$user,
      'password'=>$passwords
    );
    
   
    $retorno = $this->Mtecnico->ValidaLogin('tb_tecnico', '*', $where);//FAZ LOGIN PARA ATUALIZAR A OS
    
 
  
 

    ##FALTA VALIDAR POR USUARIO NO SISTEMA DO CHAMADO MAIS VAI FICAR P/ PROXIMA
     /*LEMBRAR AQUI 10-02-2019


         */
    ###
    if(empty($retorno)){//CASO RETORNE FALSO EXIBE ERRRO P/ TECNICO E SEGURA NA PAGINA DE LOGIN
        echo '<div class="alert alert-error alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Erro!</h4>
        Senha ou Usuário incorreto </div>';
    
    }else{//CASO LOGIN OK FAZ ATUALIZAÇÃO
        $id_tecnico_novo=$retorno[0]->id_tecnico;//pega o id do tecnico pela variavel retorno 
        /*FAZER AQUI OS UPDATES  */
        #dados update
        #02/05/2019 SE VIER ST_OS COMO FECHADO ADICIONAR DATA FECHAMENTO E HORA DO FECHAMENTO.
       
       if ($st_os=='Fechado') {
        $dados=array(
            'st_os'=>$st_os,
            'dt_atendimento'=>formataBD($dt_atendimento),
            'hr_atendimento'=>$hr_atendimento,
            'cid_tecnico'=>$id_tecnico_novo,
            'ld_tecnico'=>$ld_tecnico,
            'dt_encerramento'=>formataBD($dt_encerramento)
        ); 

        $movimentacao=array(//prepara para gravar na tabela tb_os_movimentação
            'dt_movimentacao'=>formataBD(date('d/m/Y')),
            'hr_movimentacao'=>date("H:i:s"),
            'ld_movimentacao'=>$ld_tecnico,
            'cid_os'=>$id_os,
            'st_os'=>$st_os
        );
       } else {
        $dados=array(
            'st_os'=>$st_os,
            'dt_atendimento'=>formataBD($dt_atendimento),
            'hr_atendimento'=>$hr_atendimento,
            'cid_tecnico'=>$id_tecnico_novo,
            'ld_tecnico'=>$ld_tecnico
        );
        $movimentacao=array(//prepara para gravar na tabela tb_os_movimentação
            'dt_movimentacao'=>formataBD(date('d/m/Y')),
            'hr_movimentacao'=>date("H:i:s"),
            'ld_movimentacao'=>$ld_tecnico,
            'cid_os'=>$id_os,
            'st_os'=>$st_os
        );

       }
       
       
        $this->Mos->add('tb_os_movimentacao',$movimentacao); //grava movimentação  

        $this->Mos->edit('tb_os', $dados, 'id_os', $id_os);//chama função editar
         //aqui implementar email com a nova atualização para o usuario 
         //03/07/2019



        //TUDO CERTO SETA MSG E REDIRECIONA P/ MESMA PAGINA.
        set_msg('salvo', '<div class="alert alert-success alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Chamado Atualizado!.
   </div>', 'sucesso');
        echo '<script>window.location.href ="'.base_url().'index.php/os/answer/'.encript($id_os).'";</script>';
    }
}

public function imprime_chamado(){
    $pdf = new FPDF();//cria o objeto fpdf
$id =decrypt($this->uri->segment(3));

echo $id;
//ate aqui certo
$where=array(
    'id_os'=>$id
);
$data['os']=$this->Mos->listaOsLike($p=0,$per_page=1,'v_os','*',$like=null,$porData=null,$where,$order=null);
$id_tecnico=$data['os'][0]->id_tecnico;


$where=array(
    'id_tecnico'=>$id_tecnico
);
$data['tecnico']=$this->Mtecnico->listaTecnicoLike($p=0,$por_pagina=1,'v_tecnico','*',$like=null,$where=$where,$order=null);


$this->load->view('admin/relatorios/imp_chamado', $data);
  
}

public function detalhe(){
    $id_os=decrypt($this->uri->segment(3));
    $p=0;
    $per_page=5;
    $where=array(
        'id_os'=>$id_os
    );
    $data['os']=$this->Mos->listaOsLike($p,$per_page,'v_os','*',$like=null,$porData=null,$where,$order='id_os');
    $order='id_movimentacao';
    $data['movimentacao']=$this->Mos->getMovimentacao('tb_os_movimentacao',$where=array('cid_os'=>$id_os));
    
    $data['titulo']='Detalhe  Chamado';
    $data['view']='admin/os/details';//passa a view por padrao
    $data['jscript']='theme/footer';
    $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
    $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
     $this->load->view('theme/header',$data); 
    
}
public function envia_email($usuario=null,$email=null,$dados,$chamado){
    //envia email assim que adicionar novo chamado
    
    $this->load->library('email');

   $subject = 'CPD-Sistema de chamados';
   $message = '<div class="tudo">
    <h3>CPD-Prefeitura Municipal de Patrocínio</h3>
    <br>
    <p><b>'.$usuario.'</b> seu chamado foi aberto.</p>
    <br>
   
    <div class="balao">
    
    <b>Chamado: </b>'.$chamado.'</br>
    <b>Data: </b> '.formataVisao($dados['dt_os']).'            <b>Horário: </b>'.$dados['hr_os'].'</br>
    <b>Título: </b>'.$dados['titulo_os'].'</br>
    <b>Problema: </b>'.$dados['df_os'].'</br>
    <b>Observação: </b>'.$dados['ob_os'].'</br>
 
    </div>
    
    <br>
    <br>
    <hr>
    <p>Qualquer dúvida entre em contato conosco 3839-1801 ramais 476,213 ou 500.</p>
    <p><b>CPD- Prefeitura de Patrocinio Minas Gerais.</b></p>
    <p><small>cpdpmp@patrocinio.mg.gov.br</small>.</p>
    </div>';
    
    $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
    <title>' . html_escape($subject) . '</title>
    <style type="text/css">
        body {
            font-family: Arial, Verdana, Helvetica, sans-serif;
            font-size: 16px;
        }
       
    .balao{
        border: 2px solid #605ca8;
        background-color: silver;
        padding:1%;
        color:#5C2582;
    }
    .tudo{
        background-color:white;
        color:black;
        padding:1%;
        border:1px solid #605ca8;
      
    
    </style>
</head>
<body>
' . $message . '
</body>
</html>';

$result = $this->email
    ->from('suporte@wepsistemas.net.eu.org')
    ->reply_to('cdppmp@patrocinio.mg.gov.br')    // Optional, an account where a human being reads.
    ->to($email)
    ->subject($subject)
    ->message($body)
    ->send();





}

}

