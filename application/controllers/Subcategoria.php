<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategoria extends CI_Controller {

	/**
	 * 
	 */
    public function __construct() {
        parent::__construct();
        
        $this->load->model('subcategoria/Subcategoria_model','Msubcategoria');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('principal', 'refresh');
			 
		   }
    }
    public function index()
	{
        //index manda para funcão principal gerenciar
      $this->gerenciar();
        
    }
    public function gerenciar(){

          //filtro nova os
          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['nm_subcategoria']) && !empty($_GET['nm_subcategoria'])){   
        $url='';
        
        if(isset($_GET['nm_subcategoria']) && !empty($_GET['nm_subcategoria'])){
            $filtro=true;
            $url='&nm_subcategoria='.$_GET['nm_subcategoria'];
            $like['nm_subcategoria']=$_GET['nm_subcategoria'];  
        }
        
       
       
    }else{
        $like=null;
        $url='';
    }
    
        //primeiro passas as  configuraçoes para pagination

     //$data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
     $p=0;//inicio do contador da paginação
     $pg=1;
     $total_registros= count($this->Msubcategoria->listaCargoLike($p=0,$por_pagina=null,'tb_subcategoria','*',$like,$order=null));//pegar total registros
     $per_page=5;//numero de registros por paginas;
     $paginas=$total_registros/$per_page;
       
     if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;
       
 


        //primeiro busca todos os dados no banco 
        $data['filtro']=$filtro;
        $data['subcategoria'] = $this->Msubcategoria->listaCargoLike($p,$per_page,'tb_subcategoria','*',$like,$order=null);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['titulo']='Subcategoria'; //titulo da pagina
        $data['view']='admin/subcategoria/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }
    public function novo(){
       //função que chama o formulario para adição de novo subcategoria
        $data = null;
        $config['per_page']=null;
        $data['categoria']=$this->Msubcategoria->get('tb_categoria','*',$where='',$config['per_page'],$this->uri->segment(3));
     
      
      
      
        $data['titulo']='Adicionar Subcategoria'; //titulo da pagina
        $data['view']='admin/subcategoria/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
    }
    public function save(){
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');

        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nome');
            $cid_categoria= $this->input->post('cid_categoria');
            //monta o array de dados
            
            $dados=array(
                'nm_subcategoria'=>$nome,
                'cid_categoria'=>$cid_categoria
               
            );
          
            if ($this->Msubcategoria->verificaUnicidade('tb_subcategoria','nm_subcategoria',$where=['nm_subcategoria'=>$nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse subcategoria já esta cadastrado.
               </div>', 'sucesso');
                redirect('subcategoria/novo', 'refresh');
            } else {
                $this->Msubcategoria->add('tb_subcategoria', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   subcategoria adicionada com sucesso.
                 </div>', 'sucesso');
                  redirect('subcategoria', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
           $data['titulo']='Adicionar Subcategoria'; //titulo da pagina
        $data['view']='admin/subcategoria/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
        }
    }
    public function edit(){
      //pega parametro da url via get
      $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
      $config['per_page']=null;
      $data['subcategoria'] = $this->Msubcategoria->get('tb_subcategoria', '*', $where =['id_subcategoria'=>$id], $config['per_page'], null);
      
        $data['titulo']='Editar Subcategoria'; //titulo da pagina
        $data['view']='admin/subcategoria/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }

    public function update(){
     $id=$this->input->post('id_subcategoria');
     $nome=$this->input->post('nm_subcategoria'); 
     
     //editar  meio parecido com save
     $this->form_validation->set_rules('nm_subcategoria', 'Nome', 'required');

     if ($this->form_validation->run()) {
         //if this check of validation ok, to do  this action
         
        
         $nome = $this->input->post('nm_subcategoria');
        
         //monta o array de dados
         $dados=array(
             'nm_subcategoria'=>$nome
             
         );
         
         if ($this->Msubcategoria->verificaUnicidade('tb_subcategoria','nm_subcategoria',$where=['nm_subcategoria'=>$nome])) {
              //Verifica se já existe esse dada cadastrado no banco de dados
              set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Atenção!</h4>
              Esse subcategoria já esta cadastrado.
            </div>', 'sucesso');
             redirect('subcategoria/novo', 'refresh');
         } else {
             $this->Msubcategoria->edit('tb_subcategoria',$dados,'id_subcategoria',$id);
             //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                subcategoria Atualizado.
              </div>', 'sucesso');
               redirect('subcategoria', 'refresh');
         }
           
         
         
     } else {
         //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
        $data['titulo']='Editar Subcategoria'; //titulo da pagina
        $data['view']='admin/subcategoria/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
     }



    }
    public function delete(){
     $id=decrypt($this->uri->segment(3));
     //deletar simples sem confirmação por hora
     
      
     try {
       //senão tiver executa a exclusão.
     $this->Msubcategoria->delete('tb_subcategoria','id_subcategoria',$id);
     set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
    redirect('subcategoria', 'refresh');
    } catch (Exception $e) {
        echo 'Exceção capturada: ',  $e->getMessage(), "\n";
    }
  

    
    }
    public function get_subgrupo()
    {
        $id_setor = $this->input->post('cid_categoria');
        $id_setor;
        echo $this->Mos->geraSelect('tb_categoria', $where = ['cid_categoria' => $id_setor]);

    }
    
    
    
    
}

?>