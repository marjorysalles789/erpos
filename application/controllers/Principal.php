<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();

        $this->load->model('funcionario/funcionario_model', 'Mfuncionario');
		$this->load->model('tecnico/tecnico_model', 'Mtecnico');
		$this->load->model('secretaria/secretaria_model', 'Msecretaria');
		$this->load->model('os/os_model','Mos');
		



    }
	public function index(){ 
		if($this->session->userdata('id_tecnico')){
      
			
			redirect('principal/painel', 'refresh');

			
			 }
			 //$this->load->view('admin/login/index');	 
			 $this->load->view('admin/login/index');	


	   
	}

	public function login(){
       $this->load->view('admin/login/index');
	   
	}
	public function logar(){
		/*
		 função logar do sistema essa função é acessada somente do front do usuario
		 é chamada por ajax e retorna 1 ou 0 para o js do navegador
		 implementada dia 16/05/2019 sistema ERPOS
		 configurado para salvar sessão no banco de dados evitando sobrecarga no servidor web
		*/
		$user=$this->input->post('nm_usuario');
		$senha=$this->input->post('senha');
		
		$where=array(
		  'usuario'=>$user,
		  'password'=>$senha
		);
		
		$resultado=$this->Mfuncionario->verificaUnicidade('v_tecnico','*',$where);
		
		
		if($resultado){
		 
		 $this->session->set_userdata($resultado);//salva todos os dados do funcionario na sessão                                                                                                                                                                                                                                                             
		 
		//var_dump($this->session->userdata('nm_funcionario')); consigo pegar cada dado pelo nome da variavel
		//usuario logado redireciona pra principal
		redirect('principal', 'refresh');
		
		}else{
		  set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Usuário ou senha incorretos.</label></center>', 'sucesso');
		  redirect('principal', 'refresh');
		}
		
		}
		
		public function logout(){
		  $this->Mfuncionario->delete('ci_sessions','id',$this->session->session_id);
		  $this->session->sess_destroy($this->session->session_id);
		  
		  
		  
		  redirect('principal','refresh');
		 
		 
		
		}
		public function painel(){

			$dados['secretaria']=$this->Msecretaria->count('tb_secretaria');
			$dados['tecnico']=$this->Mtecnico->count('tb_tecnico');   
			$dados['funcionario']=$this->Mfuncionario->count('tb_funcionario'); 
			$dados['os']=$this->Mos->contar('v_os');
			
			/*Statisticas para os chamados feitos com google charts
			Wesley 21-06-2019
			*/
			$dados['os_abertas']=count($this->Mos->count('tb_os',$where=array('st_os'=>'Aberto')));
			$dados['os_resolvendo']=count($this->Mos->count('v_os',$where=array('st_os'=>'Resolvendo')));
			$dados['os_cancelado']=count($this->Mos->count('v_os',$where=array('st_os'=>'Cancelado')));
			$dados['os_parado']=count($this->Mos->count('v_os',$where=array('st_os'=>'Parado')));
			$dados['os_fechado']=count($this->Mos->count('v_os',$where=array('st_os'=>'Fechado')));
			//estatiticas com data do dia
			$dados['os_abertas_d']=count($this->Mos->count('tb_os',$where=array('st_os'=>'Aberto','dt_os'=>date( 'Y-m-d '))));
			$dados['os_resolvendo_d']=count($this->Mos->count('v_os',$where=array('st_os'=>'Resolvendo','dt_os'=>date( 'Y-m-d '))));
			$dados['os_cancelado_d']=count($this->Mos->count('v_os',$where=array('st_os'=>'Cancelado','dt_os'=>date( 'Y-m-d '))));
			$dados['os_parado_d']=count($this->Mos->count('v_os',$where=array('st_os'=>'Parado','dt_os'=>date( 'Y-m-d '))));
			$dados['os_fechado_d']=count($this->Mos->count('v_os',$where=array('st_os'=>'Fechado','dt_os'=>date( 'Y-m-d '))));


			//nova chamada de view 
			$dados['titulo']='Dasboard';//titulo da view
			$dados['view']='admin/dasboard/page';//caminho da view
			$dados['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
			$dados['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
		
			$this->load->view('theme/header',$dados);
			//$this->load->view('admin/dasboard/page',$dados);
			//$this->load->view('theme/footer');
			//$this->load->view('theme/header');
			 
		}

	



}
