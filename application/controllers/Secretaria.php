<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secretaria extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('secretaria/Secretaria_model','MSecretaria');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

	public function index()
	{
        //index manda para funcão principal gerenciar
      $this->gerenciar();
        
    }
    public function gerenciar(){

    
    $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['nm_secretaria'])&& !empty($_GET['nm_secretaria']) or (isset($_GET['nm_secretario']) && !empty($_GET['nm_secretario']))){   
        $url='';
        
        if(isset($_GET['nm_secretaria']) && !empty($_GET['nm_secretaria'])){
            $filtro=true;
            $url='&nm_secretaria='.$_GET['nm_secretaria'];
            $like['nm_secretaria']=$_GET['nm_secretaria'];  
        }
        if(!empty($_GET['nm_secretario'])){
            $filtro=true;
            $url=$url.'&nm_secretario='.$_GET['nm_secretario'];
            $like['nm_secretario']=$_GET['nm_secretario'];
        }
       
       
    }else{
        $like=null;
        $url='';
    }
    
    //primeiro passas as  configuraçoes para pagination

    
    $p=0;//inicio do contador da paginação
    $pg=1;
    $total_registros= count($this->MSecretaria->listaOsLike($p=0,$por_pagina=null,'tb_secretaria','*',$like,$order=null));//pegar total registros
    $per_page=5;//numero de registros por paginas;
    $paginas=$total_registros/$per_page;

   



    if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;





        //primeiro busca todos os dados no banco 

        $data['secretaria'] = $this->MSecretaria->listaOsLike($p,$per_page,'tb_secretaria','*',$like,$order=null);
        $data['filtro']=$filtro;
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['secretaria1'] = $this->MSecretaria->get('tb_secretaria',$where='',$join=null,$this->uri->segment(3));
        $data['titulo']='Secretaria';
        $data['view']='admin/secretaria/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
        /* mudado layout do sistema wesley 21-07-2020 */
        //$this->load->view('admin/secretaria/index', $data);
        //$this->load->view('theme/footer');

    }
    public function novo(){
       //função que chama o formulario para adição de novo secretaria
       $data = null;
       $data['view']='admin/secretaria/add';//carrega a view do novo
       $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
	   $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
       $data['titulo']='Adicionar Secretaria';
       $this->load->view('theme/header',$data);
       // $this->load->view('admin/secretaria/add',$data);
      //  $this->load->view('theme/footer');

    }
    public function save(){
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('secretario', 'Secretario', 'required');
        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nome');
            $secretario=$this->input->post('secretario');
            //monta o array de dados
            $dados=array(
                'nm_secretaria'=>$nome,
                 'nm_secretario'=>$secretario
            );
            
            if ($this->MSecretaria->verificaUnicidade('tb_secretaria','nm_secretaria',$where=['nm_secretaria'=>$nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse secretaria já esta cadastrado.
               </div>', 'sucesso');
                redirect('secretaria/novo', 'refresh');
            } else {
                $this->MSecretaria->add('tb_secretaria', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   secretaria adicionado com sucesso.
                 </div>', 'sucesso');
                  redirect('secretaria', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
        $data['titulo']='Adicionar Secretaria';
        $data['view']='admin/secretaria/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
            $this->load->view('theme/header',$data);
          //  $this->load->view('admin/secretaria/add', $data);
            //$this->load->view('theme/footer');
        }
    }
    public function edit(){
      //pega parametro da url via get
      $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
      $config['per_page']=null;
      $data['secretaria'] = $this->MSecretaria->get('tb_secretaria', '*', $where =['id_secretaria'=>$id], $config['per_page'], null);
      $data['view']='admin/secretaria/edit';//carrega a view do edit
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
	  $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
      $data['titulo']='Editar Secretaria';
      $this->load->view('theme/header',$data);
       
     

    }

    public function update(){
        $id=$this->input->post('id_secretaria');
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('secretario', 'Secretario', 'required');
        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nome');
            $secretario=$this->input->post('secretario');
            //monta o array de dados
            $dados=array(
                'nm_secretaria'=>$nome,
                 'nm_secretario'=>$secretario
            );
            
            if ($this->MSecretaria->verificaUnicidade('tb_secretaria','nm_secretaria',$where=['nm_secretaria'=>$nome,'nm_secretario'=>$secretario])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse secretaria já esta cadastrado.
               </div>', 'sucesso');
                redirect('secretaria/novo', 'refresh');
            } else {
                $this->MSecretaria->edit('tb_secretaria',$dados,'id_secretaria',$id);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Secretaria Atualizada com sucesso.
                 </div>', 'sucesso');
                  redirect('secretaria', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
            $data = null;
            $this->load->view('theme/header');
            $this->load->view('admin/secretaria/add', $data);
            $this->load->view('theme/footer');
        }



    }
    public function delete(){
     $id=decrypt($this->uri->segment(3));
     //deletar simples sem confirmação por hora
      
     $this->MSecretaria->delete('tb_secretaria','id_secretaria',$id);
     set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
    redirect('secretaria', 'refresh');

    }
}
