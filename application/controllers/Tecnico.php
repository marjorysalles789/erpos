<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tecnico extends CI_Controller
{

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('funcionario/Funcionario_model', 'Mfuncionario');
        $this->load->model('tecnico/Tecnico_model', 'Mtecnico');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

    public function index()
    {
        //index manda para funcão principal gerenciar
        $this->gerenciar();

    }
    public function gerenciar()
    {

        
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['busca_nome'])&& !empty($_GET['busca_nome'])){   
        $url='';
        
       
        if(!empty($_GET['busca_nome'])){
            $filtro=true;
            $url=$url.'&busca_nome='.$_GET['busca_nome'];
            $like['nm_funcionario']=$_GET['busca_nome'];
        }
       
       
    }else{
        $like=null;
        $url='';
    }
    



        //primeiro passas as  configuraçoes para pagination

        $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
        $p=0;//inicio do contador da paginação
        $pg=1;
        $total_registros= count($this->Mtecnico->listaTecnicoLike($p=0,$por_pagina=null,'v_tecnico','*',$like,$order=null));//pegar total registros
        $per_page=5;//numero de registros por paginas;
        $paginas=$total_registros/$per_page;
    

        if(isset($_GET['p']) && !empty($_GET['p'])){
            $pg=addslashes($_GET['p']);
        }
        $p=($pg-1)*$per_page;
        //primeiro busca todos os dados no banco 

        $data['tecnico'] = $this->Mtecnico->listaTecnicoLike($p,$per_page,'v_tecnico','*',$like,$order=null);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['filtro']=$filtro;
      
        $data['titulo']='Técnico';
        $data['jscript']='theme/footer';
        $data['view']='admin/tecnico/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }
    public function novo()
    {
       //função que chama o formulario para adição de novo tecnico
        $data = null;
        $config['per_page'] = null;
        $data['cargo1'] = $this->Mtecnico->get('tb_cargo', '*', $where = '', $config['per_page'], $this->uri->segment(3));
        $data['secretaria'] = $this->Mtecnico->get('tb_secretaria', '*', $where = '', $config['per_page'], $this->uri->segment(3));
        $data['titulo']='Adicionar Técnico';
        $data['jscript']='theme/footer';
        $data['view']='admin/tecnico/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);;

    }
    public function save()
    {
       
   
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('mail_funcionario', 'E-mail', 'required');
        $this->form_validation->set_rules('cid_cargo', 'Cargo', 'required');
        $this->form_validation->set_rules('cid_secretaria', 'Secretaria', 'required');
        $this->form_validation->set_rules('mail_funcionario', 'E-mail', 'required');
        $this->form_validation->set_rules('id_setor', 'Setor', 'required');
        $this->form_validation->set_rules('nm_local', 'Local', 'required');
        $this->form_validation->set_rules('n_ramal', 'Ramal', 'required');
        $this->form_validation->set_rules('nm_usuario', 'Usuário', 'required');
        $this->form_validation->set_rules('senha', 'senha', 'required');
        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action


            $nome = $this->input->post('nome');
            $nm_local = $this->input->post('nm_local');
            $id_setor = $this->input->post('id_setor');
            $n_ramal = $this->input->post('n_ramal');
            $obs_local = $this->input->post('obs_local');
            $cid_cargo = $this->input->post('cid_cargo');
            $cid_secretaria = $this->input->post('cid_secretaria');
            $mail_funcionario = $this->input->post('mail_funcionario');
            $n_resp= $this->input->post('n_resp');
            $nm_usuario=$this->input->post('nm_usuario');
            $senha=$this->input->post('senha');
            //monta o array de dados

            $dlocal = array(
                'cid_setor' => $id_setor,
                'nm_local' => $nm_local,
                'n_ramal' => $n_ramal,
                'obs_local' => $obs_local,
                 'cid_sec'=>$cid_secretaria   
            );
            
            if ($this->Mtecnico->verificaUnicidade('tb_funcionario', 'nm_funcionario', $where = ['nm_funcionario' => $nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
                 <section class="panel">
                     <header class="panel-heading">
                         <h2 class="panel-title">ERPOS</h2>
                     </header>
                     <div class="panel-body">
                         <div class="modal-wrapper">
                             <div class="modal-icon">
                                 <i class="fa fa-info-circle"></i>
                             </div>
                             <div class="modal-text">
                                 <h4>Informação</h4>
                                 <p> Esse tecnico já esta cadastrado.</p>
                             </div>
                         </div>
                     </div>
                     <footer class="panel-footer">
                         <div class="row">
                             <div class="col-md-12 text-right">
                                 <button class="btn btn-info modal-dismiss">OK</button>
                             </div>
                         </div>
                     </footer>
                 </section>
             </div>','sucesso');
                
                redirect('tecnico/novo', 'refresh');
            } else {
                //salva primeiro o local de trabalho
                $id_local = $this->Mtecnico->add('tb_local_trabalho', $dlocal);//salva local de trabalho e retorna o id inserido
                
                /* cria os dados pra serem inseridos no novo funcionar junto com o id do local */
                $dados = array(
                    'cid_local'=>$id_local,
                    'cid_cargo'=>$cid_cargo,
                    'mail_funcionario'=>$mail_funcionario,
                    'n_resp'=>$n_resp,
                    'nm_funcionario'=>$nome,
                    'nm_usuario'=>$nm_usuario,
                    'senha'=>$senha,
                    'ativo'=>1
                );

                $id_funcionario=$this->Mtecnico->add('tb_funcionario', $dados);
                $tdados= array(
                    'usuario'=>$nm_usuario,
                    'password'=>$senha,
                    'status'=>1,
                    'fid_funcionario'=>$id_funcionario
                );
                $this->Mtecnico->add('tb_tecnico', $tdados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">ERPOS</h2>
                    </header>
                    <div class="panel-body">
                        <div class="modal-wrapper">
                            <div class="modal-icon">
                                <i class="fa fa-info-circle"></i>
                            </div>
                            <div class="modal-text">
                                <h4>Informação</h4>
                                <p>tecnico adicionado com sucesso.</p>
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-info modal-dismiss">OK</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>','sucesso');
                
                redirect('tecnico', 'refresh');
            }



        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.

            $data = null;
            $data['titulo']='Adicionar Técnico';
            $data['view']='admin/tecnico/add';//passa a view por padrao
            $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
            $data['jscript']='theme/footer';
            $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
             $this->load->view('theme/header',$data);
        }
    }
    public function edit()
    {
     //pega parametro da url via get
     $id = decrypt($this->uri->segment(3));
     //primeira coisa fazer um select com id
       $config['per_page'] = null;
       $data['cargo1'] = $this->Mfuncionario->pegaDados('tb_cargo');
       $data['secretaria'] = $this->Mfuncionario->pegaDados('tb_secretaria');
     //  $data['funcionario'] = $this->Mfuncionario->get('tb_funcionario', '*', $where = ['id_funcionario' => $id], $config['per_page'], null);
      $data['funcionario'] = $this->Mtecnico->get('v_tecnico', '*', $where = ['id_tecnico' => $id],$join='', $config['per_page'], $this->uri->segment(3));
     
      $data['titulo']='Editar Técnico';
      $data['view']='admin/tecnico/edit';//passa a view por padrao
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['jscript']='theme/footer';
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
      $this->load->view('theme/header',$data);

    }

    

    
    public function delete()
    {
        $id = decrypt($this->uri->segment(3));//id tecnico
       // $id_funcionario = decrypt($this->uri->segment(4));//id funcionario
       // $id_local= decrypt($this->uri->segment(5));//id local
     //deletar simples sem confirmação por hora
        $dados=array(
            'status'=>0
        );

       
        $this->Mfuncionario->edit('tb_tecnico',$dados,'id_tecnico',$id);
        set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
     <section class="panel">
         <header class="panel-heading">
             <h2 class="panel-title">ERPOS</h2>
         </header>
         <div class="panel-body">
             <div class="modal-wrapper">
                 <div class="modal-icon">
                     <i class="fa fa-info-circle"></i>
                 </div>
                 <div class="modal-text">
                     <h4>Informação</h4>
                     <p>Por motivo de segurança o registro foi marcado como inativo.</p>
                 </div>
             </div>
         </div>
         <footer class="panel-footer">
             <div class="row">
                 <div class="col-md-12 text-right">
                     <button class="btn btn-info modal-dismiss">OK</button>
                 </div>
             </div>
         </footer>
     </section>
 </div>','sucesso');
   
        redirect('tecnico', 'refresh');

    }
    public function get_setor()
    {
        $id_setor = $this->input->post('cid_secretaria');
        $id_setor;
        echo $this->Mtecnico->geraSelect('tb_setor', $where = ['cid_secretaria' => $id_setor]);

    }

    public function update($id)
    {
        
          //pega os dados via 
          $id_funcionario = decrypt($this->uri->segment(3));
          $nome = $this->input->post('nome');
          $nm_local = $this->input->post('nm_local');
          $id_setor = $this->input->post('id_setor');
          $n_ramal = $this->input->post('n_ramal');
          $obs_local = $this->input->post('obs_local');
          $cid_cargo = $this->input->post('cid_cargo');
          $cid_secretaria = $this->input->post('cid_secretaria');
          $mail_funcionario = $this->input->post('mail_funcionario');
          $n_resp= $this->input->post('n_resp');
          $nm_usuario=$this->input->post('nm_usuario');
          $senha=$this->input->post('senha');
          $id_local=$this->input->post('id_local');
          $id_setor_old=$this->input->post('id_setor_old');
          $id_tecnico= $this->input->post('id_tecnico');
          
          //monta o array de dados

          $dlocal = array(
              'cid_setor' => $id_setor,
              'nm_local' => $nm_local,
              'n_ramal' => $n_ramal,
              'obs_local' => $obs_local,
              'cid_sec'=>$cid_secretaria

          );
          

                //salva primeiro o local de trabalho
            
                
            /* cria os dados pra serem inseridos no novo funcionar junto com o id do local */
            $dados = array(
                'cid_local'=>$id_local,
                'cid_cargo'=>$cid_cargo,
                'mail_funcionario'=>$mail_funcionario,
                'n_resp'=>$n_resp,
                'nm_funcionario'=>$nome,
                'nm_usuario'=>$nm_usuario,
                'senha'=>$senha,
                'ativo'=>1
            );
            $dsetor=array(
               'cid_secretaria'=>$cid_secretaria

            );

            //dados do tecnico
            $tdados= array(
                'usuario'=>$nm_usuario,
                'password'=>$senha,
                'status'=>1,
                'fid_funcionario'=>$id
            );    
                 
                $this->Mfuncionario->edit('tb_funcionario', $dados, 'id_funcionario', $id);
                $this->Mfuncionario->edit('tb_tecnico',$tdados,'id_tecnico',$id_tecnico);
                $this->Mfuncionario->edit('tb_local_trabalho', $dlocal,'id_local',$id_local);//salva local de trabalho e retorna o id inserido
                
                
                
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
     <section class="panel">
         <header class="panel-heading">
             <h2 class="panel-title">ERPOS</h2>
         </header>
         <div class="panel-body">
             <div class="modal-wrapper">
                 <div class="modal-icon">
                     <i class="fa fa-info-circle"></i>
                 </div>
                 <div class="modal-text">
                     <h4>Informação</h4>
                     <p>Dados Atualizado.</p>
                 </div>
             </div>
         </div>
         <footer class="panel-footer">
             <div class="row">
                 <div class="col-md-12 text-right">
                     <button class="btn btn-info modal-dismiss">OK</button>
                 </div>
             </div>
         </footer>
     </section>
 </div>','sucesso');
            
              redirect('tecnico', 'refresh');
            }
}

