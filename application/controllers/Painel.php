<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Painel extends CI_Controller
{

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('os/Os_model', 'Mos');
        $this->load->model('secretaria/Secretaria_model','Msecretaria');
        $this->load->model('funcionario/Funcionario_model','Mfuncionario');
        $this->load->model('tecnico/Tecnico_model', 'Mtecnico');
        $this->load->library('form_validation');
        $this->load->helper('date');
        $this->load->helper('fpdf_helper');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

  public function index(){
     
    /*painel usuario aqui vão estar todas as funcionalidades atribuidas ao usuario como criar e acompanha chamados
    *
    */
    
    /*
    array(25) {
  ["__ci_last_regenerate"]=>
  int(1558391103)
  ["id_funcionario"]=>
  string(1) "1"
  ["cid_cargo"]=>
  string(1) "1"
  ["cid_local"]=>
  string(1) "1"
  ["nm_funcionario"]=>
  string(13) "ADMINISTRADOR"
  ["mail_funcionario"]=>
  string(30) "suporte@wepsistemas.net.eu.org"
  ["nm_usuario"]=>
  string(5) "admin"
  ["senha"]=>
  string(9) "admin4321"
  ["n_resp"]=>
  string(1) "1"
  ["ativo"]=>
  string(1) "1"
  ["id_cargo"]=>
  string(1) "1"
  ["nm_cargo"]=>
  string(20) "ANALISTA DE SISTEMAS"
  ["id_local"]=>
  string(1) "1"
  ["cid_setor"]=>
  string(1) "1"
  ["nm_local"]=>
  string(7) "CPD-PMP"
  ["n_ramal"]=>
  string(3) "476"
  ["obs_local"]=>
  string(18) "ANTIGO PREDIO IBGE"
  ["cid_sec"]=>
  string(1) "1"
  ["id_setor"]=>
  string(1) "1"
  ["cid_secretaria"]=>
  string(1) "1"
  ["nm_setor"]=>
  string(6) "CPD/TI"
  ["nm_responsavel"]=>
  string(12) "NICOLA TESLA"
  ["id_secretaria"]=>
  string(1) "1"
  ["nm_secretaria"]=>
  string(3) "CPD"
  ["nm_secretario"]=>
  string(11) "ALAN TURING"
}
<!DOCTYPE html>

    */
    $filtro=false;   
    //verifica se existe condição de busca #endregion
 if(isset($_GET['id_secretaria'])&& !empty($_GET['id_secretaria']) or (isset($_GET['id_setor']) && !empty($_GET['id_setor']) or (isset($_GET['st_os'])&& !empty($_GET['st_os'])) or (isset($_GET['busca_nome'])&& !empty($_GET['busca_nome'])) or (isset($_GET['data1'])&& !empty($_GET['data1'])) or (isset($_GET['data2'])&& !empty($_GET['data2'])) or isset($_GET['cid_tecnico'])&& !empty($_GET['cid_tecnico']) )){   
    $url='';
    $like=null;
    $porData='';
    $where='';
    if(isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria'])){
        $filtro=true;
        $url='&id_secretaria='.$_GET['id_secretaria'].'&id_setor='.$_GET['id_setor'].'&st_os='.$_GET['st_os'].'&busca_nome='.$_GET['busca_nome'];
        $like['cid_secretaria']=$_GET['id_secretaria'];  
    }
    if(!empty($_GET['id_setor'])){
        $filtro=true;
        $url=$url.'&id_setor='.$_GET['id_setor'];
        $like['id_setor']=$_GET['id_setor'];
    }
    if(!empty($_GET['st_os'])){
        $filtro=true;
        $url=$url.'&st_os='.$_GET['st_os'];
        $like['st_os']=$_GET['st_os'];
    }
    if(!empty($_GET['busca_nome'])){
        $filtro=true;
        $url=$url.'&busca_nome='.$_GET['busca_nome'];
        $like['nm_funcionario']=$_GET['busca_nome'];
    }
    if(!empty($_GET['data1']) && (!empty($_GET['data2']))){
        $filtro=true;
        $url=$url.'&data1='.$_GET['data1'].'&data2='.$_GET['data2'];
        $porData=array('de'=>formataBD($_GET['data1']),'ate'=>formataBD($_GET['data2']));
       
       
    }else{
        $url=$url.'&data1=';
    }
    if(!empty($_GET['cid_tecnico'])){
        $filtro=true;
        $url=$url.'&cid_tecnico='.$_GET['cid_tecnico'];
        $like['cid_tecnico']=$_GET['cid_tecnico'];
    }

    
   
   
}else{
    $like=null;
    $url='';
    $porData='';
    $where='';

}

/* busca por data esta funcionando 06-04-2019
 wesley da silva pereira  
*/


    $like['id_funcionario']=$this->session->userdata('id_funcionario');
  
    $p=0;//inicio do contador da paginação
    $pg=1;
    $total_registros= count($this->Mos->listaOsLike($p=0,$por_pagina=null,'v_os','*',$like,$porData,$order=null));//pegar total registros
    $per_page=10;//numero de registros por paginas;
    $paginas=$total_registros/$per_page;

    if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;
    $data['filtro']=$filtro;
    #carrega todos os tecnicos para o filtro
    $data['os']=$this->Mos->listaOsLike($p,$per_page,'v_os','*',$like,$porData,$where,$order);
    $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
    $this->load->view('usuario/theme/header');
    $this->load->view('usuario/dasboard/index',$data);
    $this->load->view('usuario/theme/footer');
    
    
    


  }
  public function detalhe(){
      $id_os=decrypt($this->uri->segment(3));
      $p=0;
      $per_page=5;
      $where=array(
          'id_os'=>$id_os
      );
      $data['os']=$this->Mos->listaOsLike($p,$per_page,'v_os','*',$like=null,$porData=null,$where,$order=null);
      $order='id_movimentacao';
      $data['movimentacao']=$this->Mos->getMovimentacao('tb_os_movimentacao',$where=array('cid_os'=>$id_os));
      $this->load->view('usuario/theme/header');
      $this->load->view('usuario/dasboard/details',$data);
      $this->load->view('usuario/theme/footer'); 
      
  }
  public function abrir_chamado(){
    $data = null;
    //id do funcionario logago vem por session
    $like['id_funcionario']=$this->session->userdata('id_funcionario');
    $data['funcionario'] = $this->Mfuncionario->listaFuncionariosLike($p=0,$per_page=5,'v_funcionario','*',$like,$order=null);
    $this->load->view('usuario/theme/header');
      $this->load->view('usuario/dasboard/add',$data);
      $this->load->view('usuario/theme/footer'); 
        
  }

  public function save_chamado(){


    //validação somente front fica mais leve o codigo e mais facil
           

            
    $nm_funcionario = $this->input->post('nm_funcionario');
    $cid_funcionario = $this->input->post('cid_funcionario');
    $ob_os = $this->input->post('ob_os');
    $cid_tecnico = $this->input->post('id_tecnico');
    $df_os= $this->input->post('df_os');
    $st_os= $this->input->post('st_os');
    $dt_os=$this->input->post('dt_os');
    $hr_or=$this->input->post('hr_os');
    $titulo_os=$this->input->post('titulo_os');
    //monta o array de dados
    $data_banco=formataBD($dt_os);
    if($cid_tecnico=='null'){
        $cid_tecnico=1;
    } 
    $dados= array(
        'cid_funcionario' => $cid_funcionario,
        'ob_os' => $ob_os,
        'cid_tecnico' => $cid_tecnico,
        'df_os' => $df_os,
        'st_os' => $st_os,
        'dt_os'=>$data_banco,
        'hr_os'=>$hr_or,
        'titulo_os'=>$titulo_os

    );

   //envia email com dados do chamado aberto
   $email_f=$this->session->userdata('mail_funcionario');
   $nome=$this->session->userdata('nm_funcionario');
  

         $chamado=$this->Mos->add('tb_os', $dados);
        //cria registro de movimentação do chamado
        $movimentacao=array(//prepara para gravar na tabela tb_os_movimentação
            'dt_movimentacao'=>$data_banco,
            'hr_movimentacao'=>$hr_or,
            'ld_movimentacao'=>'',
            'cid_os'=>$chamado,
            'st_os'=>$st_os
        );

   $this->Mos->add('tb_os_movimentacao',$movimentacao); //grava movimentação  




        //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
        set_msg('salvo', '<div class="alert alert-info alert-dismissible">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
           <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
           <b>Chamado adicionado, em breve seu problema será resolvido.<p>
           Dúvidas ligue no ramal 473 ou 213.</p></b>
         </div>', 'sucesso');



         $this->envia_email($nome,$email_f,$dados,$chamado);



        redirect('painel', 'refresh');
    



  }

  public function excluir_chamado(){
        //excluir chamado por parte do requerente 
        //aqui vai somente fazer update com o cancelamento.
        //criar regra que para excluir somente os que ainda não foram atendidos.
        
        $id =decrypt($this->uri->segment(3));//pega numero da id via get e decrypta 
    
            $nome = $this->input->post('nm_os');
        
         //monta o array de dados
         $st_os='Cancelado';
            $dados = array(
                'st_os'=>$st_os

            );

            $this->Mos->edit('tb_os', $dados, 'id_os', $id);
              //Verifica se já existe esse dada cadastrado no banco de dados
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Atenção!</h4>
              Chamado cancelado.
            </div>', 'sucesso');
                redirect('painel', 'refresh');
  
        }
   public function imprime_chamado(){
   
    


$pdf = new FPDF();//cria o objeto fpdf
$id =decrypt($this->uri->segment(3));

echo $id;
//ate aqui certo
$where=array(
    'id_os'=>$id
);
$data['os']=$this->Mos->listaOsLike($p=0,$per_page=1,'v_os','*',$like=null,$porData=null,$where,$order=null);
$id_tecnico=$data['os'][0]->id_tecnico;


$where=array(
    'id_tecnico'=>$id_tecnico
);
$data['tecnico']=$this->Mtecnico->listaTecnicoLike($p=0,$por_pagina=1,'v_tecnico','*',$like=null,$where=$where,$order=null);


$this->load->view('usuario/dasboard/relatorio', $data);
  

   }
   public function envia_email($usuario=null,$email=null,$dados,$chamado){
    //envia email assim que adicionar novo chamado
    
    
    
    $this->load->library('email');

    $subject = 'CPD-Sistema de chamados';
    $message = '<div class="tudo">
     <h3>CPD-Prefeitura Municipal de Patrocínio</h3>
     <br>
     <p><b>'.$usuario.'</b> seu chamado foi aberto.</p>
     <br>
    
     <div class="balao">
     
     <b>Chamado: </b>'.$chamado.'</br>
     <b>Data: </b> '.formataVisao($dados['dt_os']).'            <b>Horário: </b>'.$dados['hr_os'].'</br>
     <b>Título: </b>'.$dados['titulo_os'].'</br>
     <b>Problema: </b>'.$dados['df_os'].'</br>
     <b>Observação: </b>'.$dados['ob_os'].'</br>
  
     </div>
     
     <br>
     <br>
     <hr>
     <p>Qualquer dúvida entre em contato conosco 3839-1801 ramais 476,213 ou 500.</p>
     <p><b>CPD- Prefeitura de Patrocinio Minas Gerais.</b></p>
     <p><small>cpdpmp@patrocinio.mg.gov.br</small>.</p>
     </div>';
     
     $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
     <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
     <title>' . html_escape($subject) . '</title>
     <style type="text/css">
         body {
             font-family: Arial, Verdana, Helvetica, sans-serif;
             font-size: 16px;
         }
        
     .balao{
         border: 2px solid #605ca8;
         background-color: silver;
         padding:1%;
         color:#5C2582;
     }
     .tudo{
         background-color:#5C2582;
         color:white;
         padding:1%;
     
     </style>
 </head>
 <body>
 ' . $message . '
 </body>
 </html>';
 
 $result = $this->email
     ->from('suporte@wepsistemas.net.eu.org')
     ->reply_to('suporte@wepsistemas.net.eu.or')    // Optional, an account where a human being reads.
     ->to($email)
     ->subject($subject)
     ->message($body)
     ->send();
 
 
 
 
 
 }




}