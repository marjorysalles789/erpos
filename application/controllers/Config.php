<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Config extends CI_Controller
{

    /**Contreler da configuração do sistema
     * como Nome da empresa, email padrão para log
     * 
     * 
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('os/Os_model', 'Mos');
        $this->load->model('secretaria/Secretaria_model','Msecretaria');
        $this->load->model('funcionario/Funcionario_model','Mfuncionario');
        $this->load->model('tecnico/Tecnico_model', 'Mtecnico');
        $this->load->library('form_validation');
        $this->load->helper('date');
        $this->load->helper('fpdf_helper');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

    public function index()
    {  
        $this->gerenciar();
        
    }

}