<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller {

    public function __construct() {
        parent::__construct();
      
      $this->load->model('produto/Produto_model','Mproduto');
      $this->load->model('categoria/Categoria_model','Mcategoria');
      $this->load->library('form_validation'); 
      if(!$this->session->userdata('id_funcionario')){
      
			
        set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
        redirect('usuario', 'refresh');
         
       }
      
    }
    public function index(){

         //index manda para funcão principal gerenciar
      $this->gerenciar();
    }
    public function gerenciar(){

         /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['id_categoria'])&& !empty($_GET['id_categoria']) or (isset($_GET['nm_produto']) && !empty($_GET['nm_produto'])) or (isset($_GET['id_subgrupo']) && !empty($_GET['id_subgrupo']) ) ){   
        $url='';
        
        if(isset($_GET['id_categoria']) && !empty($_GET['id_categoria'])){
            $filtro=true;
            $url='&id_categoria='.$_GET['id_categoria'].'&nm_produto='.$_GET['nm_produto'];
            $like['cid_categoria']=$_GET['id_categoria'];  
        }
        if(!empty($_GET['nm_produto'])){
            $filtro=true;
            $url=$url.'&nm_produto='.$_GET['nm_produto'];
            $like['nm_produto']=$_GET['nm_produto'];
        }
        if(!empty($_GET['id_subgrupo'])){
            $filtro=true;
            $url=$url.'&id_subgrupo='.$_GET['id_subgrupo'];
            $like['id_subcategoria']=$_GET['id_subgrupo'];
        }
       
    }else{
        $like=null;
        $url='';
    }

        
        $data['categoria']=$this->Mcategoria->get('tb_categoria',$where='',$join=null,$this->uri->segment(3));
       
       
        $p=0;//inicio do contador da paginação
        $pg=1;
        $total_registros= count($this->Mproduto->listaOsLike($p=0,$por_pagina=null,'v_produto','*',$like,$order=null));//pegar total registros
        $per_page=5;//numero de registros por paginas;
        $paginas=$total_registros/$per_page;
    
        if(isset($_GET['p']) && !empty($_GET['p'])){
            $pg=addslashes($_GET['p']);
        }
        $p=($pg-1)*$per_page;
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['filtro']=$filtro;
        $data['produto']=$this->Mproduto->listaOsLike($p,$per_page,'v_produto','*',$like,$order);
        //primeiro busca todos os dados no banco 
     
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['titulo']='Produto'; //titulo da pagina
        $data['view']='admin/produto/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
    }
    public function novo(){
        //função que chama o formulario para adição de novo produto
        $data = null;
        $config['per_page']=null;
        $data['categoria']=$this->Mcategoria->get('tb_categoria','*',$where='',$config['per_page'],$this->uri->segment(3));
       
        $data['titulo']='Adicionar Produto'; //titulo da pagina
        $data['view']='admin/produto/add';//passa a view por padrao
        $data['jscript']='theme/footer';//adiciona script 
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
 
     }

     public function save(){
        
        $this->form_validation->set_rules('nm_produto', 'Produto', 'required');
       
        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nm_produto= $this->input->post('nm_produto');
            $id_categoria=$this->input->post('id_categoria');
            $id_subgrupo=$this->input->post('id_subgrupo');
            $desc_produto=$this->input->post('desc_produto');
            $cod_sysdardani=$this->input->post('cod_sysdardani');
            $unidade=$this->input->post('unidade');
            //monta o array de dados
            $dados=array(
                'nm_produto'=>$nm_produto,
                 'cid_subcategoria'=>$id_subgrupo,
                 'desc_produto'=>$desc_produto,
                 'cod_sysdardani'=>$cod_sysdardani,
                 'unidade'=>$unidade
            );
            if ($this->Mproduto->verificaUnicidade('tb_produto','nm_produto',$where=['nm_produto'=>$nm_produto])and ($this->Mcategoria->verificaUnicidade('tb_categoria','id_categoria',$where=['id_categoria'=>$id_categoria])) ) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse produto já esta cadastrado.
               </div>', 'sucesso');
                redirect('produto/novo', 'refresh');
            } else {
                $this->Mproduto->add('tb_produto', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   produto adicionado com sucesso.
                 </div>', 'sucesso');
                  redirect('produto', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
            $data = null;
            $this->load->view('theme/header');
            $this->load->view('admin/produto/add', $data);
            $this->load->view('theme/footer');
        }
    }
    public function delete(){
        $id=decrypt($this->uri->segment(3));
        //deletar simples sem confirmação por hora
         
        $this->Mproduto->delete('tb_produto','id_produto',$id);
        set_msg('salvo', '<div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
        Registro apagado.
      </div>', 'sucesso');
       redirect('produto', 'refresh');
   
       }
       public function edit($id_set){
        //pega parametro da url via get
        
      
        $id = decrypt($this->uri->segment(3));
          
           
      
        //primeira coisa fazer um select com 
       
        $config['per_page']=null;
        $data['produto'] = $this->Mproduto->get('v_produto','*', $where =['id_produto'=>$id],$join=null, $config['per_page'], $this->uri->segment(3));
       // $data['produto'][0]->cid_categoria;
        $data['categoria'] = $this->Mcategoria->get('tb_categoria', '*', $where ='', $config['per_page'], null);
        $data['titulo']='Adicionar Produto'; //titulo da pagina
        $data['view']='admin/produto/edit';//passa a view por padrao
        $data['jscript']='theme/footer';//adiciona script 
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
  
      }
      public function update(){

        $id=$this->input->post('id_produto');
        $this->form_validation->set_rules('nm_produto', 'Produto', 'required');
       // $this->form_validation->set_rules('responsavel', 'Responsável', 'required');
        
            //if this check of validation ok, to do  this action
            
           
            $nm_produto= $this->input->post('nm_produto');
            $id_categoria=$this->input->post('id_categoria');
            $id_subgrupo=$this->input->post('id_subgrupo');
            $desc_produto=$this->input->post('desc_produto');
            $cod_sysdardani=$this->input->post('cod_sysdardani');
            $unidade=$this->input->post('unidade');
            //monta o array de dados
            $dados=array(
                'nm_produto'=>$nm_produto,
                 'cid_subcategoria'=>$id_subgrupo,
                 'desc_produto'=>$desc_produto,
                 'cod_sysdardani'=>$cod_sysdardani,
                 'unidade'=>$unidade
            );
            
         //  var_dump($dados);
           
           
                $this->Mproduto->edit('tb_produto',$dados,'id_produto',$id);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Produto Atualizado com sucesso.
                 </div>', 'sucesso');
                  redirect('produto', 'refresh');
            
              
            
            
        /*
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
     
         //primeira coisa fazer um select com 
         echo 'deu erro ta aqui';
         exit;
         $config['per_page']=null;
         $data['produto'] = $this->Mproduto->get('v_produto','*', $where =['id_produto'=>$id],$join=null, $config['per_page'], $this->uri->segment(3));
       //  $sec=$data['produto'][0]->cid_subcategoria;
         $data['categoria'] = $this->Mcategoria->get('tb_categoria', '*', $where ='', $config['per_page'], null);
         $this->load->view('theme/header');
         $this->load->view('admin/produto/edit', $data);
         $this->load->view('theme/footer');
          */
        



    }
   
  


}