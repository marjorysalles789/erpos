<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setor extends CI_Controller {

    public function __construct() {
        parent::__construct();
      
      $this->load->model('setor/Setor_model','MSetor');
      $this->load->model('secretaria/Secretaria_model','MSecretaria');
      $this->load->library('form_validation'); 
      if(!$this->session->userdata('id_funcionario')){
      
			
        set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
        redirect('usuario', 'refresh');
         
       }
      
    }
    public function index(){

         //index manda para funcão principal gerenciar
      $this->gerenciar();
    }
    public function gerenciar(){

         /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['id_secretaria'])&& !empty($_GET['id_secretaria']) or (isset($_GET['nm_setor']) && !empty($_GET['nm_setor']) or (isset($_GET['nm_responsavel'])&& !empty($_GET['nm_responsavel'])))){   
        $url='';
        
        if(isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria'])){
            $filtro=true;
            $url='&id_secretaria='.$_GET['id_secretaria'].'&nm_setor='.$_GET['nm_setor'].'&nm_responsavel='.$_GET['nm_responsavel'];
            $like['cid_secretaria']=$_GET['id_secretaria'];  
        }
        if(!empty($_GET['nm_setor'])){
            $filtro=true;
            $url=$url.'&nm_setor='.$_GET['nm_setor'];
            $like['id_setor']=$_GET['nm_setor'];
        }
        if(!empty($_GET['nm_responsavel'])){
            $filtro=true;
            $url=$url.'&nm_responsavel='.$_GET['nm_responsavel'];
            $like['nm_responsavel']=$_GET['nm_responsavel'];
        }
       
    }else{
        $like=null;
        $url='';
    }

        
        $data['secretaria']=$this->MSecretaria->get('tb_secretaria',$where='',$join=null,$this->uri->segment(3));
       
       
        $p=0;//inicio do contador da paginação
        $pg=1;
        $total_registros= count($this->MSecretaria->listaOsLike($p=0,$por_pagina=null,'v_setor','*',$like,$order=null));//pegar total registros
        $per_page=5;//numero de registros por paginas;
        $paginas=$total_registros/$per_page;
    
        if(isset($_GET['p']) && !empty($_GET['p'])){
            $pg=addslashes($_GET['p']);
        }
        $p=($pg-1)*$per_page;
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['filtro']=$filtro;
        $data['setor']=$this->MSecretaria->listaOsLike($p,$per_page,'v_setor','*',$like,$order='order by id asc');
        $data['titulo']='Setor';
         $data['jscript']='theme/footer';//adiciona javascript
        $data['view']='admin/setor/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
        //$this->load->view('theme/header');
        //$this->load->view('admin/setor/index', $data);
        //$this->load->view('theme/footer');
        
    }
    public function novo(){
        //função que chama o formulario para adição de novo setor
        $data = null;
        $config['per_page']=null;
        $data['secretaria']=$this->MSecretaria->get('tb_secretaria','*',$where='',$config['per_page'],$this->uri->segment(3));
        $data['titulo']='Adicionar Setor';
        $data['view']='admin/setor/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
       // $this->load->view('theme/header');
       // $this->load->view('admin/setor/add',$data);
       // $this->load->view('theme/footer');
 
     }

     public function save(){
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        //$this->form_validation->set_rules('secretario', 'Secretario', 'required');
        $this->form_validation->set_rules('id_secretaria', 'Secretaria', 'required');
        $this->form_validation->set_rules('responsavel', 'responsavel', 'required');
        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nome');
            $responsavel=$this->input->post('responsavel');
            $id_secretaria=$this->input->post('id_secretaria');
            //monta o array de dados
            $dados=array(
                'nm_setor'=>$nome,
                 'nm_responsavel'=>$responsavel,
                 'cid_secretaria'=>$id_secretaria
            );
            
            if ($this->MSetor->verificaUnicidade('tb_setor','nm_setor',$where=['nm_setor'=>$nome])and ($this->MSecretaria->verificaUnicidade('tb_secretaria','id_secretaria',$where=['id_secretaria'=>$id_secretaria])) ) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse setor já esta cadastrado.
               </div>', 'sucesso');
                redirect('setor/novo', 'refresh');
            } else {
                $this->MSetor->add('tb_setor', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   setor adicionado com sucesso.
                 </div>', 'sucesso');
                  redirect('setor', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
            $data['titulo']='Adicionar Setor';
            $data['view']='admin/setor/add';//passa a view por padrao
            $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
            $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
            $this->load->view('theme/header',$data);
        }
    }
    public function delete(){
        $id=decrypt($this->uri->segment(3));
        //deletar simples sem confirmação por hora
         
        $this->MSetor->delete('tb_setor','id_setor',$id);
        set_msg('salvo', '<div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
        Registro apagado.
      </div>', 'sucesso');
       redirect('setor', 'refresh');
   
       }
       public function edit($id_set){
        //pega parametro da url via get
        
      
        $id = decrypt($this->uri->segment(3));
          
           
      
        //primeira coisa fazer um select com 
       
        $config['per_page']=null;
        $data['setor'] = $this->MSetor->get('tb_setor','*', $where =['id_setor'=>$id],$join=array('tabela'=>'tb_secretaria','campos'=>'id_secretaria=cid_secretaria'), $config['per_page'], $this->uri->segment(3));
        $data['setor'][0]->cid_secretaria;
        $data['secretaria'] = $this->MSecretaria->get('tb_secretaria', '*', $where ='', $config['per_page'], null);
        
        $data['titulo']='Editar Setor';
        $data['view']='admin/setor/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
       
  
      }
      public function update(){

        $id=$this->input->post('id_setor');
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('responsavel', 'Responsável', 'required');
        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
          
            $nome = $this->input->post('nome');
            $responsavel=$this->input->post('responsavel');
            $id_secretaria=$this->input->post('id_secretaria');
            //monta o array de dados
            $dados=array(
                'nm_setor'=>$nome,
                 'nm_responsavel'=>$responsavel,
                 'cid_secretaria'=>$id_secretaria
            );
            
           
           
                $this->MSetor->edit('tb_setor',$dados,'id_setor',$id);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Setor Atualizado com sucesso.
                 </div>', 'sucesso');
                  redirect('setor', 'refresh');
            
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
     
         //primeira coisa fazer um select com 
       
         $config['per_page']=null;
         $data['setor'] = $this->MSetor->get('tb_setor','*', $where =['id_setor'=>$id],$join=array('tabela'=>'tb_secretaria','campos'=>'id_secretaria=cid_secretaria'), $config['per_page'], $this->uri->segment(3));
         $sec=$data['setor'][0]->cid_secretaria;
         $data['secretaria'] = $this->MSecretaria->get('tb_secretaria', '*', $where ='', $config['per_page'], null);
         $this->load->view('theme/header');
         $this->load->view('admin/setor/edit', $data);
         $this->load->view('theme/footer');
   
        }



    }
  


}