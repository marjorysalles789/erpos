<?php
defined('BASEPATH') OR exit('No direct script access allowed');




function set_msg($id,$msg,$tipo) {
 if ($id){
   $CI=& get_instance();
   switch ($tipo) {
       case 'erro':

           $CI->session->set_flashdata($id,$msg);
           

           break;
       case 'sucesso':
            $CI->session->set_flashdata($id,$msg);
           break;
       default:
           break;
   }
 }
 return FALSE;
}

 function get_msg($id,$printar=true) {
    
     $CI=& get_instance();
     if($CI->session->flashdata($id)){
         if($printar){
             echo $CI->session->flashdata($id);
             return true;
         }
     }
    
}
 function dataDiaDB(){

    date_default_timezone_get('America/Sao_paulo');
    $formato='DATE_W3C';
    $hora=time();
    return standard_date($formato,$hora);
 }
 function formataBD($data){
   $data=str_split($data);
   //12/01/1990
   
   return  $data[6].$data[7].$data[8].$data[9].'-'.$data[3].$data[4].'-'.$data[0].$data[1];
 }
 function formataVisao($data){
    $data=str_split($data);
   // 2019-01-29
    
    return  $data[8].$data[9].'/'.$data[5].$data[6].'/'.$data[0].$data[1].$data[2].$data[3];
  }
 function encript($data){
     return base64_encode($data);
 }
 function decrypt($data){
     return base64_decode($data);
 }

 function paginacao($totalPaginas,$por_pagina,$p,$url){
   
   
  
    $max_links=5;
    $pg=ceil($totalPaginas/$por_pagina);
    $links_laterais =ceil($max_links / 2);
    $inicio = $p - $links_laterais;
    $limite = $p + $links_laterais;

    //variaveis do boostrap links
    $ul='<ul class="pagination">';
    $li='<li class="paginate_button previous disabled" id="example2_previous">';
    $fli='</li>';
    $ili='<li class="page-item"">';
    $ulf='</ul>';
    $style='class=""';
    $firt_link='<a href="?p=1'.$url.'">Primeira</a>';
    $last_link='<a href="?p='.$pg.$url.'">Última</a>';
    //primeiro link
    if($totalPaginas > $por_pagina){  
    $pag[0]=$ili.$firt_link.$fli; 
    for ($i = $inicio; $i <= $limite; $i++)
    {
     if ($i == $p)
     {
        $ili='<li class="page-item active">';
       $pag[$i]=$ili.'<a href="?p='.($i).$url.'" aria-controls="example2" >'.($i).'</a>'.$fli;
     }
     else
     { $ili='<li class="page-item">';
      if ($i >= 1 && $i <= $pg)
      {
       $pag[$i]=$ili.'<a href="?p='.($i).$url.'" aria-controls="example2" >'.($i).'</a>'.$fli;
      }
     }
    
    }
    array_push($pag,$ili.$last_link.$fli);
    return $pag;

 } 
}
   