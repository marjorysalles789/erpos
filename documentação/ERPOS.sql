-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 18/02/2019 às 11:48
-- Versão do servidor: 5.7.24-0ubuntu0.16.04.1
-- Versão do PHP: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ERPOS`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_cargo`
--

CREATE TABLE `tb_cargo` (
  `id_cargo` int(11) NOT NULL,
  `nm_cargo` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_cargo`
--

INSERT INTO `tb_cargo` (`id_cargo`, `nm_cargo`) VALUES
(1, 'TECNICO MECATRONICA');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_funcionario`
--

CREATE TABLE `tb_funcionario` (
  `id_funcionario` int(11) NOT NULL,
  `cid_cargo` int(11) DEFAULT NULL,
  `cid_local` int(11) DEFAULT NULL,
  `nm_funcionario` varchar(100) DEFAULT NULL,
  `mail_funcionario` varchar(255) DEFAULT NULL,
  `nm_usuario` varchar(20) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `n_resp` varchar(100) DEFAULT NULL,
  `ativo` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_funcionario`
--

INSERT INTO `tb_funcionario` (`id_funcionario`, `cid_cargo`, `cid_local`, `nm_funcionario`, `mail_funcionario`, `nm_usuario`, `senha`, `n_resp`, `ativo`) VALUES
(1, 1, 1, 'WESLEY DA SILVA PEREIRA', 'WESLEY_CRAS@HOTMAIL.COM', 'admin@admin.com', '88081156', '4654', 1),
(2, 1, 2, 'priscila silva de lima', 'armando@hotmail.com', 'hacker-games@hotmail', '88081156', '4654', 1),
(3, 1, 3, 'ADMIN', 'AMAR@GMAIL.COM', 'normam@hotmail.com', '88081156', '4654', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_local_trabalho`
--

CREATE TABLE `tb_local_trabalho` (
  `id_local` int(11) NOT NULL,
  `cid_setor` int(11) DEFAULT NULL,
  `nm_local` varchar(100) DEFAULT NULL,
  `n_ramal` varchar(20) DEFAULT NULL,
  `obs_local` varchar(255) DEFAULT NULL,
  `cid_sec` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_local_trabalho`
--

INSERT INTO `tb_local_trabalho` (`id_local`, `cid_setor`, `nm_local`, `n_ramal`, `obs_local`, `cid_sec`) VALUES
(1, 1, 'BOLSA FAMILIA', '23452345', 'ACIMA DO PREDIO DA PREFEITURA', 1),
(2, 2, 'BOLSA FAMILIA', '456', 'ACIMA DO PREDIO DA PREFEITURA', 2),
(3, 2, 'AC.SOCIAL', '23452345', 'obstrabalho', 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_os`
--

CREATE TABLE `tb_os` (
  `id_os` int(11) NOT NULL,
  `dt_os` date DEFAULT NULL,
  `hr_os` time DEFAULT NULL,
  `st_os` varchar(30) DEFAULT NULL,
  `df_os` varchar(255) DEFAULT NULL,
  `ld_tecnico` varchar(255) DEFAULT NULL,
  `dt_atendimento` date DEFAULT NULL,
  `hr_atendimento` time DEFAULT NULL,
  `dt_encerramento` datetime DEFAULT NULL,
  `ob_os` varchar(255) DEFAULT NULL,
  `cid_funcionario` int(11) DEFAULT NULL,
  `cid_tecnico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_os`
--

INSERT INTO `tb_os` (`id_os`, `dt_os`, `hr_os`, `st_os`, `df_os`, `ld_tecnico`, `dt_atendimento`, `hr_atendimento`, `dt_encerramento`, `ob_os`, `cid_funcionario`, `cid_tecnico`) VALUES
(2, '2019-02-10', '17:04:57', 'Cancelado', 'não liga nem a pau meu pc', '', '2019-02-12', '21:08:36', NULL, 'asdfasdfasdfasd', 2, 1),
(3, '2019-02-10', '17:05:21', 'Cancelado', 'ver tela queimada', 'cancelei essa merda', '2019-02-17', '17:03:52', NULL, 'muito cheiro de quimado', 1, 1),
(4, '2019-02-10', '17:31:18', 'Resolvendo', 'ASDASdASD', 'gsdgdfgsdfgsdfg', '2019-02-17', '17:07:43', NULL, 'ASDASDASD', 3, 1),
(5, '2019-02-11', '22:53:23', 'Cancelado', '', '', '2019-02-17', '16:33:19', NULL, '', 1, 1),
(6, '2019-02-12', '20:14:39', 'Cancelado', 'TESTANDO ESSA PORRA AGORA COM VIEWER QUE MUITO MAIS QUE EQUIO', 'BUCETA PRETA', '2019-02-14', '20:56:17', NULL, 'ASDFASDFASDFSADF', 1, 1),
(8, '2019-02-14', '20:59:08', 'Cancelado', '', '', '2019-02-17', '16:33:06', NULL, '', 1, 1),
(9, '2019-02-14', '20:59:26', 'Cancelado', '', '', '2019-02-17', '16:49:05', NULL, '', 1, 1),
(10, '2019-02-14', '20:59:46', 'Cancelado', '', '', '2019-02-17', '16:49:15', NULL, '', 1, 1),
(11, '2019-02-14', '21:00:03', 'Cancelado', '', '', '2019-02-17', '16:49:33', NULL, '', 1, 1),
(12, '2019-02-14', '21:00:24', 'Cancelado', '', '', '2019-02-17', '16:49:59', NULL, '', 1, 1),
(13, '2019-02-14', '21:00:49', 'Cancelado', '', '', '2019-02-17', '17:04:05', NULL, '', 1, 1),
(14, '2019-02-16', '23:12:55', 'Cancelado', '', '', '2019-02-17', '17:04:15', NULL, '', 1, 1),
(15, '2019-02-17', '10:07:15', 'Cancelado', '', '', '2019-02-17', '16:50:36', NULL, '', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_secretaria`
--

CREATE TABLE `tb_secretaria` (
  `id_secretaria` int(11) NOT NULL,
  `nm_secretaria` varchar(100) DEFAULT NULL,
  `nm_secretario` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_secretaria`
--

INSERT INTO `tb_secretaria` (`id_secretaria`, `nm_secretaria`, `nm_secretario`) VALUES
(1, 'OBRAS E POSTURAS', 'WESLEY DA SILVA PEREIRA'),
(2, 'ACÃO SOCIAL', 'HABITAÇÃO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_setor`
--

CREATE TABLE `tb_setor` (
  `id_setor` int(11) NOT NULL,
  `cid_secretaria` int(11) DEFAULT NULL,
  `nm_setor` varchar(100) DEFAULT NULL,
  `nm_responsavel` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_setor`
--

INSERT INTO `tb_setor` (`id_setor`, `cid_secretaria`, `nm_setor`, `nm_responsavel`) VALUES
(1, 1, 'OBRAS E POSTURAS', 'LETICIA'),
(2, 2, 'ACÃO SOCIAL', 'ANA LUCIA');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_tecnico`
--

CREATE TABLE `tb_tecnico` (
  `id_tecnico` int(11) NOT NULL,
  `usuario` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `fid_funcionario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `tb_tecnico`
--

INSERT INTO `tb_tecnico` (`id_tecnico`, `usuario`, `password`, `status`, `fid_funcionario`) VALUES
(1, 'normam@hotmail.com', '88081156', '1', 3);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `v_funcionario`
--
CREATE TABLE `v_funcionario` (
`id_funcionario` int(11)
,`cid_cargo` int(11)
,`cid_local` int(11)
,`nm_funcionario` varchar(100)
,`mail_funcionario` varchar(255)
,`nm_usuario` varchar(20)
,`senha` varchar(255)
,`n_resp` varchar(100)
,`ativo` int(1)
,`id_cargo` int(11)
,`nm_cargo` varchar(60)
,`id_local` int(11)
,`cid_setor` int(11)
,`nm_local` varchar(100)
,`n_ramal` varchar(20)
,`obs_local` varchar(255)
,`cid_sec` int(11)
,`id_setor` int(11)
,`cid_secretaria` int(11)
,`nm_setor` varchar(100)
,`nm_responsavel` varchar(100)
,`id_secretaria` int(11)
,`nm_secretaria` varchar(100)
,`nm_secretario` varchar(100)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `v_os`
--
CREATE TABLE `v_os` (
`id_os` int(11)
,`dt_os` date
,`hr_os` time
,`st_os` varchar(30)
,`df_os` varchar(255)
,`ld_tecnico` varchar(255)
,`dt_atendimento` date
,`hr_atendimento` time
,`dt_encerramento` datetime
,`ob_os` varchar(255)
,`cid_funcionario` int(11)
,`cid_tecnico` int(11)
,`id_funcionario` int(11)
,`cid_cargo` int(11)
,`cid_local` int(11)
,`nm_funcionario` varchar(100)
,`mail_funcionario` varchar(255)
,`nm_usuario` varchar(20)
,`senha` varchar(255)
,`n_resp` varchar(100)
,`ativo` int(1)
,`id_tecnico` int(11)
,`usuario` varchar(60)
,`password` varchar(60)
,`status` varchar(10)
,`fid_funcionario` int(11)
,`id_local` int(11)
,`cid_setor` int(11)
,`nm_local` varchar(100)
,`n_ramal` varchar(20)
,`obs_local` varchar(255)
,`cid_sec` int(11)
,`id_setor` int(11)
,`cid_secretaria` int(11)
,`nm_setor` varchar(100)
,`nm_responsavel` varchar(100)
,`id_secretaria` int(11)
,`nm_secretaria` varchar(100)
,`nm_secretario` varchar(100)
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `v_tecnico`
--
CREATE TABLE `v_tecnico` (
`id_tecnico` int(11)
,`usuario` varchar(60)
,`password` varchar(60)
,`status` varchar(10)
,`fid_funcionario` int(11)
,`id_funcionario` int(11)
,`cid_cargo` int(11)
,`cid_local` int(11)
,`nm_funcionario` varchar(100)
,`mail_funcionario` varchar(255)
,`nm_usuario` varchar(20)
,`senha` varchar(255)
,`n_resp` varchar(100)
,`ativo` int(1)
,`id_cargo` int(11)
,`nm_cargo` varchar(60)
,`id_local` int(11)
,`cid_setor` int(11)
,`nm_local` varchar(100)
,`n_ramal` varchar(20)
,`obs_local` varchar(255)
,`cid_sec` int(11)
,`id_setor` int(11)
,`cid_secretaria` int(11)
,`nm_setor` varchar(100)
,`nm_responsavel` varchar(100)
,`id_secretaria` int(11)
,`nm_secretaria` varchar(100)
,`nm_secretario` varchar(100)
);

-- --------------------------------------------------------

--
-- Estrutura para view `v_funcionario`
--
DROP TABLE IF EXISTS `v_funcionario`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_funcionario`  AS  select `tb_funcionario`.`id_funcionario` AS `id_funcionario`,`tb_funcionario`.`cid_cargo` AS `cid_cargo`,`tb_funcionario`.`cid_local` AS `cid_local`,`tb_funcionario`.`nm_funcionario` AS `nm_funcionario`,`tb_funcionario`.`mail_funcionario` AS `mail_funcionario`,`tb_funcionario`.`nm_usuario` AS `nm_usuario`,`tb_funcionario`.`senha` AS `senha`,`tb_funcionario`.`n_resp` AS `n_resp`,`tb_funcionario`.`ativo` AS `ativo`,`tb_cargo`.`id_cargo` AS `id_cargo`,`tb_cargo`.`nm_cargo` AS `nm_cargo`,`tb_local_trabalho`.`id_local` AS `id_local`,`tb_local_trabalho`.`cid_setor` AS `cid_setor`,`tb_local_trabalho`.`nm_local` AS `nm_local`,`tb_local_trabalho`.`n_ramal` AS `n_ramal`,`tb_local_trabalho`.`obs_local` AS `obs_local`,`tb_local_trabalho`.`cid_sec` AS `cid_sec`,`tb_setor`.`id_setor` AS `id_setor`,`tb_setor`.`cid_secretaria` AS `cid_secretaria`,`tb_setor`.`nm_setor` AS `nm_setor`,`tb_setor`.`nm_responsavel` AS `nm_responsavel`,`tb_secretaria`.`id_secretaria` AS `id_secretaria`,`tb_secretaria`.`nm_secretaria` AS `nm_secretaria`,`tb_secretaria`.`nm_secretario` AS `nm_secretario` from ((((`tb_funcionario` join `tb_cargo` on((`tb_funcionario`.`cid_cargo` = `tb_cargo`.`id_cargo`))) join `tb_local_trabalho` on((`tb_funcionario`.`cid_local` = `tb_local_trabalho`.`id_local`))) join `tb_setor` on((`tb_local_trabalho`.`cid_setor` = `tb_setor`.`id_setor`))) join `tb_secretaria` on((`tb_setor`.`cid_secretaria` = `tb_secretaria`.`id_secretaria`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `v_os`
--
DROP TABLE IF EXISTS `v_os`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_os`  AS  select `tb_os`.`id_os` AS `id_os`,`tb_os`.`dt_os` AS `dt_os`,`tb_os`.`hr_os` AS `hr_os`,`tb_os`.`st_os` AS `st_os`,`tb_os`.`df_os` AS `df_os`,`tb_os`.`ld_tecnico` AS `ld_tecnico`,`tb_os`.`dt_atendimento` AS `dt_atendimento`,`tb_os`.`hr_atendimento` AS `hr_atendimento`,`tb_os`.`dt_encerramento` AS `dt_encerramento`,`tb_os`.`ob_os` AS `ob_os`,`tb_os`.`cid_funcionario` AS `cid_funcionario`,`tb_os`.`cid_tecnico` AS `cid_tecnico`,`tb_funcionario`.`id_funcionario` AS `id_funcionario`,`tb_funcionario`.`cid_cargo` AS `cid_cargo`,`tb_funcionario`.`cid_local` AS `cid_local`,`tb_funcionario`.`nm_funcionario` AS `nm_funcionario`,`tb_funcionario`.`mail_funcionario` AS `mail_funcionario`,`tb_funcionario`.`nm_usuario` AS `nm_usuario`,`tb_funcionario`.`senha` AS `senha`,`tb_funcionario`.`n_resp` AS `n_resp`,`tb_funcionario`.`ativo` AS `ativo`,`tb_tecnico`.`id_tecnico` AS `id_tecnico`,`tb_tecnico`.`usuario` AS `usuario`,`tb_tecnico`.`password` AS `password`,`tb_tecnico`.`status` AS `status`,`tb_tecnico`.`fid_funcionario` AS `fid_funcionario`,`tb_local_trabalho`.`id_local` AS `id_local`,`tb_local_trabalho`.`cid_setor` AS `cid_setor`,`tb_local_trabalho`.`nm_local` AS `nm_local`,`tb_local_trabalho`.`n_ramal` AS `n_ramal`,`tb_local_trabalho`.`obs_local` AS `obs_local`,`tb_local_trabalho`.`cid_sec` AS `cid_sec`,`tb_setor`.`id_setor` AS `id_setor`,`tb_setor`.`cid_secretaria` AS `cid_secretaria`,`tb_setor`.`nm_setor` AS `nm_setor`,`tb_setor`.`nm_responsavel` AS `nm_responsavel`,`tb_secretaria`.`id_secretaria` AS `id_secretaria`,`tb_secretaria`.`nm_secretaria` AS `nm_secretaria`,`tb_secretaria`.`nm_secretario` AS `nm_secretario` from (((((`tb_os` join `tb_funcionario` on((`tb_os`.`cid_funcionario` = `tb_funcionario`.`id_funcionario`))) join `tb_tecnico` on((`tb_os`.`cid_tecnico` = `tb_tecnico`.`id_tecnico`))) join `tb_local_trabalho` on((`tb_funcionario`.`cid_local` = `tb_local_trabalho`.`id_local`))) join `tb_setor` on((`tb_local_trabalho`.`cid_setor` = `tb_setor`.`id_setor`))) join `tb_secretaria` on((`tb_setor`.`cid_secretaria` = `tb_secretaria`.`id_secretaria`))) ;

-- --------------------------------------------------------

--
-- Estrutura para view `v_tecnico`
--
DROP TABLE IF EXISTS `v_tecnico`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tecnico`  AS  select `tb_tecnico`.`id_tecnico` AS `id_tecnico`,`tb_tecnico`.`usuario` AS `usuario`,`tb_tecnico`.`password` AS `password`,`tb_tecnico`.`status` AS `status`,`tb_tecnico`.`fid_funcionario` AS `fid_funcionario`,`tb_funcionario`.`id_funcionario` AS `id_funcionario`,`tb_funcionario`.`cid_cargo` AS `cid_cargo`,`tb_funcionario`.`cid_local` AS `cid_local`,`tb_funcionario`.`nm_funcionario` AS `nm_funcionario`,`tb_funcionario`.`mail_funcionario` AS `mail_funcionario`,`tb_funcionario`.`nm_usuario` AS `nm_usuario`,`tb_funcionario`.`senha` AS `senha`,`tb_funcionario`.`n_resp` AS `n_resp`,`tb_funcionario`.`ativo` AS `ativo`,`tb_cargo`.`id_cargo` AS `id_cargo`,`tb_cargo`.`nm_cargo` AS `nm_cargo`,`tb_local_trabalho`.`id_local` AS `id_local`,`tb_local_trabalho`.`cid_setor` AS `cid_setor`,`tb_local_trabalho`.`nm_local` AS `nm_local`,`tb_local_trabalho`.`n_ramal` AS `n_ramal`,`tb_local_trabalho`.`obs_local` AS `obs_local`,`tb_local_trabalho`.`cid_sec` AS `cid_sec`,`tb_setor`.`id_setor` AS `id_setor`,`tb_setor`.`cid_secretaria` AS `cid_secretaria`,`tb_setor`.`nm_setor` AS `nm_setor`,`tb_setor`.`nm_responsavel` AS `nm_responsavel`,`tb_secretaria`.`id_secretaria` AS `id_secretaria`,`tb_secretaria`.`nm_secretaria` AS `nm_secretaria`,`tb_secretaria`.`nm_secretario` AS `nm_secretario` from (((((`tb_tecnico` join `tb_funcionario` on((`tb_tecnico`.`fid_funcionario` = `tb_funcionario`.`id_funcionario`))) join `tb_cargo` on((`tb_funcionario`.`cid_cargo` = `tb_cargo`.`id_cargo`))) join `tb_local_trabalho` on((`tb_funcionario`.`cid_local` = `tb_local_trabalho`.`id_local`))) join `tb_setor` on((`tb_local_trabalho`.`cid_setor` = `tb_setor`.`id_setor`))) join `tb_secretaria` on((`tb_setor`.`cid_secretaria` = `tb_secretaria`.`id_secretaria`))) ;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tb_cargo`
--
ALTER TABLE `tb_cargo`
  ADD PRIMARY KEY (`id_cargo`);

--
-- Índices de tabela `tb_funcionario`
--
ALTER TABLE `tb_funcionario`
  ADD PRIMARY KEY (`id_funcionario`),
  ADD KEY `FK_tb_funcionario_2` (`cid_local`),
  ADD KEY `FK_tb_funcionario_3` (`cid_cargo`);

--
-- Índices de tabela `tb_local_trabalho`
--
ALTER TABLE `tb_local_trabalho`
  ADD PRIMARY KEY (`id_local`),
  ADD KEY `FK_tb_local_trabalho_1` (`cid_setor`),
  ADD KEY `FK_tb_local_trabalho_2` (`cid_sec`);

--
-- Índices de tabela `tb_os`
--
ALTER TABLE `tb_os`
  ADD PRIMARY KEY (`id_os`),
  ADD KEY `FK_tb_os_2` (`cid_funcionario`),
  ADD KEY `FK_tb_os_3` (`cid_tecnico`);

--
-- Índices de tabela `tb_secretaria`
--
ALTER TABLE `tb_secretaria`
  ADD PRIMARY KEY (`id_secretaria`);

--
-- Índices de tabela `tb_setor`
--
ALTER TABLE `tb_setor`
  ADD PRIMARY KEY (`id_setor`),
  ADD KEY `FK_tb_setor_2` (`cid_secretaria`);

--
-- Índices de tabela `tb_tecnico`
--
ALTER TABLE `tb_tecnico`
  ADD PRIMARY KEY (`id_tecnico`),
  ADD KEY `FK_tb_tecnico_2` (`fid_funcionario`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `tb_cargo`
--
ALTER TABLE `tb_cargo`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `tb_funcionario`
--
ALTER TABLE `tb_funcionario`
  MODIFY `id_funcionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `tb_local_trabalho`
--
ALTER TABLE `tb_local_trabalho`
  MODIFY `id_local` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `tb_os`
--
ALTER TABLE `tb_os`
  MODIFY `id_os` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de tabela `tb_secretaria`
--
ALTER TABLE `tb_secretaria`
  MODIFY `id_secretaria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `tb_setor`
--
ALTER TABLE `tb_setor`
  MODIFY `id_setor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `tb_tecnico`
--
ALTER TABLE `tb_tecnico`
  MODIFY `id_tecnico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `tb_funcionario`
--
ALTER TABLE `tb_funcionario`
  ADD CONSTRAINT `FK_tb_funcionario_2` FOREIGN KEY (`cid_local`) REFERENCES `tb_local_trabalho` (`id_local`),
  ADD CONSTRAINT `FK_tb_funcionario_3` FOREIGN KEY (`cid_cargo`) REFERENCES `tb_cargo` (`id_cargo`);

--
-- Restrições para tabelas `tb_local_trabalho`
--
ALTER TABLE `tb_local_trabalho`
  ADD CONSTRAINT `FK_tb_local_trabalho_1` FOREIGN KEY (`cid_setor`) REFERENCES `tb_setor` (`id_setor`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_tb_local_trabalho_2` FOREIGN KEY (`cid_sec`) REFERENCES `tb_secretaria` (`id_secretaria`) ON DELETE NO ACTION;

--
-- Restrições para tabelas `tb_os`
--
ALTER TABLE `tb_os`
  ADD CONSTRAINT `FK_tb_os_2` FOREIGN KEY (`cid_funcionario`) REFERENCES `tb_funcionario` (`id_funcionario`),
  ADD CONSTRAINT `FK_tb_os_3` FOREIGN KEY (`cid_tecnico`) REFERENCES `tb_tecnico` (`id_tecnico`);

--
-- Restrições para tabelas `tb_setor`
--
ALTER TABLE `tb_setor`
  ADD CONSTRAINT `FK_tb_setor_2` FOREIGN KEY (`cid_secretaria`) REFERENCES `tb_secretaria` (`id_secretaria`);

--
-- Restrições para tabelas `tb_tecnico`
--
ALTER TABLE `tb_tecnico`
  ADD CONSTRAINT `FK_tb_tecnico_2` FOREIGN KEY (`fid_funcionario`) REFERENCES `tb_funcionario` (`id_funcionario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
